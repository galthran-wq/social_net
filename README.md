Fullstack, more-than-a-CRUD web app with CI (automatic testing and documentation powered by sphinx) and CD (deploy to AWS with awscdk) built with Vue.js + Django.

## Run locally

Specify environment (or simply use ```.env.dummy```):

```
cp .env.template .env
vim .env
```

then export all the variables from the file

```
export $(cat .env | egrep -v "(^#.*|^$)" | xargs)
```

Finally,

```
docker compose up
```

The applicatoin is now running at localhost:80

## Features

- auth/login with microservices compatible ```JWT``` (stateless authentication/authorization -- no cookies on the server);
- basic CRUD functions

<figure>
    <img src="./imgs/crud.png" alt="Trulli" style="width:100%">
    <figcaption align = "center">
        <b>VK-like basic group manegement features</b>
    </figcaption>
</figure>

- flexible premission system (discord-like) -- group-specific custom roles

<figure>
    <img src="./imgs/roles.png" alt="Trulli" style="width:100%">
    <figcaption align = "center">
        <b>Exhaustive variety of customizable permissions</b>
    </figcaption>
</figure>

- clean architecture