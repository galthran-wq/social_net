from django.urls import path, include

urlpatterns = [
    path(
        'api/',  # todo test purposes, redo
        include([
            path("messages/", include("django_apps.messages.urls")),
            path("auth/", include("django_apps.users.urls")),
            path("", include("django_apps.groups.urls")),
        ])
    )
]
