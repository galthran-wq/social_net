"""
    This module contains channels JWT-compatible auth middleware
"""
from django.contrib.auth.models import AnonymousUser
from django.db import close_old_connections
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.exceptions import (
    TokenError,
    InvalidToken,
)


class JWTAuthenticationMiddlewareAdapter(JWTAuthentication):
    def __init__(self, inner):
        # Store the ASGI application we were passed
        self.inner = inner

    def __call__(self, scope):
        # Close old database connections to prevent usage of timed out
        # connections
        close_old_connections()

        # Error handling is not supported, so assumes its just anonymous
        try:
            user = self.authenticate(scope)
        except (TokenError, InvalidToken):
            user = AnonymousUser()

        return self.inner(dict(scope, user=user))

    def get_header(self, scope):
        """extracts bearer token from authorization header"""
        return bytes(
            [
                header_tuple[1]
                for header_tuple in scope["headers"]
                if header_tuple[0].lower() == "authorization"
            ][0],
            "utf-8",
        )
