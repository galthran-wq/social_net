import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get("DEBUG")

ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS")

# Application definition
INSTALLED_APPS = [
    "django.contrib.contenttypes",
    "django.contrib.auth",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "rest_framework",
    "guardian",
    "django_rest_passwordreset",
    "rest_framework_simplejwt",
    "rest_framework_simplejwt.token_blacklist",
    "social_django",  # django social auth
    "rest_social_auth",  # this package
    "django_filters",
    "channels",
    "django_apps.users",
    "django_apps.groups",
    "django_apps.messages",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "backend.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

ASGI_APPLICATION = "backend.routing.application"
WSGI_APPLICATION = "backend.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": os.environ.get("SQL_ENGINE"),
        "NAME": os.environ.get("SQL_DATABASE"),
        "USER": os.environ.get("SQL_USER"),
        "PASSWORD": os.environ.get("SQL_PASSWORD"),
        "HOST": os.environ.get("SQL_HOST"),
        "PORT": os.environ.get("SQL_PORT"),
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

#  AUTH_PASSWORD_VALIDATORS = [
#      {
#          "NAME": "django.contrib.users.password_validation.UserAttributeSimilarityValidator",  # noqa
#      },
#      {
#          "NAME": "django.contrib.users.password_validation.MinimumLengthValidator",  # noqa
#      },
#      {
#          "NAME": "django.contrib.users.password_validation.CommonPasswordValidator",  # noqa
#      },
#      {
#          "NAME": "django.contrib.users.password_validation.NumericPasswordValidator",  # noqa
#      },
#  ]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = "/static/"

# REST Config
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
    ),
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",  # noqa
    "PAGE_SIZE": 10,
}

# Channels config
CHANNEL_LAYERS = (
    {
        "default": {
            "BACKEND": "channels_redis.core.RedisChannelLayer",
            "CONFIG": {
                "hosts": [
                    (os.environ.get("REDIS_HOST"),
                     os.environ.get("REDIS_PORT"))
                ],
            },
        },
    }
    if not DEBUG
    else {"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}}
)

# Auth
AUTH_USER_MODEL = "users.CustomUser"

AUTHENTICATION_BACKENDS = [
    "social_core.backends.discord.DiscordOAuth2",
    "django_apps.users.backend.EmailOrUsernameModelBackend",
    "guardian.backends.ObjectPermissionBackend",
]

# used in reset password flow
DJANGO_REST_LOOKUP_FIELD = "email__email"

SOCIAL_AUTH_DISCORD_KEY = "736981795295330335"
SOCIAL_AUTH_DISCORD_SECRET = "gL3-vB4u9UIKHmdZxCBz7yCsTCMlLE7G"

# email config
EMAIL_USE_TLS = os.environ.get("EMAIL_USE_TLS")
EMAIL_HOST = os.environ.get("EMAIL_HOST")
EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD")
EMAIL_PORT = os.environ.get("EMAIL_PORT")

# sites config
SITE_ID = 1
# celery
CELERY_BROKER_URL = CELERY_RESULT_BACKEND = \
    f"{os.environ.get('REDIS_HOST')}:{os.environ.get('REDIS_PORT')}"
CELERY_BEAT_SCHEDULE = {}
