from channels.routing import ProtocolTypeRouter, URLRouter

import django_apps.messages.routing
from .middlewares import JWTAuthenticationMiddlewareAdapter

application = ProtocolTypeRouter(
    {
        # (http->django views is added by default)
        "websocket": JWTAuthenticationMiddlewareAdapter(
            URLRouter(django_apps.messages.routing.websocket_urlpatterns)
        ),
    }
)
