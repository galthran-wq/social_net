django\_apps.messages package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   django_apps.messages.migrations
   django_apps.messages.tests

Submodules
----------

django\_apps.messages.apps module
---------------------------------

.. automodule:: django_apps.messages.apps
   :members:

django\_apps.messages.consumers module
--------------------------------------

.. automodule:: django_apps.messages.consumers
   :members:

django\_apps.messages.models module
-----------------------------------

.. automodule:: django_apps.messages.models
   :members:

django\_apps.messages.permissions module
----------------------------------------

.. automodule:: django_apps.messages.permissions
   :members:

django\_apps.messages.routing module
------------------------------------

.. automodule:: django_apps.messages.routing
   :members:

django\_apps.messages.serializers module
----------------------------------------

.. automodule:: django_apps.messages.serializers
   :members:

django\_apps.messages.urls module
---------------------------------

.. automodule:: django_apps.messages.urls
   :members:

django\_apps.messages.utils module
----------------------------------

.. automodule:: django_apps.messages.utils
   :members:

django\_apps.messages.views module
----------------------------------

.. automodule:: django_apps.messages.views
   :members:

Module contents
---------------

.. automodule:: django_apps.messages
   :members:
