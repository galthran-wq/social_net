django\_apps.messages.tests package
===================================

Submodules
----------

django\_apps.messages.tests.test\_consumers module
--------------------------------------------------

.. automodule:: django_apps.messages.tests.test_consumers
   :members:

django\_apps.messages.tests.test\_serializers module
----------------------------------------------------

.. automodule:: django_apps.messages.tests.test_serializers
   :members:

django\_apps.messages.tests.test\_utils module
----------------------------------------------

.. automodule:: django_apps.messages.tests.test_utils
   :members:

django\_apps.messages.tests.test\_views module
----------------------------------------------

.. automodule:: django_apps.messages.tests.test_views
   :members:

Module contents
---------------

.. automodule:: django_apps.messages.tests
   :members:
