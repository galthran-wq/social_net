django\_apps.groups.migrations package
======================================

Submodules
----------

django\_apps.groups.migrations.0001\_initial module
---------------------------------------------------

.. automodule:: django_apps.groups.migrations.0001_initial
   :members:

django\_apps.groups.migrations.0002\_auto\_20200730\_1221 module
----------------------------------------------------------------

.. automodule:: django_apps.groups.migrations.0002_auto_20200730_1221
   :members:

django\_apps.groups.migrations.0003\_remove\_group\_followers module
--------------------------------------------------------------------

.. automodule:: django_apps.groups.migrations.0003_remove_group_followers
   :members:

Module contents
---------------

.. automodule:: django_apps.groups.migrations
   :members:
