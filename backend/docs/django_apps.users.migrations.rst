django\_apps.users.migrations package
=====================================

Submodules
----------

django\_apps.users.migrations.0001\_initial module
--------------------------------------------------

.. automodule:: django_apps.users.migrations.0001_initial
   :members:

django\_apps.users.migrations.0002\_auto\_20200730\_1539 module
---------------------------------------------------------------

.. automodule:: django_apps.users.migrations.0002_auto_20200730_1539
   :members:

django\_apps.users.migrations.0003\_profile\_friends module
-----------------------------------------------------------

.. automodule:: django_apps.users.migrations.0003_profile_friends
   :members:

Module contents
---------------

.. automodule:: django_apps.users.migrations
   :members:
