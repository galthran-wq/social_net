django\_apps package
====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   django_apps.groups
   django_apps.messages
   django_apps.users

Module contents
---------------

.. automodule:: django_apps
   :members:
