django\_apps.groups package
===========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   django_apps.groups.migrations
   django_apps.groups.mixins
   django_apps.groups.tests

Submodules
----------

django\_apps.groups.apps module
-------------------------------

.. automodule:: django_apps.groups.apps
   :members:

django\_apps.groups.fields module
---------------------------------

.. automodule:: django_apps.groups.fields
   :members:

django\_apps.groups.filters module
----------------------------------

.. automodule:: django_apps.groups.filters
   :members:

django\_apps.groups.managers module
-----------------------------------

.. automodule:: django_apps.groups.managers
   :members:

django\_apps.groups.models module
---------------------------------

.. automodule:: django_apps.groups.models
   :members:

django\_apps.groups.permissions module
--------------------------------------

.. automodule:: django_apps.groups.permissions
   :members:

django\_apps.groups.serializers module
--------------------------------------

.. automodule:: django_apps.groups.serializers
   :members:

django\_apps.groups.signals module
----------------------------------

.. automodule:: django_apps.groups.signals
   :members:

django\_apps.groups.tasks module
--------------------------------

.. automodule:: django_apps.groups.tasks
   :members:

django\_apps.groups.urls module
-------------------------------

.. automodule:: django_apps.groups.urls
   :members:

django\_apps.groups.utils module
--------------------------------

.. automodule:: django_apps.groups.utils
   :members:

django\_apps.groups.validators module
-------------------------------------

.. automodule:: django_apps.groups.validators
   :members:

django\_apps.groups.views module
--------------------------------

.. automodule:: django_apps.groups.views
   :members:

Module contents
---------------

.. automodule:: django_apps.groups
   :members:
