django\_apps.messages.migrations package
========================================

Submodules
----------

django\_apps.messages.migrations.0001\_initial module
-----------------------------------------------------

.. automodule:: django_apps.messages.migrations.0001_initial
   :members:

django\_apps.messages.migrations.0002\_message\_ignored\_by module
------------------------------------------------------------------

.. automodule:: django_apps.messages.migrations.0002_message_ignored_by
   :members:

django\_apps.messages.migrations.0003\_dialogue\_creator module
---------------------------------------------------------------

.. automodule:: django_apps.messages.migrations.0003_dialogue_creator
   :members:

django\_apps.messages.migrations.0004\_auto\_20200807\_1253 module
------------------------------------------------------------------

.. automodule:: django_apps.messages.migrations.0004_auto_20200807_1253
   :members:

Module contents
---------------

.. automodule:: django_apps.messages.migrations
   :members:
