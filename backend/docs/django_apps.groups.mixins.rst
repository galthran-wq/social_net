django\_apps.groups.mixins package
==================================

Submodules
----------

django\_apps.groups.mixins.models module
----------------------------------------

.. automodule:: django_apps.groups.mixins.models
   :members:

django\_apps.groups.mixins.views module
---------------------------------------

.. automodule:: django_apps.groups.mixins.views
   :members:

Module contents
---------------

.. automodule:: django_apps.groups.mixins
   :members:
