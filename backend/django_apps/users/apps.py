from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = "django_apps.users"

    # noinspection PyUnresolvedReferences
    def ready(self):
        from . import signals  # noqa
