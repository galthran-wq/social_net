from celery import shared_task
from django.core.mail import send_mail
from django.urls import reverse


@shared_task
def send_email_confirmation(recipient, code):
    """
    Sends email confirmation.
    TODO: User external service.

    :param recipient: recipient's email address
    :param code: confirmation code
    """
    send_mail(
        "Email confirmation",
        reverse("auth:confirm-email", kwargs={"token": code}),
        "noreply@my-site.local",
        [recipient],
    )


@shared_task
def send_password_reset(recipient, code):
    """
    Sends password reset confirmation.
    TODO: User external service.

    :param recipient: recipient's email address
    :param code: confirmation code
    """
    send_mail(
        "Password reset",
        # must redirect to frontend,
        # then using this code to confirm endpoint
        code,
        "noreply@my-site.local",
        [recipient],
    )
