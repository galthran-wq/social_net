from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from .views import (
    SignupView,
    LogoutView,
    ChangePasswordView,
    ProfileView,
    ConfirmEmailView,
    SendEmailConfirmationView,
)

app_name = "auth"

password_urls = [
    path("change/", ChangePasswordView.as_view(), name="password_change",),
    path(
        "reset/",
        include("django_rest_passwordreset.urls", namespace="password_reset",),
    ),
]

urlpatterns = [
    path("signup/", SignupView.as_view(), name="signup"),
    path(
        "confirm-email/<path:token>/",
        ConfirmEmailView.as_view(),
        name="confirm-email",
    ),
    path(
        "send-email-confirmation/",
        SendEmailConfirmationView.as_view(),
        name="send-email-confirmation",
    ),
    path("login/", TokenObtainPairView.as_view(), name="login"),
    path("refresh/", TokenRefreshView.as_view(), name="refresh"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("profile/", ProfileView.as_view(), name="profile"),
    path("password/", include(password_urls)),
    path("social/login/", include("rest_social_auth.urls_jwt_pair")),
]
