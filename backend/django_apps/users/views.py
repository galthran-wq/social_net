from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _
from rest_framework import status
from rest_framework.generics import (
    CreateAPIView,
    RetrieveUpdateDestroyAPIView,
    GenericAPIView,
)
from rest_framework.mixins import UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import EmailAddress
from .serializers import (
    CustomUserSerializer,
    ProfileSerializer,
    ChangePasswordSerializer, LogoutSerializer,
)


class SignupView(CreateAPIView):
    model = get_user_model()
    serializer_class = CustomUserSerializer


class ProfileView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileSerializer

    def get_object(self):
        """
        Return request user's profile as an
        object to operate on.

        :rtype: :class:`.Profile`
        """
        return self.request.user.profile


class LogoutView(GenericAPIView):
    serializer_class = LogoutSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        """
        Instantiates serializer with request.data.
        Saves if data is valid, otherwise -
        handle serializer errors

        :return: 200 Response if valid, 400 otherwise
        :rtype: :class:`.Response`
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK)


class ChangePasswordView(GenericAPIView, UpdateModelMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer

    def get_object(self):
        """
        Returns request.user as object to
        operate on

        :return:
        """
        return self.request.user

    def post(self, request):
        """
        Invokes update with redefined serializer_class:
        :class:`.ChangePasswordSerializer`

        Accepts only POST requests.
        """
        return super(ChangePasswordView, self).update(request)


class ConfirmEmailView(APIView):
    def post(self, request, token):
        """
        Tries to get an :class:*.EmailAddress* object
        using token provided in the URL.

        Confirms email, if the object exists.

        :param request: request
        :param token: token from url
        :return: 200 Response, if token is valid and email is confirmed,
        and 400 Response otherwise
        """
        email = EmailAddress.from_key(token)

        if email is None:
            return Response(
                _("Token is invalid"), status=status.HTTP_400_BAD_REQUEST,
            )

        email.confirm()

        return Response()


class SendEmailConfirmationView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        """
        Sends email confirmation to request.user's email.

        :param request:
        :return: 200 Response
        """
        request.user.email.send_confirmation()
        return Response()
