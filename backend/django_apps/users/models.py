from django.core import signing
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)

from .tasks import send_email_confirmation


class CustomUserManager(BaseUserManager):
    def create_user(self, email, username=None, password=None):
        """
        Creates user, and corresponding :class:`.EmailAddress`.

        :param email: user's email address
        :param username: username (*optional*)
        :param password:
        :return: new user object
        :rtype: :class:`.CustomUser`
        """
        email_address = EmailAddress.objects.create(email=email)

        user = self.model(username=username, email=email_address)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, *args, **kwargs):
        """
        Creates user via :meth:`.create_user`
        and sets *is_superuser* flag to true.
        """
        user = self.create_user(*args, **kwargs)
        user.is_superuser = True
        user.save()
        return user


class EmailAddress(models.Model):
    email = models.EmailField(max_length=127, unique=True)
    is_verified = models.BooleanField(default=False)

    def send_confirmation(self):
        """
        Invokes :func:`send_email_confirmation` celery task
        providing email key (self._key)
        """
        send_email_confirmation.delay(self.email, self._key)

    def confirm(self):
        """
        Sets *is_verified* flag to True
        """
        if not self.is_verified:
            self.is_verified = True
            self.save()

    def __str__(self):
        """
        Returns raw email as object's representation.

        :rtype: str
        """
        return self.email

    @property
    def _key(self):
        """
        Dumps self.pk via django's signing package

        :return: key
        :rtype: str
        """
        return signing.dumps(obj=self.pk)

    @classmethod
    def from_key(cls, key):
        """
        Gets EmailAddress object from dumped key

        :param key: Dumped EmailAddress primary key
        :return:
        """
        try:
            max_age = 60 * 60 * 24  # 1 day long
            pk = signing.loads(key, max_age=max_age)
            ret = EmailAddress.objects.get(pk=pk)
        except (
            signing.SignatureExpired,
            signing.BadSignature,
            EmailAddress.DoesNotExist,
        ):
            ret = None
        return ret


class CustomUser(PermissionsMixin, AbstractBaseUser):
    username = models.CharField(
        max_length=127, unique=True, blank=True, null=True
    )
    email = models.OneToOneField(
        EmailAddress, on_delete=models.SET_NULL, default=None, null=True,
    )
    is_verified = models.BooleanField(default=False)
    USERNAME_FIELD = "username"
    objects = CustomUserManager()

    def __str__(self):
        """
        If there is username: return it.
        Else return representation of email instance

        :rtype: str
        """
        return self.username or str(self.email)


class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=32, blank=True, default='No')
    last_name = models.CharField(max_length=32, blank=True, default='Name')
    description = models.CharField(max_length=127, blank=True)
    friends = models.ManyToManyField("self")
