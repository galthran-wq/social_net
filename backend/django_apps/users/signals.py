from django.db.models.signals import post_save
from django.dispatch import receiver
from django_rest_passwordreset.signals import reset_password_token_created

from .models import CustomUser, Profile
from .tasks import send_password_reset


@receiver(reset_password_token_created)
def reset_password_signal(
    sender, instance, reset_password_token, *args, **kwargs
):
    """
    Handles password reset tokens.
    When a token is created, an e-mail needs to be sent to the user

    :param sender: View Class that sent the signal
    :param instance: View Instance that sent the signal
    :param reset_password_token: Token Model Object
    """
    send_password_reset.delay(
        reset_password_token.user.email.email, reset_password_token.key,
    )


@receiver(post_save, sender=CustomUser)
def initialize_user_profile(sender, instance, **kwargs):
    """
    Creates corresponding to the new custom user profile object.

    :param sender: View Class that sent the signal
    :param instance: View Instance that sent the signal
    """
    if not getattr(instance, "profile", None):
        Profile.objects.create(user=instance)
