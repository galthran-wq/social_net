from django.contrib.auth.backends import ModelBackend

from .models import EmailAddress, CustomUser


class EmailOrUsernameModelBackend(ModelBackend):
    """
    Custom auth backend that allows to user authentication
    using either email or username
    """
    def authenticate(self, request, username=None, password=None, **kwargs):
        if '@' in username:
            kwargs.update({'email': EmailAddress.objects.get(email=username)})
        else:
            kwargs.update({'username': username})

        try:
            user = CustomUser.objects.get(**kwargs)
        except CustomUser.DoesNotExist:
            pass
        else:
            if user.check_password(password):
                return user
