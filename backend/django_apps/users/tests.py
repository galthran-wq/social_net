from unittest import TestCase
from unittest.mock import patch

from django.contrib.auth import authenticate
from django.core import mail
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import RefreshToken

from .backend import EmailOrUsernameModelBackend
from .models import CustomUser, EmailAddress, Profile
from .serializers import ProfileSerializer
from .tasks import send_email_confirmation, send_password_reset


class AuthorizedTestRequestsMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = APIClient()

    @staticmethod
    def get_new_user():
        user_id = CustomUser.objects.count()
        new_user = CustomUser.objects.create(
            email=EmailAddress.objects.create(
                email=f"some_email{user_id}@gmail.com",
            ),
            username=f"some_username{user_id}",
        )
        new_user.set_password("some_test_password")
        new_user.save()
        return new_user

    @staticmethod
    def _get_auth_headers(user):
        return {
            "HTTP_AUTHORIZATION": "Bearer "
            + str(RefreshToken.for_user(user).access_token)
        }

    def _authorized_get(self, path, user=None):
        return self.client.get(
            path, **self._get_auth_headers(user) if user is not None else {},
        )

    def _authorized_post(self, path, body, user=None):
        return self.client.post(
            path,
            body,
            **self._get_auth_headers(user) if user is not None else {},
            format="json",
        )

    def _authorized_patch(self, path, body, user=None):
        return self.client.patch(
            path,
            body,
            **self._get_auth_headers(user) if user is not None else {},
            format="json",
        )

    def _authorized_put(self, path, body, user=None):
        return self.client.put(
            path,
            body,
            **self._get_auth_headers(user) if user is not None else {},
            format="json",
        )

    def _authorized_delete(self, path, user=None):
        return self.client.delete(
            path, **self._get_auth_headers(user) if user is not None else {},
        )


class AccountTest(AuthorizedTestRequestsMixin, APITestCase):
    def test_successful_user_signup(self):
        data = {
            "email": "testemail@gmail.com",
            "password": "testpassword",
        }
        response = self.client.post(
            reverse("auth:signup"), data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.data["email"]["email"], data["email"]
        )
        self.assertFalse(response.data["email"]["is_verified"])
        self.assertIn("access", response.data)
        self.assertIn("refresh", response.data)

    def test_successful_user_signup_without_username(self):
        data = {
            "username": "test_username",
            "email": "testemail@gmail.com",
            "password": "testpassword",
        }
        response = self.client.post(
            reverse("auth:signup"), data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.data["email"]["email"], data["email"]
        )
        self.assertFalse(response.data["email"]["is_verified"])
        self.assertEqual(response.data["username"], data["username"])
        self.assertIn("access", response.data)
        self.assertIn("refresh", response.data)

    def test_unsuccessful_user_signup(self):
        data = {
            "username": "test_username2",
            "email": "testemail@gmail.com",
            "password": "testpassword",
        }

        # create user
        self.client.post(reverse("auth:signup"), data, format="json")
        # try to create the same user
        response = self.client.post(
            reverse("auth:signup"), data, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("username", response.data)
        self.assertIn("email", response.data)


class LoggedInAccountTest(AuthorizedTestRequestsMixin, APITestCase):
    def setUp(self) -> None:
        self.user = self.get_new_user()
        self.user.refresh = RefreshToken.for_user(user=self.user)
        self.user.access = str(self.user.refresh.access_token)

    def test_successful_user_login(self):
        self.user.set_password("test_password")
        self.user.save()
        data = {
            "username": self.user.username,
            "password": "test_password",
        }
        response = self.client.post(reverse("auth:login"), data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("access", response.data)
        self.assertIn("refresh", response.data)

    def test_unsuccessful_user_login(self):
        data = {
            "username": "test_username",
            "password": "wrong_password",
        }
        response = self.client.post(reverse("auth:login"), data, format="json")

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertIn("detail", response.data)

    def test_user_not_found_login(self):
        data = {
            "username": "no_such_user",
            "password": "some_password",
        }
        response = self.client.post(reverse("auth:login"), data, format="json")

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertIn("detail", response.data)

    def test_logout(self):
        refresh = RefreshToken.for_user(self.user)
        access = str(refresh.access_token)
        response = self.client.post(
            reverse("auth:logout"),
            {"refresh": str(refresh)},
            HTTP_AUTHORIZATION="Bearer " + access,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        with self.assertRaises(TokenError):
            RefreshToken.check_blacklist(refresh)

    def test_logout_blacklisted_token(self):
        refresh = RefreshToken.for_user(self.user)
        access = str(refresh.access_token)
        refresh.blacklist()
        response = self.client.post(
            reverse("auth:logout"),
            {"refresh": str(refresh)},
            HTTP_AUTHORIZATION="Bearer " + access,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("refresh", response.data)

    def test_logout_with_missing_data(self):
        refresh = RefreshToken.for_user(self.user)
        access = str(refresh.access_token)
        response = self.client.post(
            reverse("auth:logout"),
            None,
            HTTP_AUTHORIZATION="Bearer " + access,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("refresh", response.data)

    def test_logout_unauthorized(self):
        response = self.client.post(reverse("auth:logout"), None,)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_valid_refresh_user_token(self):
        data = {
            "refresh": str(self.user.refresh),
        }
        response = self.client.post(
            reverse("auth:refresh"), data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("access", response.data)

    def test_invalid_refresh_user_token(self):
        data = {"refresh": "not_valid"}
        response = self.client.post(
            reverse("auth:refresh"), data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertIn("detail", response.data)


class ChangePasswordTest(AuthorizedTestRequestsMixin, APITestCase):
    def setUp(self) -> None:
        self.user = self.get_new_user()
        self.user.set_password("test_pass")
        self.user.save()

    def test_successful(self):
        data = {
            "old_password": "test_pass",
            "new_password": "test_pass2",
        }
        response = self._authorized_post(
            reverse("auth:password_change"), data, user=self.user
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password("test_pass2"))

    def test_wrong_old_password(self):
        data = {
            "old_password": "wrong_pass",
            "new_password": "test_pass2",
        }
        response = self._authorized_post(
            reverse("auth:password_change"), data, user=self.user
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("old_password", response.data)

    def test_unauthorized(self):
        data = {
            "old_password": "wrong_pass",
            "new_password": "test_pass2",
        }
        response = self.client.post(reverse("auth:password_change"), data,)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class EmailConfirmationTest(AuthorizedTestRequestsMixin, APITestCase):
    def setUp(self) -> None:
        self.user = self.get_new_user()

    @patch("django_apps.users.tasks.send_email_confirmation.delay")
    def test_send_confirmation(self, send_email_confirmation_task):
        response = self._authorized_post(
            reverse("auth:send-email-confirmation"), None, user=self.user,
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        send_email_confirmation_task.assert_called_with(
            self.user.email.email, self.user.email._key
        )

    def test_send_confirmation_task(self):
        send_email_confirmation("some_email@gmail.com", "some_code")
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "Email confirmation")
        self.assertEqual(mail.outbox[0].to, ["some_email@gmail.com"])

    def test_send_confirmation_unauthorized(self):
        response = self.client.post(
            reverse("auth:send-email-confirmation"), None,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_email_confirmation(self):
        token = self.user.email._key

        self.assertFalse(self.user.email.is_verified)

        response = self._authorized_post(
            reverse("auth:confirm-email", kwargs={"token": token}),
            None,
            user=self.user,
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertTrue(self.user.email.is_verified)

    def test_email_confirmation_wrong_token(self):
        self.assertFalse(self.user.email.is_verified)

        response = self._authorized_post(
            reverse("auth:confirm-email", kwargs={"token": "wrong_token"}),
            None,
            user=self.user,
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.user.refresh_from_db()
        self.assertFalse(self.user.email.is_verified)


# noinspection PyShadowingNames
class PasswordResetTest(AuthorizedTestRequestsMixin, APITestCase):
    def setUp(self) -> None:
        self.user = self.get_new_user()

    @patch("django_apps.users.tasks.send_password_reset.delay")
    def test_reset_password_flow(self, send_password_reset):
        response = self.client.post(
            reverse("auth:password_reset:reset-password-request"),
            {"email": self.user.email.email},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(send_password_reset.call_count, 1)
        self.assertEqual(
            self.user.email.email, send_password_reset.call_args.args[0],
        )
        code = send_password_reset.call_args.args[1]

        response = self.client.post(
            reverse("auth:password_reset:reset-password-confirm"),
            {
                "email": self.user.email.email,
                "token": code,
                "password": "some_new_password",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password("some_new_password"))

    def test_reset_password_email_task(self):
        send_password_reset("some_email@gmail.com", "some_code")
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "Password reset")
        self.assertEqual(mail.outbox[0].to, ["some_email@gmail.com"])


class ProfileTest(AuthorizedTestRequestsMixin, APITestCase):
    def setUp(self) -> None:
        self.user = self.get_new_user()

    def test_initialization(self):
        new_user = self.get_new_user()
        self.assertTrue(new_user.profile)

    def test_user_profile_update(self):
        response = self._authorized_patch(
            reverse("auth:profile"),
            {
                "description": "new_one",
                "first_name": "new_first_name",
                "last_name": "new_last_name"
            },
            user=self.user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)
        self.assertEqual(response.data["description"], "new_one")
        self.assertEqual(response.data["first_name"], "new_one")
        self.assertEqual(response.data["last_name"], "new_last_name")

    def test_user_profile_update_unauthorized(self):
        response = self.client.post(
            reverse("auth:profile"), {"description": "some_text"}
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ProfileSerializerTest(TestCase):
    def setUp(self) -> None:
        self.user = CustomUser.objects.create_user(
            email="test_email@gmail.com",
            username="test",
            password=123,
        )

    def test_serialization(self):
        serialized_profile = ProfileSerializer(
            self.user.profile
        ).data
        self.assertEqual(serialized_profile["id"], self.user.id)
        self.assertIn("description", serialized_profile)
        self.assertIn("fist_name", serialized_profile)
        self.assertIn("last_name", serialized_profile)


class BackendTest(APITestCase):
    def test_user_authentication_using_email(self):
        user = CustomUser.objects.create_user(
            username="test_username",
            email="test3@gmail.com",
            password="test"
        )
        self.assertEqual(
            authenticate(request=object(), username="test_username", password="test"),
            user
        )

    def test_user_authentication_using_username(self):
        user = CustomUser.objects.create_user(
            email="test4@gmail.com",
            password="test"
        )
        self.assertEqual(
            authenticate(request=object(), username="test4@gmail.com", password="test"),
            user
        )
