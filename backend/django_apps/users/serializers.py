from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.password_validation import (
    validate_password,
    get_password_validators,
)
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.exceptions import TokenBackendError, TokenError
from rest_framework_simplejwt.tokens import RefreshToken

from ..users.models import EmailAddress, Profile, CustomUser
from backend import settings


class EmailAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailAddress
        fields = ["email", "is_verified"]
        read_only_fields = ["is_verified"]


class CustomUserSerializer(serializers.ModelSerializer):
    refresh = serializers.SerializerMethodField()
    email = serializers.EmailField(required=True)

    def validate_email(self, email):
        """
        Ensures provided email is globally unique

        :param email:
        :return:
        """
        if EmailAddress.objects.filter(email=email).exists():
            raise ValidationError(
                _("Custom user with such email already exists")
            )
        return email

    def get_refresh(self, user):
        """
        Returns refresh token for user

        :param user:
        :return:
        """
        return RefreshToken.for_user(user)

    class Meta:
        model = get_user_model()
        fields = ["id", "username", "email", "password", "refresh"]
        read_only_fields = ["id"]
        extra_kwargs = {
            "password": {"write_only": True},
            "username": {"required": False}
        }

    def create(self, validated_data):
        """
        Creates user with validated_data

        :returns: New user object
        :rtype: :class:`.CustomUser`
        """
        return CustomUser.objects.create_user(
            **dict(validated_data)
        )

    def update(self, user, validated_data):
        """
        Updates user with validated_data

        :returns: updated user object
        :rtype: :class:`.CustomUser`
        """
        email = validated_data.pop("email", None)
        if email is not None:
            user.email.email = email
        return super(CustomUserSerializer, self).update(user, validated_data)

    def to_representation(self, instance):
        """
        Replace raw email with corresponding serialized email instance.
        Add refresh and access tokens to representation.

        :param instance: User
        :type instance: :class:`.CustomUser`
        :rtype: dict
        """
        ret = super().to_representation(instance)
        ret["email"] = EmailAddressSerializer(instance.email).data
        ret["access"] = str(ret["refresh"].access_token)
        ret["refresh"] = str(ret["refresh"])
        return ret


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    def validate_refresh(self, refresh):
        """
        Checks if the provided token is valid.

        :raises ValidationError: if the token is
        not valid (or expired).
        :param refresh: refresh token
        :type refresh: str
        :return: refresh
        """
        try:
            RefreshToken(refresh)
        except (TokenError, TokenBackendError) as e:
            error_message = e.args[0]
            raise ValidationError(error_message)
        return refresh

    def save(self, **kwargs):
        """
        Blacklists provided token.
        (No more valid for getting access token with it)

        :rtype: None
        """
        validated_data = self.validated_data
        token = validated_data.pop("refresh")

        token = RefreshToken(token)
        token.blacklist()


class ProfileSerializer(serializers.ModelSerializer):

    id = serializers.SerializerMethodField()

    def get_id(self, profile):
        return profile.user.id

    class Meta:
        model = Profile
        fields = [
            "id",
            "description",
            "first_name",
            "last_name"
        ]


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(max_length=127, write_only=True)
    new_password = serializers.CharField(max_length=127, write_only=True)

    def validate_old_password(self, old_password):
        """
        Ensure the old password is correct

        :return: old_password
        """
        user = self.context["request"].user
        if not user.check_password(old_password):
            raise ValidationError(
                _("Passwords didn't match")
            )
        return old_password

    def validate_new_password(self, new_password):
        """
        Validate new password using validators from settings file.

        :raises ValidationError: if password didn't pass the validation
        :param new_password:
        :return: new_password
        """
        validate_password(
            new_password,
            user=self.context["request"].user,
            password_validators=get_password_validators(
                getattr(settings, "AUTH_PASSWORD_VALIDATORS", [])
            ),
        )
        return new_password

    def create(self, validated_data):
        """
        Create is restricted

        :raises ValidationError:
        """
        raise NotImplementedError

    def update(self, user, validated_data):
        """
        Changes user password

        :return: user
        :rtype: :class:`.CustomUser`
        """
        user.set_password(validated_data["new_password"])
        user.save()
        return user
