from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError


class NewPermissionValidator:
    requires_context = True

    def __call__(self, permission_dict, serializer):
        """
        Ensures provided target is either None or
        associated group's role.

        Check if user has all the permissions, he is trying
        to add to the role.

        :param permission_dict:
        :param serializer:
        :raises ValidationError: If target is incorrect, or user
        hasn't got permissions, he is trying to assign
        :return:
        """
        user = serializer.context["user"]
        group = serializer.context["group"]

        permission = permission_dict["permission"]
        # Target is role object or none
        target = permission_dict.get("content_object", None)

        if target and target.social_group != group:
            raise ValidationError(_("Target must be a group's role!"))

        if not user == group.owner:
            if target is None:
                target = group

            if not user.has_perm(permission.codename, target):
                raise ValidationError(
                    _(
                        "In order to edit a permission, "
                        "you need to have it first."
                        f"You don't have '{permission.codename}' permission."
                    )
                )
