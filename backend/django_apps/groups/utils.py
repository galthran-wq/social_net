from .models import Group


def extract_group_from_post_request(request):
    return Group.objects.get(pk=request.data["group"])
