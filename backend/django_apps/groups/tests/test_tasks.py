from unittest.mock import patch

from django.urls import reverse
from rest_framework import status

from ...groups.models import Post
from ...groups.tasks import submit_post
from ...groups.tests.test_views import GroupsAPITestCase


class AsyncTasksTest(GroupsAPITestCase):
    def setUp(self):
        super(AsyncTasksTest, self).setUp()
        self.post = Post.objects.create(
            group=self.group, title="some_title", body="some_body"
        )

    @patch("django_apps.groups.tasks.submit_post.apply_async")
    def test_post_schedule(self, submit_post_task):
        self.assertEqual(self.post.is_published, False)
        self.assertEqual(self.post.publishing_datetime, None)

        response = self._authorized_post(
            reverse("groups:post-schedule", kwargs={"post_id": self.post.id},),
            {"delay_in_minutes": 60, },
            user=self.owner,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertHasEveryField(Post, response.data)
        self.assertFalse(response.data["is_published"])
        self.assertTrue(response.data["publishing_datetime"])
        submit_post_task.assert_called_with(
            args=[self.post.id], countdown=60 * 60
        )

    def test_submit_post_task(self):
        self.assertEqual(self.post.is_published, False)
        submit_post(self.post.id)
        self.post.refresh_from_db()
        self.assertEqual(self.post.is_published, True)
