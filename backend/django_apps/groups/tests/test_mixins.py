from unittest.mock import patch

from django.test import TestCase
from rest_framework.settings import api_settings
from rest_framework.test import APIRequestFactory

from ..mixins.views import ShortenFieldMixin
from ..serializers import GroupSerializer
from ...groups.models import Post, Group
from ...users.models import CustomUser, EmailAddress
from ...users.tests import AuthorizedTestRequestsMixin


class RateableMixinTest(TestCase):
    def setUp(self) -> None:
        self.rateable = Post.objects.create(
            title="some_title",
            group=Group.objects.create(title="test_Test"),
            body="some_body",
        )
        self.user = CustomUser.objects.create(
            email=EmailAddress.objects.create(email="gmail@gmail.com",),
            username="some_username",
        )
        self.user.set_password("test_test")
        self.user.save()

    def test_like(self):
        """Checks if user can like, cancel his rate"""
        self.assertEqual(self.rateable.disliked_by.count(), 0)
        self.assertEqual(self.rateable.liked_by.count(), 0)

        # like
        self.rateable.like(self.user)
        self.assertFalse(self.rateable.disliked_by.count())
        self.assertEqual(self.rateable.liked_by.count(), 1)
        self.assertEqual(self.rateable.liked_by.get(), self.user)

        # cancel like
        self.rateable.like(self.user)
        self.assertFalse(self.rateable.disliked_by.exists())
        self.assertFalse(self.rateable.liked_by.exists())

    def test_dislike(self):
        """Checks if user can like, cancel his rate"""
        self.assertEqual(self.rateable.disliked_by.count(), 0)
        self.assertEqual(self.rateable.liked_by.count(), 0)

        # dislike
        self.rateable.dislike(self.user)
        self.assertFalse(self.rateable.liked_by.count())
        self.assertEqual(self.rateable.disliked_by.count(), 1)
        self.assertEqual(self.rateable.disliked_by.get(), self.user)

        # cancel dislike
        self.rateable.dislike(self.user)
        self.assertFalse(self.rateable.liked_by.exists())
        self.assertFalse(self.rateable.disliked_by.exists())

    def test_sequential(self):
        # like -> dislike = assert disliked
        self.rateable.like(self.user)
        self.rateable.dislike(self.user)
        self.assertFalse(self.rateable.liked_by.count())
        self.assertEqual(self.rateable.disliked_by.count(), 1)
        self.assertEqual(self.rateable.disliked_by.get(), self.user)
        # dislike -> like = assert disliked
        self.rateable.dislike(self.user)
        self.rateable.like(self.user)
        self.assertEqual(self.rateable.liked_by.count(), 1)
        self.assertFalse(self.rateable.disliked_by.count())
        self.assertEqual(self.rateable.liked_by.get(), self.user)


class PostableMixinTest(TestCase):
    def setUp(self) -> None:
        self.postable = Post.objects.create(
            title="some_title",
            group=Group.objects.create(title="test_Test"),
            body="some_body",
        )
        self.user = CustomUser.objects.create(
            email=EmailAddress.objects.create(email="gmail@gmail.com",),
            username="some_username",
        )
        self.user.set_password("test_test")
        self.user.save()

    def test_create_and_update_marks(self):
        created_at_mark = self.postable.created_at
        updated_at_mark = self.postable.updated_at
        self.postable.save()
        self.assertNotEqual(self.postable.updated_at, updated_at_mark)
        self.assertEqual(self.postable.created_at, created_at_mark)


class SchedulableMixinTest(AuthorizedTestRequestsMixin, TestCase):
    def setUp(self) -> None:
        self.author = self.get_new_user()
        self.group = Group.objects.create(
            owner=self.author, title="some_title"
        )
        self.post = Post.objects.create(
            group=self.group,
            author=self.author,
            title="some_title",
            body="some_body",
            is_published=False,
        )

    @patch("django_apps.groups.tasks.submit_post.apply_async")
    def test_schedule(self, submit_post):
        delay_in_min = 3
        self.post.schedule(delay_in_min)
        submit_post.assert_called_with(
            args=[self.post.id], countdown=60 * delay_in_min
        )
        self.post.is_published = True
        self.post.save()

        self.post.schedule(delay_in_min)
        submit_post.assert_called_with(
            args=[self.post.id], countdown=60 * delay_in_min
        )
        self.assertFalse(self.post.is_published)

    def test_publish(self):
        self.post.publish()
        self.assertTrue(self.post.is_published)


class ShortenFieldMixinTest(AuthorizedTestRequestsMixin,
                            TestCase):
    def test_shorten_field(self):
        owner = self.get_new_user()
        request = APIRequestFactory().get('/')
        request.user = owner
        context = {"request": request}

        shorten_field = ShortenFieldMixin.shorten_field
        page_size = api_settings.PAGE_SIZE

        for i in range(20):
            Group.objects.create(
                title="some_title",
                owner=owner
            )

        shortened_field = shorten_field(
            Group.objects.all(),
            GroupSerializer,
            context=context
        )

        self.assertEqual(shortened_field["count"], 20)
        self.assertEqual(
            shortened_field["results"],
            GroupSerializer(
                Group.objects.all()[:page_size],
                many=True,
                context=context
            ).data
        )



