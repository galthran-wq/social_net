from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory

from ...groups.filters import RoleFilter, PostFilter
from ...groups.models import Group, Role, Post
from ...groups.views import RoleViewSet, PostViewSet
from ...users.models import CustomUser, EmailAddress


class RoleFilterTest(TestCase):
    def setUp(self) -> None:
        self.filter = RoleFilter()

        self.owner = CustomUser.objects.create(
            email=EmailAddress.objects.create(email="gmail@gmail.com",),
            username="some_username",
        )
        self.owner.set_password("123_test_123")
        self.owner.save()
        self.group = Group.objects.create(title="test", owner=self.owner)
        factory = APIRequestFactory()
        self.request = factory.get(
            reverse("groups:role-list", kwargs={"group_id": self.group.id})
        )
        self.view = RoleViewSet()
        self.view.kwargs = {RoleViewSet.group_lookup_url_kwarg: self.group.id}

        for i in range(10):
            Role.objects.create(name=f"test_role_{i}", social_group=self.group)
        self.role_queryset = self.group.roles.all()

        self.default_user = CustomUser.objects.create(
            email=EmailAddress.objects.create(email="gmail2@gmail.com",),
            username="some_username2",
        )
        self.default_user.set_password("123_test_123")
        self.default_user.save()

    def test_owner_filtering(self):
        """should return the same queryset as passed"""
        self.request.user = self.owner
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.role_queryset, self.view
        )
        self.assertEqual(list(filtered_queryset), list(self.role_queryset))

    def test_default_role_filtering(self):
        self.request.user = self.default_user
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.role_queryset, self.view
        )
        # assert initially default user sees nothing
        self.assertEqual(list(filtered_queryset), [])

        # give permission to everyone to see every role
        for role in self.role_queryset:
            self.group.get_default_role().edit_permission(
                "view_role", target=role
            )

        self.request.user = self.default_user
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.role_queryset, self.view
        )
        self.assertEqual(list(filtered_queryset), list(self.role_queryset))

    def test_personal_role_filtering(self):
        private_role = Role.objects.create(
            name="private_role", social_group=self.group
        )
        private_role.edit_member(self.default_user)

        viewable_roles = []
        for index, role in enumerate(self.role_queryset):
            if index % 2:
                viewable_roles.append(role)
                private_role.edit_permission("view_role", target=role)

        self.request.user = self.default_user
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.role_queryset, self.view
        )
        self.assertEqual(list(filtered_queryset), viewable_roles)


class PostFilterTest(TestCase):
    def setUp(self) -> None:
        self.filter = PostFilter()

        self.author = CustomUser.objects.create(
            email=EmailAddress.objects.create(email="test_test_@gmail.com"),
            username="some_username3",
        )
        self.author.set_password("123_test_123")
        self.author.save()

        self.owners = []
        self.groups = []
        for i in range(10):
            owner = CustomUser.objects.create(
                email=EmailAddress.objects.create(
                    email=f"{i}gmail@gmail.com",
                ),
                username=f"{i}some_username",
            )
            owner.set_password("123_test_123")
            owner.save()

            group = Group.objects.create(title="test", owner=owner)
            for j in range(10):
                Post.objects.create(
                    author=self.author,
                    title=f"test_role_{j}",
                    body="some_body",
                    group=group,
                    is_published=True if j % 2 else False,
                )

            self.owners.append(owner)
            self.groups.append(group)

        factory = APIRequestFactory()
        self.request = factory.get(reverse("groups:post-list"))
        self.view = PostViewSet()

        self.init_queryset = Group.objects.all()

        self.default_user = CustomUser.objects.create(
            email=EmailAddress.objects.create(email="gmail2@gmail.com",),
            username="some_username2",
        )
        self.default_user.set_password("123_test_123")
        self.default_user.save()

    def test_owner_list(self):
        for owner in self.owners:
            self.request.user = owner
            filtered_queryset = self.filter.filter_queryset(
                self.request, self.init_queryset, self.view
            )
            self.assertEqual(
                list(filtered_queryset),
                list(Post.objects.filter(group__owner=owner).all()),
            )

    def test_default_role(self):
        self.groups[0].get_default_role().edit_permission("group_view_post")
        self.request.user = self.default_user
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.init_queryset, self.view
        )
        self.assertEqual(
            list(filtered_queryset),
            list(self.groups[0].posts.filter(is_published=True)),
        )

        # add unpublished
        self.groups[0].get_default_role().edit_permission(
            "group_view_unpublished_post"
        )
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.init_queryset, self.view
        )
        self.assertCountEqual(
            list(filtered_queryset), list(self.groups[0].posts.all())
        )

        # allow all group's users to see everything
        for group in self.groups[1:]:
            group.get_default_role().edit_permission(
                "group_view_post", "group_view_unpublished_post"
            )
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.init_queryset, self.view
        )
        self.assertCountEqual(
            list(filtered_queryset), list(Post.objects.all())
        )

    def test_post_list(self):
        expected_list = []
        self.request.user = self.default_user
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.init_queryset, self.view
        )
        self.assertEqual(list(filtered_queryset), expected_list)

        for index, group in enumerate(self.groups):
            role = Role.objects.create(name=f"test_role{index}", social_group=group)
            role.edit_permission("group_view_unpublished_post")
            role.edit_member(self.default_user)
            expected_list += list(group.posts.filter(is_published=False))

            filtered_queryset = self.filter.filter_queryset(
                self.request, self.init_queryset, self.view
            )
            self.assertCountEqual(list(filtered_queryset), expected_list)

            role.edit_permission("group_view_post")
            expected_list += list(group.posts.filter(is_published=True))

            filtered_queryset = self.filter.filter_queryset(
                self.request, self.init_queryset, self.view
            )
            self.assertCountEqual(list(filtered_queryset), expected_list)

    def test_author_list(self):
        self.request.user = self.author
        filtered_queryset = self.filter.filter_queryset(
            self.request, self.init_queryset, self.view
        )
        self.assertEqual(list(filtered_queryset), [])
