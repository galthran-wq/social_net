from rest_framework.test import APITestCase

from ...groups.models import Group


class SignalsTest(APITestCase):
    def test_initialize_roles(self):
        new_group = Group(title="test_group",)
        self.assertEqual(new_group.roles.count(), 0)
        new_group.save()
        self.assertEqual(new_group.roles.count(), 3)
        self.assertTrue(
            new_group.roles.get(name=f"{new_group.id}_follower_role")
        )
        self.assertTrue(
            new_group.roles.get(name=f"{new_group.id}_default_role")
        )
        self.assertTrue(
            new_group.roles.get(name=f"{new_group.id}_blacklist_role")
        )
