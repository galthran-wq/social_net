from unittest.mock import patch

from django.test import TestCase
from rest_framework.test import APIRequestFactory

from ...groups.models import Group, Role, Post, PostComment
from ...groups.serializers import (
    ObjectPermissionSerializer,
    GroupSerializer,
    FollowGroupSerializer,
    LeaveGroupSerializer,
    RequestFollowGroupSerializer,
    ApproveFollowGroupSerializer,
    CreatePostSerializer,
    RoleSerializer,
    PostCommentSerializer,
    PostScheduleSerializer,
    UpdatePostSerializer,
)
from ...users.tests import AuthorizedTestRequestsMixin


class GetSerializerMixin(AuthorizedTestRequestsMixin):

    serializer_cls = CreatePostSerializer

    def get_serializer(self, *args, _user=None, context=None, **kwargs):
        if _user is None:
            _user = self.get_new_user()

        request = APIRequestFactory().get('/')
        request.user = _user
        actual_context = {"request": request}
        if context is not None:
            actual_context.update(context)
        return self.serializer_cls(
            *args,
            context=actual_context,
            **kwargs
        )


class BaseGroupSerializerTest(GetSerializerMixin,
                              TestCase):
    serializer_cls = GroupSerializer

    def setUp(self) -> None:
        self.owner = self.get_new_user()
        self.group = Group.objects.create(
            title="some_title",
            description="some_description",
            owner=self.owner,
        )
        self.default_user = self.get_new_user()

    def test_serialization(self):
        data = self.get_serializer(self.group).data
        self.assertEqual(data["id"], self.group.id)
        self.assertEqual(data["title"], "some_title")
        self.assertEqual(data["description"], "some_description")
        self.assertEqual(data["followers"]["results"][0]["id"], self.owner.id)
        self.assertIn("description", data["followers"]["results"][0])
        self.assertEqual(data["follow_queue"]["count"], 0)
        self.assertEqual(data["owner"]["id"], self.owner.id)
        self.assertIn("description", data["owner"])
        self.assertIn("is_following", data)
        self.assertIn("is_follow_requested", data)
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 10)


class GroupSerializerTest(BaseGroupSerializerTest):
    def test_deserialization(self):
        data = {
            "title": "brand_new_title",
            "description": "some_description",
        }
        data2 = {
            "title": "brand_new_title",
        }
        for i, data in enumerate([data, data2]):
            serializer = self.get_serializer(data=data, _user=self.owner)
            self.assertTrue(serializer.is_valid())
            serializer.save()
            data = serializer.data
            self.assertEqual(data["id"], self.group.id + 1 + i)
            self.assertEqual(data["title"], data["title"])
            self.assertEqual(
                data["description"], data.get("description", None)
            )
            self.assertEqual(data["followers"]["results"][0]["id"], self.owner.id)
            self.assertEqual(data["follow_queue"]["count"], 0)
            self.assertEqual(data["owner"]["id"], self.owner.id)
            self.assertIn("description", data["owner"])
            self.assertIn("is_following", data)
            self.assertIn("is_follow_requested", data)
            self.assertIn("created_at", data)
            self.assertIn("updated_at", data)
            self.assertEqual(len(data), 10)

    def test_invalid_deserialization(self):
        data = {
            "id": 111,
            "followers": [2, 3],
            "follow_queue": [],
            "owner": 333,
            "created_at": "string",
            "updated_at": "string",
        }

        serializer = self.get_serializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn("title", serializer.errors)

        data = {
            "title": "added_title",
            "id": 111,
            "followers": [2, 3],
            "follow_queue": [],
            "owner": 333,
            "created_at": "string",
            "updated_at": "string",
        }
        serializer = self.get_serializer(data=data, _user=self.owner)
        self.assertTrue(serializer.is_valid())  # assert worked
        serializer.save()
        data = serializer.data

        self.assertEqual(data["id"], self.group.id + 1)
        self.assertEqual(data["title"], "added_title")
        self.assertEqual(data["description"], None)
        self.assertEqual(data["followers"]["results"][0]["id"], self.owner.id)
        self.assertEqual(data["follow_queue"]["count"], 0)
        self.assertEqual(data["owner"]["id"], self.owner.id)
        self.assertIn("description", data["owner"])
        self.assertIn("is_following", data)
        self.assertIn("is_follow_requested", data)
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 10)


class FollowGroupSerializerTest(BaseGroupSerializerTest):
    serializer_cls = FollowGroupSerializer

    def test_update(self):
        data = {
            "user": self.default_user.id,
            # ensure nothing below will work
            "id": 111,
            "followers": [2, 3],
            "follow_queue": [2, 3],
            "owner": 333,
            "created_at": "string",
            "updated_at": "string",
        }
        serializer = self.get_serializer(self.group, data=data)
        serializer.is_valid()
        serializer.save()

        data = serializer.data
        self.assertEqual(data["id"], self.group.id)
        self.assertEqual(data["title"], "some_title")
        self.assertEqual(data["description"], "some_description")
        self.assertEqual(data["followers"]["results"][0]["id"], self.owner.id)
        self.assertEqual(
            data["followers"]["results"][1]["id"],
            self.default_user.id
        )
        self.assertEqual(data["follow_queue"]["count"], 0)
        self.assertEqual(data["owner"]["id"], self.owner.id)
        self.assertIn("description", data["owner"])
        self.assertIn("is_following", data)
        self.assertIn("is_follow_requested", data)
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 10)

    def test_invalid_update(self):
        data = {
            "user": "wrong_data",
            "id": 111,
            "followers": [2, 3],
            "follow_queue": [],
            "owner": 333,
            "created_at": "string",
            "updated_at": "string",
        }
        serializer = self.get_serializer(self.group, data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn("user", serializer.errors)
        self.assertEqual(len(serializer.errors), 1)

        # already exists
        data = {
            "user": self.owner.id,
            "id": 111,
            "followers": [2, 3],
            "follow_queue": [],
            "owner": 333,
            "created_at": "string",
            "updated_at": "string",
        }
        serializer = self.serializer_cls(self.group, data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn("user", serializer.errors)
        self.assertEqual(len(serializer.errors), 1)


class LeaveGroupSerializerTest(BaseGroupSerializerTest):
    serializer_cls = LeaveGroupSerializer

    def test_update(self):
        self.group.edit_follower(self.default_user)
        data = {
            "user": self.default_user.id,
            # ensure nothing below will work
            "id": 111,
            "followers": [2, 3],
            "follow_queue": [2, 3],
            "owner": 333,
            "created_at": "string",
            "updated_at": "string",
        }

        serializer = self.get_serializer(
            self.group,
            data=data,
            _user=self.owner
        )
        serializer.is_valid()
        serializer.save()

        data = serializer.data
        self.assertEqual(data["id"], self.group.id)
        self.assertEqual(data["title"], "some_title")
        self.assertEqual(data["description"], "some_description")
        self.assertEqual(len(data["followers"]["results"]), 1)
        self.assertEqual(data["followers"]["results"][0]["id"], self.owner.id)
        self.assertEqual(data["follow_queue"]["count"], 0)
        self.assertEqual(data["owner"]["id"], self.owner.id)
        self.assertIn("description", data["owner"])
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 9)

    def test_invalid_update(self):
        for user in [
            "wrong_data",
            self.default_user.id,
            self.owner.id,
        ]:
            data = {
                "user": user,
                "id": 111,
                "followers": [2, 3],
                "follow_queue": [],
                "owner": 333,
                "created_at": "string",
                "updated_at": "string",
            }
            serializer = self.get_serializer(self.group, data=data)
            self.assertFalse(serializer.is_valid())
            self.assertIn("user", serializer.errors)
            self.assertEqual(len(serializer.errors), 1)


class RequestFollowGroupSerializerTest(BaseGroupSerializerTest):
    serializer_cls = RequestFollowGroupSerializer

    def test_update(self):
        data = {
            "user": self.default_user.id,
            # ensure nothing below will work
            "id": 111,
            "followers": [2, 3],
            "follow_queue": [2, 3],
            "owner": 333,
            "created_at": "string",
            "updated_at": "string",
        }

        serializer = self.get_serializer(self.group, data=data)
        serializer.is_valid()
        serializer.save()

        data = serializer.data
        self.assertEqual(data["id"], self.group.id)
        self.assertEqual(data["title"], "some_title")
        self.assertEqual(data["description"], "some_description")
        self.assertEqual(data["followers"]["results"][0]["id"], self.owner.id)
        self.assertEqual(data["follow_queue"]["count"], 1)
        self.assertEqual(
            data["follow_queue"]["results"][0]["id"],
            self.default_user.id
        )
        self.assertEqual(data["owner"]["id"], self.owner.id)
        self.assertIn("description", data["owner"])
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertIn("is_following", data)
        self.assertIn("is_follow_requested", data)
        self.assertEqual(len(data), 10)

    def test_invalid_update(self):
        self.group.edit_follower(self.default_user)
        for user in [
            "wrong_data",
            self.default_user.id,
            self.owner.id,
        ]:
            data = {
                "user": user,
                "id": 111,
                "followers": [2, 3],
                "follow_queue": [],
                "owner": 333,
                "created_at": "string",
                "updated_at": "string",
            }
            serializer = self.get_serializer(self.group, data=data)
            self.assertFalse(serializer.is_valid())
            self.assertIn("user", serializer.errors)
            self.assertEqual(len(serializer.errors), 1)


class ApproveFollowGroupSerializerTest(BaseGroupSerializerTest):
    serializer_cls = ApproveFollowGroupSerializer

    def test_update(self):
        self.group.request_follow(self.default_user)
        data = {
            "users": [self.default_user.id],
            # ensure nothing below will work
            "id": 111,
            "followers": [2, 3],
            "follow_queue": [2, 3],
            "owner": 333,
            "created_at": "string",
            "updated_at": "string",
        }

        serializer = self.get_serializer(self.group, data=data)
        serializer.is_valid()
        serializer.save()

        data = serializer.data
        self.assertEqual(data["id"], self.group.id)
        self.assertEqual(data["title"], "some_title")
        self.assertEqual(data["description"], "some_description")
        self.assertEqual(len(data["followers"]), 2)
        self.assertEqual(
            {
                data["followers"]["results"][0]["id"],
                data["followers"]["results"][1]["id"]
            },
            {self.default_user.id, self.owner.id},
        )
        self.assertEqual(data["owner"]["id"], self.owner.id)
        self.assertIn("description", data["owner"])
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 10)

    def test_invalid_update(self):
        for user in [
            "wrong_data",
            self.default_user.id,
            self.owner.id,
        ]:
            data = {
                "user": user,
                "id": 111,
                "followers": [2, 3],
                "follow_queue": [],
                "owner": 333,
                "created_at": "string",
                "updated_at": "string",
            }
            serializer = self.get_serializer(self.group, data=data)
            self.assertFalse(serializer.is_valid())
            self.assertIn("users", serializer.errors)
            self.assertEqual(len(serializer.errors), 1)


class PostSerializerTest(
    GetSerializerMixin,
    TestCase,
):
    serializer_cls = CreatePostSerializer
    update_serializer_cls = UpdatePostSerializer

    def setUp(self) -> None:
        self.owner = self.get_new_user()
        self.group = Group.objects.create(
            title="some_title",
            description="some_description",
            owner=self.owner,
        )
        self.post = Post.objects.create(
            title="test_title",
            body="must_body",
            group=self.group,
            author=self.owner,
        )
        self.default_user = self.get_new_user()

    def test_serialization(self):
        data = self.get_serializer(self.post).data
        self.assertEqual(data["id"], self.post.id)
        self.assertEqual(data["group"], self.group.id)
        self.assertEqual(data["title"], "test_title")
        self.assertEqual(data["body"], "must_body")
        self.assertEqual(data["author"]["id"], self.owner.id)
        self.assertIn('description', data["author"])
        self.assertEqual(data["publishing_datetime"], None)
        self.assertEqual(data["best_comment"], None)
        self.assertEqual(data["comments_amount"], 0)
        self.assertEqual(data["is_published"], False)
        self.assertEqual(data["liked_by"]["count"], 0)
        self.assertEqual(data["disliked_by"]["count"], 0)
        self.assertEqual(data["is_liked"], False)
        self.assertEqual(data["is_disliked"], False)
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 15)

    def test_best_comment_comments_amount_serialization(self):
        first_comment = PostComment.objects.create(
            post=self.post,
            body="123"
        )
        serialized_post = self.get_serializer(self.post).data
        self.assertEqual(serialized_post["best_comment"]["id"], first_comment.id)
        self.assertEqual(serialized_post["comments_amount"], 1)
        second_comment = PostComment.objects.create(
            post=self.post,
            body="123"
        )
        second_comment.liked_by.add(self.default_user)
        serialized_post = self.get_serializer(self.post).data
        self.assertEqual(serialized_post["best_comment"]["id"], second_comment.id)
        self.assertEqual(serialized_post["comments_amount"], 2)

    def test_deserialization(self):
        # try create without body and title
        data = {
            "title": "some_title",
            "body": "some_body",
            "group": self.group.id,
            # ensure nothing below will work
            "id": 111,
            "author": 333,
            "created_at": "string",
            "updated_at": "string",
            "publishing_datetime": "some_string",
            "is_published": True,
            "disliked_by": [1, 2, 3],
            "liked_by": [1, 2, 3],
        }
        serializer = self.get_serializer(data=data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        data = serializer.data

        self.assertEqual(data["id"], self.post.id + 1)
        self.assertEqual(data["group"], self.group.id)
        self.assertEqual(data["title"], "some_title")
        self.assertEqual(data["body"], "some_body")
        self.assertIn('description', data["author"])
        self.assertEqual(data["publishing_datetime"], None)
        self.assertEqual(data["best_comment"], None)
        self.assertEqual(data["comments_amount"], 0)
        self.assertEqual(data["is_published"], False)
        self.assertEqual(data["liked_by"]["count"], 0)
        self.assertEqual(data["disliked_by"]["count"], 0)
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 15)

    def test_invalid_deserialization(self):
        # try create without body and title
        data = {
            # ensure nothing below will work
            "id": 111,
            "author": 333,
            "created_at": "string",
            "updated_at": "string",
            "publishing_datetime": "some_string",
            "is_published": True,
            "disliked_by": [1, 2, 3],
            "liked_by": [1, 2, 3],
        }

        serializer = self.get_serializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn("title", serializer.errors)
        self.assertIn("body", serializer.errors)
        self.assertIn("group", serializer.errors)
        self.assertEqual(len(serializer.errors), 3)

    def test_update(self):
        data = {
            "title": "new_title",
            "body": "new_body",
            # ensure nothing below will work
            "group": 999,
            "id": 111,
            "author": 333,
            "created_at": "string",
            "updated_at": "string",
            "publishing_datetime": "some_string",
            "is_published": True,
            "disliked_by": [1, 2, 3],
            "liked_by": [1, 2, 3],
        }
        PostSerializerTest.serializer_cls = self.update_serializer_cls
        serializer = self.get_serializer(self.post, data=data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        data = serializer.data

        self.assertEqual(data["id"], self.post.id)
        self.assertEqual(data["group"], self.group.id)
        self.assertEqual(data["title"], "new_title")
        self.assertEqual(data["body"], "new_body")
        self.assertEqual(data["author"]["id"], self.owner.id)
        self.assertIn('description', data["author"])
        self.assertEqual(data["publishing_datetime"], None)
        self.assertEqual(data["best_comment"], None)
        self.assertEqual(data["comments_amount"], 0)
        self.assertEqual(data["is_published"], False)
        self.assertEqual(data["liked_by"]["count"], 0)
        self.assertEqual(data["disliked_by"]["count"], 0)
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 15)


class PostScheduleSerializerTest(AuthorizedTestRequestsMixin, TestCase):
    def setUp(self) -> None:
        self.owner = self.get_new_user()
        self.group = Group.objects.create(
            title="some_title",
            description="some_description",
            owner=self.owner,
        )
        self.post = Post.objects.create(
            title="test_title",
            body="must_body",
            group=self.group,
            author=self.owner,
            is_published=False,
        )
        self.default_user = self.get_new_user()

    @patch("django_apps.groups.mixins.models.Schedulable.schedule")
    def test_deserialization(self, schedule):
        data = {"delay_in_minutes": 30}
        serializer = PostScheduleSerializer(self.post, data=data)
        self.assertTrue(serializer.is_valid())
        post = serializer.save()
        self.assertFalse(post.is_published)
        schedule.assert_called_with(30)


class ObjectPermissionSerializerTest(TestCase):
    def setUp(self) -> None:
        self.group = Group.objects.create(title="some_title")
        self.role = Role.objects.create(name="some_role", social_group=self.group)
        self.role.edit_permission("group_view_post")
        self.role.edit_permission(
            "edit_role_users", target=self.group.get_default_role()
        )
        self.nontarget_permission = self.role.groupobjectpermission_set.get(
            permission__codename="group_view_post"
        )
        self.target_permission = self.role.groupobjectpermission_set.get(
            permission__codename="edit_role_users"
        )

    def test_serialization(self):
        serializer = ObjectPermissionSerializer(self.nontarget_permission)
        self.assertEqual(
            serializer.data, {"name": "group_view_post", "target": None},
        )

        serializer = ObjectPermissionSerializer(self.target_permission)
        self.assertEqual(
            serializer.data,
            {
                "name": "edit_role_users",
                "target": self.group.get_default_role().name,
            },
        )

    def test_deserialization(self):
        to_deserialize_nontarget = {
            "name": "group_view_post",
            "target": None,
        }
        serializer = ObjectPermissionSerializer(data=to_deserialize_nontarget)
        serializer.is_valid()
        self.assertEqual(serializer.data, to_deserialize_nontarget)

        to_deserialize_nontarget2 = {
            "name": "group_view_post",
        }
        serializer = ObjectPermissionSerializer(data=to_deserialize_nontarget2)
        serializer.is_valid()
        self.assertEqual(serializer.data, to_deserialize_nontarget)

        to_deserialize_target = {
            "name": "group_view_post",
            "target": self.group.get_default_role().name,
        }
        serializer = ObjectPermissionSerializer(data=to_deserialize_target)
        serializer.is_valid()
        self.assertEqual(serializer.data, to_deserialize_target)

    def test_invalid_deserialization(self):
        to_deserialize_nontarget = {
            "name": "does_not_exist",
            "target": None,
        }
        serializer = ObjectPermissionSerializer(data=to_deserialize_nontarget)
        self.assertFalse(serializer.is_valid())
        self.assertIn("name", serializer.errors)

        to_deserialize_nontarget2 = {
            "name": "group_view_post",
            "target": "does_not_exist",
        }
        serializer = ObjectPermissionSerializer(data=to_deserialize_nontarget2)
        self.assertFalse(serializer.is_valid())
        self.assertIn("target", serializer.errors)

        to_deserialize_target = {
            "name": "does_not_exist",
            "target": "does_not_exist",
        }
        serializer = ObjectPermissionSerializer(data=to_deserialize_target)
        self.assertFalse(serializer.is_valid())
        self.assertIn("target", serializer.errors)
        self.assertIn("name", serializer.errors)

        to_deserialize_target2 = {
            "name": "edit_role_users",
            "target": "does_not_exist",
        }
        serializer = ObjectPermissionSerializer(data=to_deserialize_target2)
        self.assertFalse(serializer.is_valid())
        self.assertIn("target", serializer.errors)


class RoleSerializerTest(AuthorizedTestRequestsMixin, TestCase):
    serializer_cls = RoleSerializer

    def setUp(self) -> None:
        self.owner = self.get_new_user()
        self.group = Group.objects.create(
            title="some_title",
            description="some_description",
            owner=self.owner,
        )
        self.role = Role.objects.create(name="some_role", social_group=self.group)
        self.default_user = self.get_new_user()

    def test_serialization(self):
        data = self.serializer_cls(self.role).data
        self.assertEqual(data["name"], "some_role")
        self.assertEqual(data["users"], [])
        self.assertEqual(data["group"], self.group.id)
        self.assertIn("permissions", data)
        self.assertEqual(len(data), 4)

    def test_deserialization(self):
        some_other_group = Group.objects.create(title="some_title")
        some_other_group.save()

        self.group.edit_follower(self.default_user)
        data = {
            "name": "some_role_name",
            "users": [self.default_user.id],
            # ensure nothing below will work
            "group": some_other_group.id,
            "id": 111,
        }
        serializer = self.serializer_cls(
            data=data,
            context={"user": self.default_user, "group": self.group},
        )
        self.assertTrue(serializer.is_valid())
        serializer.save()
        data = serializer.data

        self.assertEqual(data["name"], f"{self.group.id}_some_role_name_role")
        self.assertEqual(data["users"][0]["id"], self.default_user.id)
        self.assertIn('description', data["users"][0])
        self.assertEqual(len(data["users"]), 1)
        self.assertEqual(data["group"], self.group.id)
        self.assertEqual(data["permissions"], [])
        self.assertEqual(len(data), 4)

    def test_invalid_deserialization(self):
        some_other_group = Group.objects.create(title="some_title")
        some_other_group.save()

        data1 = {}
        serializer = self.serializer_cls(
            data=data1,
            context={"user": self.default_user, "group": self.group},
        )
        self.assertFalse(serializer.is_valid())
        self.assertIn("name", serializer.errors)
        self.assertEqual(len(serializer.errors), 1)
        # name already exist
        data2 = {"name": "some_role"}
        serializer = self.serializer_cls(
            data=data2,
            context={"user": self.default_user, "group": self.group},
        )
        self.assertFalse(serializer.is_valid())
        self.assertIn("name", serializer.errors)
        self.assertEqual(len(serializer.errors), 1)

        # users not following
        data3 = {"users": [self.default_user.id]}
        serializer = self.serializer_cls(
            data=data3,
            context={"user": self.default_user, "group": self.group},
        )
        self.assertFalse(serializer.is_valid())
        self.assertIn("users", serializer.errors)
        self.assertIn("name", serializer.errors)
        self.assertEqual(len(serializer.errors), 2)


class PostCommentSerializerTest(GetSerializerMixin, TestCase):
    serializer_cls = PostCommentSerializer

    def setUp(self) -> None:
        self.owner = self.get_new_user()
        self.group = Group.objects.create(
            title="some_title",
            description="some_description",
            owner=self.owner,
        )
        self.post = Post.objects.create(
            title="test_title",
            body="must_body",
            group=self.group,
            author=self.owner,
        )
        self.comment = PostComment.objects.create(
            post=self.post, author=self.owner, body="some_body",
        )
        self.default_user = self.get_new_user()

    def test_serialization(self):
        data = self.get_serializer(self.comment).data
        self.assertEqual(data["id"], self.comment.id)
        self.assertEqual(data["post"], self.post.id)
        self.assertEqual(data["body"], "some_body")
        self.assertEqual(data["author"]["id"], self.owner.id)
        self.assertIn("description", data["author"])
        self.assertEqual(data["liked_by"]["count"], 0)
        self.assertEqual(data["disliked_by"]["count"], 0)
        self.assertEqual(data["is_liked"], False)
        self.assertEqual(data["is_disliked"], False)
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 10)

    def test_deserialization(self):
        data = {
            "post": self.post.id,
            "body": "some_body",
            # ensure nothing below will work
            "id": 111,
            "author": 333,
            "created_at": "string",
            "updated_at": "string",
            "disliked_by": [1, 2, 3],
            "liked_by": [1, 2, 3],
        }
        serializer = self.get_serializer(
            data=data,
            context={
                "post": self.post,
            },
            _user=self.default_user
        )
        self.assertTrue(serializer.is_valid())
        serializer.save()
        data = serializer.data

        self.assertEqual(data["id"], self.comment.id + 1)
        self.assertEqual(data["post"], self.post.id)
        self.assertEqual(data["body"], "some_body")
        self.assertEqual(data["author"]["id"], self.default_user.id)
        self.assertIn("description", data["author"])
        self.assertEqual(data["liked_by"]["count"], 0)
        self.assertEqual(data["disliked_by"]["count"], 0)
        self.assertEqual(data["is_liked"], False)
        self.assertEqual(data["is_disliked"], False)
        self.assertIn("created_at", data)
        self.assertIn("updated_at", data)
        self.assertEqual(len(data), 10)

    def test_invalid_deserialization(self):
        # try create without body and title
        data = {
            # ensure nothing below will work
            "id": 111,
            "author": 333,
            "created_at": "string",
            "updated_at": "string",
            "disliked_by": [1, 2, 3],
            "liked_by": [1, 2, 3],
        }

        serializer = self.serializer_cls(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn("body", serializer.errors)
        self.assertEqual(len(serializer.errors), 1)
