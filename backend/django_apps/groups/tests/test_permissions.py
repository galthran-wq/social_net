from django.http import Http404
from django.urls import reverse
from rest_framework.test import APIRequestFactory

from .test_views import GroupsAPITestCase
from ..models import Post
from ...groups.permissions import (
    GroupPermissions,
    FollowPermissions,
    ApproveFollowPermissions,
    CommentPermissions,
)
from ...groups.views import GroupViewSet, PostCommentViewSet

HTTP_METHODS = (
    "GET",
    "OPTIONS",
    "HEAD",
    "POST",
    "PUT",
    "PATCH",
    "DELETE",
)
SAFE_METHODS = ("GET", "HEAD", "OPTIONS")
DETAIL_METHODS = [
    method
    for method in HTTP_METHODS
    if method not in SAFE_METHODS and method != "POST"
]


class BasePermissionsTest(GroupsAPITestCase):
    permission_cls = None
    reverse_url = None
    view_cls = None

    @staticmethod
    def get_reverse_kwargs():
        return None

    def setUp(self) -> None:
        super(BasePermissionsTest, self).setUp()
        request_factory = APIRequestFactory()
        self.requests = {
            method_name: getattr(request_factory, method_name.lower())(
                reverse(self.reverse_url, kwargs=self.get_reverse_kwargs())
            )
            for method_name in HTTP_METHODS
        }
        self.view = self.view_cls()
        self.permission_object = self.permission_cls()


class GroupPermissionsTest(BasePermissionsTest):

    permission_cls = GroupPermissions
    reverse_url = "groups:group-list"
    view_cls = GroupViewSet

    def test_has_permission(self):
        # ensure every user can read group
        for user in [
            self.owner,
            self.default_user,
            self.anonymous_user,
        ]:
            for method, request in self.requests.items():
                request.user = user
                if method in SAFE_METHODS:
                    self.assertTrue(
                        self.permission_object.has_permission(
                            request, self.view
                        )
                    )
        # ensure authorized user can write group
        for user in [self.owner, self.default_user]:
            for method, request in self.requests.items():
                request.user = user
                if method not in SAFE_METHODS:
                    self.assertTrue(
                        self.permission_object.has_permission(
                            request, self.view
                        )
                    )
        # ensure unauthorized users can't write to group
        for method, request in self.requests.items():
            request.user = self.anonymous_user
            if method not in SAFE_METHODS:
                self.assertFalse(
                    self.permission_object.has_permission(request, self.view)
                )

    def test_has_object_permission(self):
        # 1. works on owner/user with permission
        authorized_user = self.get_user_with_permission(
            "change_group", "delete_group"
        )
        for user in [self.owner, authorized_user]:
            for method, request in self.requests.items():
                request.user = user
                if method in DETAIL_METHODS:
                    self.assertTrue(
                        self.permission_object.has_object_permission(
                            request, self.view, self.group
                        )
                    )
        # 2. works on default role
        self.group.get_default_role().edit_permission(
            "change_group", "delete_group"
        )
        for method, request in self.requests.items():
            request.user = self.default_user
            if method in DETAIL_METHODS:
                self.assertTrue(
                    self.permission_object.has_object_permission(
                        request, self.view, self.group
                    )
                )
        self.group.get_default_role().edit_permission(
            "change_group", "delete_group", to_add=False
        )
        # 3. doesnt work on anonymous/(user without permission)
        authorized_user.groups.get().role.edit_permission(
            "change_group", "delete_group", to_add=False
        )
        for user in [self.anonymous_user, authorized_user]:
            for method, request in self.requests.items():
                request.user = user
                if method in DETAIL_METHODS:
                    self.assertFalse(
                        self.permission_object.has_object_permission(
                            request, self.view, self.group
                        )
                    )


class FollowPermissionsTest(GroupPermissionsTest):

    permission_cls = FollowPermissions

    def test_has_object_permission(self):
        # 1. ensure user with permission can follow
        authorized_user = self.get_user_with_permission("group_follow")
        for user in [self.owner, authorized_user]:
            request = self.requests["POST"]
            request.user = user
            self.assertTrue(
                self.permission_object.has_object_permission(
                    request, self.view, self.group
                )
            )
        # 2. ensure user without permissions cannot follow
        authorized_user.groups.get().role.edit_permission(
            "group_follow", to_add=False
        )
        for user in [
            authorized_user,
            self.default_user,
            self.anonymous_user,
        ]:
            request = self.requests["POST"]
            request.user = user
            self.assertFalse(
                self.permission_object.has_object_permission(
                    request, self.view, self.group
                )
            )
        # 3. default role follow permission
        self.group.get_default_role().edit_permission("group_follow")
        for user in [authorized_user, self.default_user]:
            request = self.requests["POST"]
            request.user = user
            self.assertTrue(
                self.permission_object.has_object_permission(
                    request, self.view, self.group
                )
            )
        # ensure anonymous user cannot follow
        # even with default role permission set
        request = self.requests["POST"]
        request.user = self.anonymous_user
        self.assertFalse(
            self.permission_object.has_object_permission(
                request, self.view, self.group
            )
        )


class ApproveFollowPermissionsTest(GroupPermissionsTest):

    permission_cls = ApproveFollowPermissions

    def test_has_object_permission(self):
        # 1. ensure user with permission can approve follow
        authorized_user = self.get_user_with_permission("group_approve_follow")
        for user in [self.owner, authorized_user]:
            request = self.requests["POST"]
            request.user = user
            self.assertTrue(
                self.permission_object.has_object_permission(
                    request, self.view, self.group
                )
            )
        # 2. ensure user without permission cannot approve follow
        authorized_user.groups.get().role.edit_permission(
            "group_approve_follow", to_add=False
        )
        for user in [
            authorized_user,
            self.default_user,
            self.anonymous_user,
        ]:
            request = self.requests["POST"]
            request.user = user
            self.assertFalse(
                self.permission_object.has_object_permission(
                    request, self.view, self.group
                )
            )
        # 3. default role approve follow permission
        self.group.get_default_role().edit_permission("group_approve_follow")
        for user in [authorized_user, self.default_user]:
            request = self.requests["POST"]
            request.user = user
            self.assertTrue(
                self.permission_object.has_object_permission(
                    request, self.view, self.group
                )
            )
        # ensure anonymous user cannot approve follow
        # even with default role permission set
        request = self.requests["POST"]
        request.user = self.anonymous_user
        self.assertFalse(
            self.permission_object.has_object_permission(
                request, self.view, self.group
            )
        )


class RolesObjectsPermissionsTest(BasePermissionsTest):
    pass


class CommentObjectPermissionsTest(BasePermissionsTest):
    permission_cls = CommentPermissions
    view_cls = PostCommentViewSet
    reverse_url = "groups:comment-list"

    @staticmethod
    def get_reverse_kwargs():
        return {"post_id": Post.objects.latest("id").id}

    def test_has_permission(self):
        authorized_user = self.get_user_with_permission(
            "group_view_postcomment", "group_add_postcomment"
        )
        # assert not detail works for permissioned users only
        for user in [self.owner, authorized_user]:
            for method, request in self.requests.items():
                if method not in DETAIL_METHODS:
                    request.user = user
                    self.view.detail = False
                    self.view.setup(request, **self.get_reverse_kwargs())
                    self.assertTrue(
                        self.permission_object.has_permission(
                            request, self.view
                        )
                    )

        # assert detail works for logged in users
        for user in [self.owner, authorized_user, self.default_user]:
            for method, request in self.requests.items():
                if method in DETAIL_METHODS:
                    request.user = user
                    self.view.detail = True
                    self.view.setup(request, **self.get_reverse_kwargs())
                    self.assertTrue(
                        self.permission_object.has_permission(
                            request, self.view
                        )
                    )
                    # assert does not work for anonymous
                    request.user = self.anonymous_user
                    self.assertFalse(
                        self.permission_object.has_permission(
                            request, self.view
                        )
                    )

    def test_has_object_permission(self):
        # 1. every permission
        authorized_user = self.get_user_with_permission(
            "group_view_postcomment",
            "group_change_postcomment",
            "group_delete_postcomment",
        )
        for user in [self.owner, authorized_user]:
            for method, request in self.requests.items():
                if method in DETAIL_METHODS:
                    request.user = user
                    self.view.setup(request, **self.get_reverse_kwargs())
                    self.assertTrue(
                        self.permission_object.has_object_permission(
                            request, self.view, self.comment
                        )
                    )
        # 2. no permission
        authorized_user.groups.get().role.edit_permission(
            "group_view_postcomment",
            "group_change_postcomment",
            "group_delete_postcomment",
            to_add=False,
        )
        for user in [
            authorized_user,
            self.default_user,
            self.anonymous_user,
        ]:
            for method, request in self.requests.items():
                if method in DETAIL_METHODS:
                    request.user = user
                    with self.assertRaises(Http404):
                        self.permission_object.has_object_permission(
                            request, self.view, self.comment
                        )
        # 3. default role
        self.group.get_default_role().edit_permission(
            "group_view_postcomment",
            "group_change_postcomment",
            "group_delete_postcomment",
        )
        for user in [authorized_user, self.default_user]:
            for method, request in self.requests.items():
                if method in DETAIL_METHODS:
                    request.user = user
                    self.assertTrue(
                        self.permission_object.has_object_permission(
                            request, self.view, self.comment
                        )
                    )
        self.group.get_default_role().edit_permission(
            "group_view_postcomment",
            "group_change_postcomment",
            "group_delete_postcomment",
            to_add=False,
        )
        # detail without permission
        authorized_user.groups.get().role.edit_permission(
            "group_view_postcomment"
        )
        for method, request in self.requests.items():
            request.user = authorized_user
            if method in DETAIL_METHODS:
                self.assertFalse(
                    self.permission_object.has_object_permission(
                        request, self.view, self.comment
                    )
                )


class RateCommentObjectPermissionsTest(CommentObjectPermissionsTest):
    def test_has_object_permission(self):
        pass
