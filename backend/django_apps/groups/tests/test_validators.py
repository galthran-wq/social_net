from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from rest_framework.exceptions import ValidationError

from ..models import Group, Role
from ..validators import NewPermissionValidator
from ...users.tests import AuthorizedTestRequestsMixin


class NewPermissionValidatorTest(AuthorizedTestRequestsMixin, TestCase):
    class PseudoSerializer:
        def __init__(self, context):
            self.context = context

    def setUp(self) -> None:
        self.validator = NewPermissionValidator()
        self.owner = self.get_new_user()
        self.group = Group.objects.create(title="test_group", owner=self.owner)
        self.role = Role.objects.create(name="some_name", social_group=self.group)

        permission_mapping = Group.get_permission_mapping()
        self.non_target_permissions = [
            {
                "permission": Permission.objects.filter(
                    content_type=ContentType.objects.get_for_model(Group)
                ).get(codename=permission_name),
                "content_object": None,
            }
            for permission_name in permission_mapping["non_target"]
        ]
        self.target_permissions = [
            {
                "permission": Permission.objects.get(codename=permission_name),
                "content_object": self.group.get_default_role(),
            }
            for permission_name in permission_mapping["target"]
        ]

    def test_owner_validation(self):

        serializer = self.PseudoSerializer(
            {"user": self.owner, "group": self.group}
        )
        # assert not raises
        for perm in self.non_target_permissions + self.target_permissions:
            self.validator(permission_dict=perm, serializer=serializer)

        another_group = Group.objects.create(
            title="test", description="test", owner=self.owner,
        )
        # assert raises
        with self.assertRaises(ValidationError):
            for perm in self.target_permissions:
                perm.update(
                    {"content_object": another_group.get_default_role()}
                )
                self.validator(permission_dict=perm, serializer=serializer)

    def test_validation(self):
        user = self.get_new_user()

        self.role.edit_member(user)
        self.role.edit_permission(
            *[
                permission_tuple["permission"].codename
                for permission_tuple in self.non_target_permissions
            ]
        )
        self.role.edit_permission(
            *[
                permission_tuple["permission"].codename
                for permission_tuple in self.target_permissions
            ],
            target=self.group.get_default_role()
        )

        serializer = self.PseudoSerializer({"user": user, "group": self.group})
        # assert not raises
        for perm in self.non_target_permissions + self.target_permissions:
            self.validator(permission_dict=perm, serializer=serializer)

        self.role.edit_member(user, to_add=False)
        with self.assertRaises(ValidationError):
            for perm in self.non_target_permissions + self.target_permissions:
                self.validator(permission_dict=perm, serializer=serializer)
