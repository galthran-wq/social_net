from django.contrib.auth.models import AnonymousUser
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ...groups.models import Group, Post, PostComment, Role
from ...users.models import CustomUser
from ...users.tests import AuthorizedTestRequestsMixin


class GroupsAPITestCase(AuthorizedTestRequestsMixin, APITestCase):
    """
    Provides some convenient utilities for testing.
    Defines initial data -
    instances of all the models + users of different permission types
    """

    def setUp(self):
        self.owner = self.get_new_user()
        self.owner.set_password("test")
        self.owner.save()

        self.group = Group.objects.create(
            title="test", description="test", owner=self.owner,
        )
        self.group.save()

        self.post = Post.objects.create(
            group=self.group,
            author=self.owner,
            title="test",
            body="test",
            is_published=True,
        )
        self.post.save()

        self.comment = PostComment.objects.create(
            post=self.post, author=self.owner, body="test",
        )
        self.comment.save()

        self.default_user = self.get_new_user()
        self.default_user.set_password("test")
        self.default_user.save()
        self.anonymous_user = AnonymousUser()

    @staticmethod
    def assertHasEveryField(model_cls, response_data, *, exclude=None):
        for field in tuple(model_cls._meta.fields) + tuple(
            model_cls._meta.local_many_to_many
        ):
            if exclude is None or field.name not in exclude:
                assert (
                    field.name in response_data.keys()
                ), f"{field.name} is not in response_data"

    def get_user_with_permission(self, *permission_names, target=None):
        authorized_user = self.get_new_user()
        role = self.group.roles.create(
            name=f"authorized_role{CustomUser.objects.count()}"
        )
        role.edit_permission(*permission_names, target=target)
        role.edit_member(authorized_user)

        return authorized_user


class GroupSuccessfulEndpointsTest(GroupsAPITestCase):
    def test_get_list(self):
        """ensures everyone can get the list of groups"""
        response = self.client.get(reverse("groups:group-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)
        self.assertEqual(len(response.data["results"]), 1)

    def test_create(self):
        """ensures authorized user can create a group"""
        response = self._authorized_post(
            reverse("groups:group-list"),
            {"title": "test2", "description": "test2", "followers": [1, 2], },
            user=self.default_user,
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        instance = response.data

        self.assertHasEveryField(Group, response.data)
        self.assertEqual(instance["followers"]["count"], 1)
        self.assertEqual(
            instance["followers"]["results"][0]["id"],
            self.default_user.id
        )
        self.assertEqual(instance["id"], self.group.id + 1)
        self.assertEqual(len(Group.objects.all()), 2)

    def test_get_detail(self):
        """ensures everyone can see group detail"""
        for user in [self.default_user, self.owner, None]:
            response = self._authorized_get(
                reverse(
                    "groups:group-detail", kwargs={"group_id": self.group.id}
                ),
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertHasEveryField(Group, response.data)
            self.assertEqual(response.data["id"], self.group.id)
            self.assertNotIn("posts", response.data)

    def test_update_detail(self):
        """ensure endpoint works for
            owner and users with corresponding permission
             returns appropriate data"""
        authorized_user = self.get_user_with_permission("change_group")

        for user in [authorized_user, self.owner]:
            patch_response = self._authorized_patch(
                reverse(
                    "groups:group-detail", kwargs={"group_id": self.group.id}
                ),
                {"title": "new_test_title", },
                user=user,
            )
            put_response = self._authorized_put(
                reverse(
                    "groups:group-detail", kwargs={"group_id": self.group.id}
                ),
                {
                    "title": "new_test_title",
                    "description": "new_test_description",
                },
                user=user,
            )
            for response in [patch_response, put_response]:
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                self.assertEqual(response.data["title"], "new_test_title")
            self.assertEqual(
                put_response.data["description"], "new_test_description",
            )

    def test_owner_delete(self):
        """ensures owner can delete group"""
        response = self._authorized_delete(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            user=self.owner,
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Group.objects.all().count(), 0)

    def test_authorized_user_delete(self):
        """ensures user with corresponding permission can delete group"""
        authorized_user = self.get_user_with_permission("delete_group")

        response = self._authorized_delete(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Group.objects.all().count(), 0)

    def test_follow(self):
        """ensure users with permission can follow group"""
        authorized_user = self.get_user_with_permission("group_follow")

        response = self._authorized_post(
            reverse(
                "groups:group-follow", kwargs={"group_id": self.group.id},
            ),
            None,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(
            authorized_user.groups.filter(
                name=self.group.roles.get(
                    name=self.group.get_role_name("follower")
                )
            ).exists()
        )

    def test_request_follow(self):
        """if user does not have a permission, he will be placed in a queue"""
        response = self._authorized_post(
            reverse(
                "groups:group-request-follow",
                kwargs={"group_id": self.group.id},
            ),
            None,
            user=self.default_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.group.follow_queue.count(), 1)
        self.assertEqual(self.group.follow_queue.get(), self.default_user)

    def test_approve_follow(self):
        """ensure user with permissions/owner can approve followers"""
        authorized_user = self.get_user_with_permission("group_approve_follow")

        users_to_approve = {"users": [self.default_user.id]}

        for user in [self.owner, authorized_user]:
            self.group.follow_queue.add(self.default_user)

            response = self._authorized_post(
                reverse(
                    "groups:group-approve-follow",
                    kwargs={"group_id": self.group.id},
                ),
                users_to_approve,
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data["follow_queue"]["count"], 0)
            self.assertEqual(len(response.data["followers"]), 2)
            self.assertEqual(
                response.data["followers"]["results"][0]["id"], self.owner.id
            )
            self.assertEqual(
                response.data["followers"]["results"][1]["id"], self.default_user.id,
            )

            self.group.followers.remove(self.default_user)

    def test_leave(self):
        """ensure user can leave group"""
        authorized_user = self.get_user_with_permission("group_view_post")
        self.group.followers.add(authorized_user)
        response = self._authorized_post(
            reverse("groups:group-leave", kwargs={"group_id": self.group.id}),
            None,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertHasEveryField(Group, response.data)
        self.assertEqual(response.data["followers"]["count"], 1)
        self.assertFalse(authorized_user.groups.exists())

        self.assertFalse(
            self.default_user.groups.filter(
                name=self.group.roles.get(
                    name=self.group.get_role_name("follower")
                )
            ).exists()
        )
        self.assertNotIn(self.default_user.id, response.data["followers"])

    def test_get_permissions_for_user(self):
        authorized_user = self.get_user_with_permission("group_view_post")
        self.group.get_default_role().edit_permission("group_view_post")

        def request(user):
            response = self._authorized_get(
                reverse("groups:group-my-permissions", kwargs={"group_id": self.group.id}),
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(len(response.data), 1)
            self.assertEqual(response.data[0]["name"], "group_view_post")

        request(authorized_user)
        authorized_user.groups.get().role.edit_permission("group_view_post", to_add=False)
        request(authorized_user)
        request(None)


class GroupUnsuccessfulEndpointsTest(GroupsAPITestCase):
    def test_user_blacklisted(self):
        """despite having all the required permissions,
            blacklisted user cannot act anyhow"""
        authorized_user = self.get_user_with_permission(
            "change_group", "add_group", "delete_group",
        )

        self.group.get_role_by_name("blacklist").edit_member(authorized_user)
        response = self._authorized_get(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self._authorized_patch(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            {},
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self._authorized_put(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            {},
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self._authorized_put(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            {},
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_with_missing_data(self):
        """ensures required fields"""
        response = self._authorized_post(
            reverse("groups:group-list"), {}, user=self.owner
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["title"][0].code, "required")

    def test_create_while_unauthorized(self):
        """ensures unauthorized users can't create group"""
        response = self.client.post(
            reverse("groups:group-list"),
            {"title": "test2", "description": "test2", "followers": [1, 2], },
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertIn("detail", response.data)

    def test_update_detail_with_missing_data(self):
        """ensure put works on full data set only"""
        put_response = self._authorized_put(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            {},
            user=self.owner,
        )
        self.assertEqual(put_response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(put_response.data["title"][0].code, "required")

    def test_update_detail_unauthorized(self):
        """no else can edit group info"""
        for user in [self.default_user, None]:
            patch_response = self._authorized_patch(
                reverse(
                    "groups:group-detail", kwargs={"group_id": self.group.id}
                ),
                {"title": "new_test_title", },
                user=user,
            )
            put_response = self._authorized_put(
                reverse(
                    "groups:group-detail", kwargs={"group_id": self.group.id}
                ),
                {
                    "id": "wont work",
                    "title": "new_test_title",
                    "description": "new_test_description",
                    "followers": "wont work",
                    "owner": "wont work",
                },
                user=user,
            )
            for response in [patch_response, put_response]:
                if user is None:
                    self.assertEqual(
                        response.status_code, status.HTTP_401_UNAUTHORIZED,
                    )
                else:
                    self.assertEqual(
                        response.status_code, status.HTTP_403_FORBIDDEN,
                    )
                self.assertIn("detail", response.data)

    def test_update_detail_with_wrong_data(self):
        put_response = self._authorized_put(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            {
                "id": "wont work",
                "title": "new_test_title",
                "description": "new_test_description",
                "followers": "wont work",
                "owner": "wont work",
            },
            user=self.owner,
        )
        self.assertEqual(put_response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(put_response.data["id"], "wont work")
        self.assertNotEqual(put_response.data["followers"], "wont work")
        self.assertNotEqual(put_response.data["owner"], "wont work")

    def test_delete_group_while_unauthorized(self):
        """ensures unauthorized can't delete group"""
        response = self._authorized_delete(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            user=self.default_user,
        )
        response2 = self._authorized_delete(
            reverse("groups:group-detail", kwargs={"group_id": self.group.id}),
            user=None,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response2.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_follow_while_unauthorized(self):
        """ensures users without permission can't follow group"""
        response = self._authorized_post(
            reverse("groups:group-follow", kwargs={"group_id": self.group.id}),
            None,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_follow_already_member(self):
        authorized_user = self.get_user_with_permission("group_follow")
        self.group.followers.add(authorized_user)
        response = self._authorized_post(
            reverse(
                "groups:group-follow", kwargs={"group_id": self.group.id},
            ),
            None,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(self.group.follow_queue.count(), 0)
        self.assertTrue(
            self.group.followers.filter(id=authorized_user.id).exists()
        )

    def test_follow_without_permission(self):
        authorized_user = self.get_user_with_permission()
        response = self._authorized_post(
            reverse(
                "groups:group-follow", kwargs={"group_id": self.group.id},
            ),
            None,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(self.group.follow_queue.count(), 0)
        self.assertFalse(
            self.group.followers.filter(id=authorized_user.id).exists()
        )

    def test_follow_request_unauthorized(self):
        response = self._authorized_post(
            reverse(
                "groups:group-request-follow",
                kwargs={"group_id": self.group.id},
            ),
            None,
            user=None,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(self.group.follow_queue.count(), 0)

    def test_approve_follow_without_permission(self):
        authorized_user = self.get_user_with_permission()

        users_to_approve = {"users": [self.default_user.id]}

        self.group.follow_queue.add(self.default_user)

        response = self._authorized_post(
            reverse(
                "groups:group-approve-follow",
                kwargs={"group_id": self.group.id},
            ),
            users_to_approve,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_approve_follow_not_in_queue(self):
        authorized_user = self.get_user_with_permission("group_approve_follow")

        users_to_approve = {"users": [self.default_user.id]}

        response = self._authorized_post(
            reverse(
                "groups:group-approve-follow",
                kwargs={"group_id": self.group.id},
            ),
            users_to_approve,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_leave_while_owner(self):
        """ensures owner can't leave group (delete instead)"""
        response = self._authorized_post(
            reverse("groups:group-leave", kwargs={"group_id": self.group.id}),
            None,
            user=self.owner,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_leave_while_unauthorized(self):
        """ensure unauthorized users can't leave group"""
        response = self._authorized_post(
            reverse("groups:group-leave", kwargs={"group_id": self.group.id}),
            None,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class RoleSuccessfulEndpointsTest(GroupsAPITestCase):
    """
    Cluster of endpoints for testing successful
    Role endpoints requests handling.
    """

    def setUp(self):
        super().setUp()
        self.role = Role.objects.create(name="new_test_role", social_group=self.group)
        self.role.save()

    def test_list_and_detail(self):
        authorized_user = self.get_user_with_permission(
            "view_role", target=self.role
        )

        authorized_user_response = self._authorized_get(
            reverse("groups:role-list", kwargs={"group_id": self.group.id}),
            user=authorized_user,
        )
        owner_response = self._authorized_get(
            reverse("groups:role-list", kwargs={"group_id": self.group.id}),
            user=self.owner,
        )
        self.assertEqual(
            authorized_user_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(owner_response.status_code, status.HTTP_200_OK)
        self.assertEqual(authorized_user_response.data["count"], 1)
        self.assertEqual(
            owner_response.data["count"], self.group.roles.count()
        )
        self.assertEqual(
            authorized_user_response.data["results"][0]["name"],
            "new_test_role",
        )

        authorized_user_response = self._authorized_get(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": "new_test_role",
                },
            ),
            user=authorized_user,
        )
        self.assertEqual(
            authorized_user_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(
            authorized_user_response.data["name"], "new_test_role"
        )

    def test_list_default_role(self):
        self.group.get_default_role().edit_permission(
            "view_role", target=self.role
        )

        response = self._authorized_get(
            reverse("groups:role-list", kwargs={"group_id": self.group.id}),
            user=self.default_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["name"], "new_test_role")

    def test_creation(self):
        """Test if authorized user/owner can create roles"""
        authorized_user = self.get_user_with_permission("group_add_role")

        for index, user in enumerate([authorized_user, self.owner]):
            role_name = f"new_test_role{index}"
            response = self._authorized_post(
                reverse(
                    "groups:role-list", kwargs={"group_id": self.group.id},
                ),
                {"name": role_name},
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(
                response.data["name"], self.group.get_role_name(role_name)
            )
            self.assertIn("users", response.data)
            self.assertEqual(response.data["users"], [])
            self.assertIn("permissions", response.data)
            self.assertEqual(response.data["permissions"], [])

    def test_deleting_with_authorized_user(self):
        authorized_user = self.get_user_with_permission(
            "view_role", "delete_role", target=self.role
        )
        response = self._authorized_delete(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": "new_test_role",
                },
            ),
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Role.objects.filter(name="new_test_role").exists())

    def test_deleting_with_owner(self):
        response = self._authorized_delete(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": "new_test_role",
                },
            ),
            user=self.owner,
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Role.objects.filter(name="new_test_role").exists())

    def test_name_updating(self):
        """Test if authorized user/owner can update roles"""
        role = Role.objects.create(name="new_test_role0", social_group=self.group)
        role.save()
        authorized_user = self.get_user_with_permission(
            "view_role", "change_role", "edit_role_name", target=role
        )

        for index, user in enumerate([authorized_user, self.owner]):
            role_name = f"new_test_role{index}"
            response = self._authorized_patch(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": role_name,
                    },
                ),
                {"name": f"new_test_role{index + 1}"},
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(
                response.data["name"], f"new_test_role{index + 1}"
            )
            self.assertIn("users", response.data)
            self.assertEqual(response.data["users"], [])
            self.assertIn("permissions", response.data)
            self.assertEqual(response.data["permissions"], [])

    def test_members_updating(self):
        authorized_user = self.get_user_with_permission(
            "view_role", "change_role", "edit_role_users", target=self.role,
        )
        # in order to assign a role, user must follow a group
        self.group.followers.add(self.default_user)
        for user in [authorized_user, self.owner]:
            self.assertEqual(self.role.user_set.count(), 0)

            update_response = self._authorized_patch(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": self.role.name,
                    },
                ),
                {"users": [self.default_user.id]},
                user=user,
            )
            self.assertEqual(update_response.status_code, status.HTTP_200_OK)
            self.assertEqual(update_response.data["name"], self.role.name)

            data = update_response.data
            self.assertEqual(data["users"][0]["id"], self.default_user.id)
            self.assertIn('description', data["users"][0])
            self.assertEqual(len(data["users"]), 1)
            self.assertIn("permissions", data)
            self.assertEqual(data["permissions"], [])

            self.role.user_set.remove(self.default_user)

    def test_blacklist_members_updating(self):
        self.group.edit_follower(self.default_user)
        blacklist_role = self.group.get_role_by_name("blacklist")
        authorized_user = self.get_user_with_permission(
            "view_role",
            "change_role",
            "edit_role_users",
            target=blacklist_role,
        )
        response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": blacklist_role.name,
                },
            ),
            {"users": [self.default_user.id]},
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # assert doesnt have follower role anymore
        self.assertEqual(self.default_user.groups.get().role, blacklist_role)

    def test_permissions_updating(self):
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": self.group.get_default_role().name,
                },
            ]
        }
        authorized_user = self.get_user_with_permission(
            "view_role",
            "change_role",
            "edit_role_permissions",
            target=self.role,
        )
        authorized_user.groups.get().role.edit_permission("group_view_post")
        authorized_user.groups.get().role.edit_permission(
            "view_role", target=self.group.get_default_role()
        )
        for user in [authorized_user, self.owner]:
            self.assertEqual(self.role.permissions.count(), 0)

            update_response = self._authorized_patch(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": self.role.name,
                    },
                ),
                permissions_to_add,
                user=user,
            )
            self.assertEqual(update_response.status_code, status.HTTP_200_OK)
            self.assertEqual(update_response.data["name"], self.role.name)
            self.assertEqual(update_response.data["users"], [])
            self.assertEqual(len(update_response.data["permissions"]), 2)

            self.assertEqual(self.role.groupobjectpermission_set.count(), 2)

            return_to_initial_response = self._authorized_patch(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": self.role.name,
                    },
                ),
                {"permissions": []},
                user=user,
            )
            self.assertEqual(
                return_to_initial_response.status_code, status.HTTP_200_OK,
            )
            self.assertEqual(
                return_to_initial_response.data["name"], self.role.name,
            )
            self.assertEqual(return_to_initial_response.data["users"], [])
            self.assertEqual(
                return_to_initial_response.data["permissions"], []
            )

    def test_put(self):
        self.group.followers.add(self.default_user)
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": self.group.get_default_role().name,
                },
            ]
        }
        authorized_user = self.get_user_with_permission(
            "view_role",
            "change_role",
            "edit_role_name",
            "edit_role_users",
            "edit_role_permissions",
            target=self.role,
        )
        authorized_user.groups.get().role.edit_permission("group_view_post")
        authorized_user.groups.get().role.edit_permission(
            "view_role", target=self.group.get_default_role()
        )
        for user in [authorized_user, self.owner]:
            put_response = self._authorized_put(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": self.role.name,
                    },
                ),
                dict(
                    permissions_to_add,
                    users=[self.default_user.id],
                    name="brand_new_name",
                ),
                user=user,
            )
            self.role = Role.objects.get(name="brand_new_name")
            self.assertEqual(put_response.status_code, status.HTTP_200_OK)

            data = put_response.data
            self.assertEqual(data["name"], self.role.name)
            self.assertEqual(data["users"][0]["id"], self.default_user.id)
            self.assertIn('description', data["users"][0])
            self.assertEqual(len(data["users"]), 1)
            self.assertEqual(len(data["permissions"]), 2)


class RoleUnsuccessfulEndpointsTest(GroupsAPITestCase):
    """
    Cluster of endpoints for testing wrong Role endpoints requests handling.
    """

    def setUp(self):
        super().setUp()
        self.role = Role.objects.create(name="new_test_role", social_group=self.group)
        self.role.save()

    def test_list_and_detail_without_view_permission(self):
        response = self._authorized_get(
            reverse("groups:role-list", kwargs={"group_id": self.group.id}),
            user=self.default_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

        detail_response = self._authorized_get(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": self.role.name,
                },
            ),
            user=self.default_user,
        )
        self.assertEqual(
            detail_response.status_code, status.HTTP_404_NOT_FOUND
        )

    def test_detail_with_wrong_data(self):
        detail_response = self._authorized_get(
            reverse(
                "groups:role-detail",
                kwargs={"group_id": 999, "role_name": self.role.name},
            ),
            user=self.owner,
        )
        self.assertEqual(
            detail_response.status_code, status.HTTP_404_NOT_FOUND
        )

    def test_creation_without_permission(self):
        response = self._authorized_post(
            reverse("groups:role-list", kwargs={"group_id": self.group.id}),
            {"name": "new_test_role4", "group": self.group.id},
            user=self.default_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_creation_with_missing_data(self):
        response = self._authorized_post(
            reverse("groups:role-list", kwargs={"group_id": self.group.id}),
            None,
            user=self.owner,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_creation_with_wrong_data(self):
        authorized_user = self.get_user_with_permission("group_add_role")
        invalid_data_response = self._authorized_post(
            reverse("groups:role-list", kwargs={"group_id": self.group.id}),
            {
                "name": self.group.get_default_role().name,
                "group": 28,
                "permissions": "__all__",
                "users": [1, 2, 3, 4],
            },
            user=authorized_user,
        )
        self.assertEqual(
            invalid_data_response.status_code, status.HTTP_400_BAD_REQUEST,
        )
        self.assertIn("name", invalid_data_response.data)
        self.assertTrue(len(invalid_data_response.data), 1)

    def test_updating_without_view_permission(self):
        authorized_user = self.get_user_with_permission(
            "change_role", target=self.role
        )
        response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": "new_test_role",
                },
            ),
            {"name": "new_test_role"},
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_updating_without_change_permission(self):
        authorized_user = self.get_user_with_permission(
            "view_role", target=self.role
        )
        response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": "new_test_role",
                },
            ),
            {"name": "new_test_role"},
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_with_invalid_data(self):
        authorized_user = self.get_user_with_permission(
            "view_role", "change_role", "edit_role_name", target=self.role,
        )
        invalid_data_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": "new_test_role",
                },
            ),
            {"name": self.group.get_default_role().name},
            user=authorized_user,
        )
        self.assertEqual(
            invalid_data_response.status_code, status.HTTP_400_BAD_REQUEST,
        )

    def test_deleting_without_view_permission(self):
        authorized_user = self.get_user_with_permission(
            "delete_role", target=self.role
        )
        response = self._authorized_delete(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": "new_test_role",
                },
            ),
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_deleting_without_delete_permission(self):
        authorized_user = self.get_user_with_permission(
            "view_role", target=self.role
        )
        response = self._authorized_delete(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": "new_test_role",
                },
            ),
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_deleting_system_roles(self):
        follower_role = self.group.get_role_by_name("follower")
        blacklist_role = self.group.get_role_by_name("blacklist")
        default_role = self.group.get_role_by_name("default")

        for role in [follower_role, blacklist_role, default_role]:
            response = self._authorized_delete(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": role.name,
                    },
                ),
                user=self.owner,
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            self.assertTrue(self.group.roles.filter(name=role.name).exists())

    def test_members_updating_without_view_permission(self):
        # create a user with members editing permission, but without view
        authorized_user = self.get_user_with_permission(
            "edit_role_users", target=self.role
        )
        update_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": self.role.name,
                },
            ),
            {"users": [self.default_user.id]},
            user=authorized_user,
        )
        self.assertEqual(
            update_response.status_code, status.HTTP_404_NOT_FOUND
        )

    def test_members_updating_without_permission(self):
        # create user with view, but without update_users permission
        authorized_user = self.get_user_with_permission(
            "view_role", target=self.role
        )
        update_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": self.role.name,
                },
            ),
            {"users": [self.default_user.id]},
            user=authorized_user,
        )
        self.assertEqual(
            update_response.status_code, status.HTTP_403_FORBIDDEN
        )

    def test_members_updating_with_wrong_data(self):
        # should return 400 (validation error on serializer)
        authorized_user = self.get_user_with_permission(
            "view_role", "change_role", "edit_role_users", target=self.role,
        )
        update_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": self.role.name,
                },
            ),
            {"users": [99]},
            user=authorized_user,
        )
        self.assertEqual(
            update_response.status_code, status.HTTP_400_BAD_REQUEST
        )

    def test_members_updating_not_following(self):
        authorized_user = self.get_user_with_permission(
            "view_role", "change_role", "edit_role_users", target=self.role,
        )
        update_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": self.role.name,
                },
            ),
            {"users": [self.default_user.id]},
            user=authorized_user,
        )
        self.assertEqual(
            update_response.status_code, status.HTTP_400_BAD_REQUEST
        )

    def test_blacklist_permissions_updating(self):
        blacklist_role = self.group.get_role_by_name("blacklist")
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": self.group.get_default_role().name,
                },
            ]
        }
        authorized_user = self.get_user_with_permission(
            "view_role",
            "change_role",
            "edit_role_permissions",
            target=blacklist_role,
        )
        authorized_user.groups.get().role.edit_permission("group_view_post")
        authorized_user.groups.get().role.edit_permission(
            "view_role", target=self.group.get_default_role()
        )
        for user in [authorized_user, self.owner]:
            self.assertEqual(blacklist_role.permissions.count(), 0)

            update_response = self._authorized_patch(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": blacklist_role.name,
                    },
                ),
                permissions_to_add,
                user=user,
            )
            self.assertEqual(
                update_response.status_code, status.HTTP_403_FORBIDDEN
            )
            self.assertEqual(blacklist_role.permissions.count(), 0)

    def test_permissions_updating_without_view_permission(self):
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": self.group.get_default_role().name,
                },
            ]
        }
        authorized_user = self.get_user_with_permission(
            "edit_role_permissions", target=self.role
        )
        update_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": self.role.name,
                },
            ),
            permissions_to_add,
            user=authorized_user,
        )
        self.assertEqual(
            update_response.status_code, status.HTTP_404_NOT_FOUND
        )

    def test_permissions_updating_without_permission(self):
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": self.group.get_default_role().name,
                },
            ]
        }
        authorized_user = self.get_user_with_permission(
            "view_role", target=self.role
        )
        update_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": self.role.name,
                },
            ),
            permissions_to_add,
            user=authorized_user,
        )
        self.assertEqual(
            update_response.status_code, status.HTTP_403_FORBIDDEN
        )

    def test_permissions_updating_without_assigning_permissions(self):
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": self.group.get_default_role().name,
                },
            ]
        }
        authorized_user = self.get_user_with_permission(
            "view_role",
            "change_role",
            "edit_role_permissions",
            target=self.role,
        )
        update_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": self.role.name,
                },
            ),
            permissions_to_add,
            user=authorized_user,
        )
        self.assertEqual(
            update_response.status_code, status.HTTP_400_BAD_REQUEST
        )

    def test_other_group_permissions_updating(self):
        """Try to update other group role permissions"""
        another_group = Group.objects.create(
            title="test", description="test", owner=self.default_user,
        )
        # try to assign permissions from other group
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": another_group.get_default_role().name,
                },
            ]
        }
        authorized_user = self.get_user_with_permission(
            "view_role",
            "change_role",
            "edit_role_permissions",
            target=self.role,
        )
        authorized_user.groups.get().role.edit_permission("group_view_post")

        for user in [authorized_user, self.owner]:
            update_response = self._authorized_patch(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": self.role.name,
                    },
                ),
                permissions_to_add,
                user=user,
            )
            self.assertEqual(
                update_response.status_code, status.HTTP_400_BAD_REQUEST,
            )
            self.assertIn("permissions", update_response.data)

    def test_blacklist_permission_updating(self):
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": self.group.get_default_role().name,
                },
            ]
        }
        blacklist_role = self.group.get_role_by_name("blacklist")
        authorized_user = self.get_user_with_permission(
            "view_role",
            "change_role",
            "edit_role_permissions",
            target=blacklist_role,
        )
        update_response = self._authorized_patch(
            reverse(
                "groups:role-detail",
                kwargs={
                    "group_id": self.group.id,
                    "role_name": blacklist_role.name,
                },
            ),
            permissions_to_add,
            user=authorized_user,
        )
        self.assertEqual(
            update_response.status_code, status.HTTP_403_FORBIDDEN
        )

    def test_put_without_required_permissions(self):
        self.group.followers.add(self.default_user)
        permissions_to_add = {
            "permissions": [
                {"name": "group_view_post"},
                {
                    "name": "view_role",
                    "target": self.group.get_default_role().name,
                },
            ]
        }
        # check that users without permissions/members/change cant PUT
        user_without_change_perm = self.get_user_with_permission(
            "view_role",
            "edit_role_permissions",
            "edit_role_users",
            target=self.role,
        )
        user_without_users_perm = self.get_user_with_permission(
            "view_role",
            "edit_role_permissions",
            "change_role",
            target=self.role,
        )
        user_without_permissions_perm = self.get_user_with_permission(
            "view_role", "edit_role_users", "change_role", target=self.role,
        )
        for user in [
            user_without_change_perm,
            user_without_users_perm,
            user_without_permissions_perm,
        ]:
            user.groups.get().role.edit_permission("group_view_post")
            user.groups.get().role.edit_permission(
                "view_role", target=self.group.get_default_role()
            )
            response = self._authorized_put(
                reverse(
                    "groups:role-detail",
                    kwargs={
                        "group_id": self.group.id,
                        "role_name": self.role.name,
                    },
                ),
                dict(
                    permissions_to_add,
                    users=[self.default_user.id],
                    name="brand_new_name",
                ),
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PostListTest(GroupsAPITestCase):
    def setUp(self):
        super().setUp()
        self.group2 = Group.objects.create(
            title="test_group2", owner=self.owner
        )
        self.group2.save()

        for group in [self.group, self.group2]:
            for i in range(10):
                Post.objects.create(
                    group=group,
                    author=self.owner,
                    title=f"test_post_{i}",
                    body="body",
                    is_published=True,
                ).save()

            for i in range(10):
                Post.objects.create(
                    group=group,
                    author=self.default_user,
                    title=f"test_post_{i}{i}",
                    body="body",
                    is_published=True,
                ).save()

        self.group.get_default_role().edit_permission("group_view_post")

    def test_author_list(self):
        response = self._authorized_get(
            reverse("groups:post-list"), user=self.default_user
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 21)

    def test_owner_list(self):
        response = self._authorized_get(
            reverse("groups:post-list"), user=self.owner
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 41)

    def test_unauthorized_list(self):
        response = self.client.get(reverse("groups:post-list"),)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 21)

    def test_unpublished_list(self):
        authorized_user = self.get_user_with_permission(
            "group_view_unpublished_post"
        )
        for i in range(10):
            Post.objects.create(
                title=f"test_tile{i}{i}{i}",
                body="test",
                group=self.group,
                author=self.owner,
                is_published=False,  # explicitly mark as unpublished
            ).save()

        response = self._authorized_get(
            reverse("groups:post-list"), user=authorized_user
        )
        response2 = self._authorized_get(
            reverse("groups:post-list"), user=self.owner
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 31)
        self.assertEqual(response2.data["count"], 51)


class PostSuccessfulEndpointsTest(GroupsAPITestCase):
    def test_creation(self):
        """ensures only user with permission/owner can create posts"""
        authorized_user = self.get_user_with_permission("group_add_post")
        for user in [self.owner, authorized_user]:
            response = self._authorized_post(
                reverse("groups:post-list"),
                {
                    "title": "test_title",
                    "body": "test_body",
                    "group": self.group.id,
                },
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertHasEveryField(
                Post, response.data, exclude=["publishing_datetime"]
            )
            self.assertEqual(response.data["is_published"], False)

    def test_placing(self):

        post = Post.objects.create(
            group=self.group,
            title="some_title",
            body="some_body",
            is_published=False,
        )
        post.save()

        authorized_user = self.get_user_with_permission(
            "group_publish_post", "group_view_unpublished_post"
        )
        response = self._authorized_post(
            reverse("groups:post-publish", kwargs={"post_id": post.id}),
            None,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertHasEveryField(
            Post, response.data, exclude=["posting_datetime"]
        )
        self.assertEqual(response.data["is_published"], True)

    def test_get_detail(self):
        """ensures user with permission/owner can see post detail"""
        authorized_user = self.get_user_with_permission("group_view_post")
        # create 10 posts, fetch every, ensure correct id
        for user in [authorized_user, self.owner]:
            for i in range(10):
                post = Post.objects.create(
                    group=self.group,
                    title=f"test{i}",
                    body="test100",
                    author=self.owner,
                    is_published=True,
                )

                response = self._authorized_get(
                    reverse(
                        "groups:post-detail", kwargs={"post_id": post.id},
                    ),
                    user=user,
                )
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                self.assertHasEveryField(Post, response.data)
                self.assertEqual(response.data["title"], f"test{i}")

    def test_get_unpublished_detail(self):
        post = Post.objects.create(
            group=self.group,
            title="test",
            body="test100",
            author=self.owner,
            is_published=False,  # imitate scheduled post
        )

        authorized_user = self.get_user_with_permission(
            "group_view_unpublished_post"
        )
        for user in [self.owner, authorized_user]:
            response = self._authorized_get(
                reverse(
                    "groups:post-detail", kwargs={"post_id": post.id}
                ),  # + 1 bcs id starts from 1 and there
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertHasEveryField(Post, response.data)
            self.assertEqual(response.data["title"], "test")

    def test_update_detail(self):
        """
        endpoint works for owner/user with permission,
        returns appropriate data
        """
        authorized_user = self.get_user_with_permission(
            "group_change_post", "group_view_post"
        )
        for user in [authorized_user, self.owner]:
            patch_response = self._authorized_patch(
                reverse(
                    "groups:post-detail", kwargs={"post_id": self.post.id}
                ),
                {
                    "id": "wont work",
                    "title": "new_test_title",
                    "followers": "wont work",
                    "owner": "wont work",
                },
                user=user,
            )
            put_response = self._authorized_put(
                reverse(
                    "groups:post-detail", kwargs={"post_id": self.post.id}
                ),
                {
                    "id": "wont work",
                    "title": "new_test_title",
                    "body": "new_test_description",
                    "group": self.group.id,
                },
                user=user,
            )
            for response in [patch_response, put_response]:
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                self.assertNotEqual(response.data["id"], "wont work")
                self.assertEqual(response.data["title"], "new_test_title")
            self.assertEqual(put_response.data["body"], "new_test_description")

    def test_delete_detail(self):
        """ensures owner/post_author/authorized can delete post"""
        authorized_user = self.get_user_with_permission(
            "group_delete_post",
            "group_view_post",
            "group_view_unpublished_post",
        )
        for i in range(4):
            Post.objects.create(
                title="test{i}0",
                body="test2",
                author=authorized_user,
                group=self.group,
                is_published=True if i % 2 == 0 else False,
            ).save()

        for index, user in enumerate([self.owner, authorized_user]):
            earliest_id = Post.objects.earliest("id").id
            response = self._authorized_delete(
                reverse(
                    "groups:post-detail", kwargs={"post_id": earliest_id},
                ),
                user=user,
            )
            response2 = self._authorized_delete(
                reverse(
                    "groups:post-detail", kwargs={"post_id": earliest_id + 1},
                ),
                user=user,
            )

            for response in [response, response2]:
                self.assertEqual(
                    response.status_code, status.HTTP_204_NO_CONTENT
                )
            self.assertEqual(Post.objects.all().count(), 2 - index * 2 + 1)


class PostUnsuccessfulEndpointsTest(GroupsAPITestCase):
    def test_creation_without_view_permission(self):
        """ensure no else can create posts"""
        for user in [self.default_user, None]:
            response = self._authorized_post(
                reverse("groups:post-list"),
                {
                    "title": "test_title",
                    "body": "test_body",
                    "group": self.group.id,
                },
                user=user,
            )
            if user is None:
                self.assertEqual(
                    response.status_code, status.HTTP_401_UNAUTHORIZED
                )
            else:
                self.assertEqual(
                    response.status_code, status.HTTP_403_FORBIDDEN
                )

    def test_creation_without_add_permission(self):
        authorized_user = self.get_user_with_permission("group_view_post")
        response = self._authorized_post(
            reverse("groups:post-list"),
            {
                "title": "test_title",
                "body": "test_body",
                "group": self.group.id,
            },
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_creation_with_missing_data(self):
        response = self._authorized_post(
            reverse("groups:post-list"),
            {"body": "test_body", },
            user=self.owner,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["title"][0].code, "required")

    def test_get_detail_with_wrong_data(self):
        """ensures wrong data doesn't work"""
        response = self.client.get(
            reverse("groups:post-detail", kwargs={"post_id": 99}),
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_without_view_permission(self):
        authorized_user = self.get_user_with_permission("group_change_post")
        response = self._authorized_patch(
            reverse("groups:post-detail", kwargs={"post_id": self.post.id}),
            {"group": self.group.id},
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_without_permission(self):
        authorized_user = self.get_user_with_permission("group_view_post")
        response = self._authorized_patch(
            reverse("groups:post-detail", kwargs={"post_id": self.post.id}),
            {"title": "new_title", },
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_while_unauthorized(self):
        """Ensures endpoint doesn't work for default/anonymous user"""
        authorized_user = self.get_user_with_permission("group_view_post")
        for user in [authorized_user, None]:
            patch_response = self._authorized_patch(
                reverse(
                    "groups:post-detail", kwargs={"post_id": self.post.id}
                ),
                {
                    "id": "wont work",
                    "title": "new_test_title",
                    "followers": "wont work",
                    "owner": "wont work",
                },
                user=user,
            )
            put_response = self._authorized_put(
                reverse(
                    "groups:post-detail", kwargs={"post_id": self.post.id}
                ),
                {
                    "id": "wont work",
                    "title": "new_test_title",
                    "body": "new_test_description",
                    "group": 1,
                },
                user=user,
            )
            for response in [patch_response, put_response]:
                if user is None:
                    self.assertEqual(
                        response.status_code, status.HTTP_401_UNAUTHORIZED,
                    )
                else:
                    self.assertEqual(
                        response.status_code, status.HTTP_403_FORBIDDEN,
                    )

    def test_update_with_wrong_data(self):
        authorized_user = self.get_user_with_permission(
            "group_view_post", "group_change_post"
        )
        before_update_response = self._authorized_get(
            reverse("groups:post-detail", kwargs={"post_id": self.post.id}),
            user=authorized_user,
        )
        response = self._authorized_patch(
            reverse("groups:post-detail", kwargs={"post_id": self.post.id}),
            {"group": 9},
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for response in [before_update_response, response]:
            response.data.pop("updated_at")
        self.assertEqual(before_update_response.data, response.data)

    def test_update_with_missing_data(self):
        put_response = self._authorized_put(
            reverse("groups:post-detail", kwargs={"post_id": self.post.id}),
            {},
            user=self.owner,
        )
        self.assertEqual(put_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_while_unauthorized(self):
        """ensures noone else can delete post"""
        Post.objects.create(
            title="test0", body="test2", author=self.owner, group=self.group,
        ).save()

        authorized_user = self.get_user_with_permission("group_view_post")
        for user in [authorized_user, None]:
            response = self._authorized_delete(
                reverse(
                    "groups:post-detail", kwargs={"post_id": self.post.id}
                ),
                user=user,
            )
            if user is None:
                self.assertEqual(
                    response.status_code, status.HTTP_401_UNAUTHORIZED
                )
            else:
                self.assertEqual(
                    response.status_code, status.HTTP_403_FORBIDDEN
                )

    def test_post_publishing_without_view_permission(self):
        authorized_user = self.get_user_with_permission("group_publish_post")
        response = self._authorized_post(
            reverse("groups:post-publish", kwargs={"post_id": self.post.id},),
            None,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_publishing_without_permission(self):
        authorized_user = self.get_user_with_permission("group_view_post")
        response = self._authorized_post(
            reverse("groups:post-publish", kwargs={"post_id": self.post.id},),
            None,
            user=authorized_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CommentSuccessfulEndpointsTest(GroupsAPITestCase):
    def test_get_comment_list(self):  # comment list view
        """ensure user with permission/owner can see comment list"""
        authorized_user = self.get_user_with_permission(
            "group_view_postcomment"
        )
        for user in [authorized_user, self.owner]:
            response = self._authorized_get(
                reverse(
                    "groups:comment-list", kwargs={"post_id": self.post.id}
                ),
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(len(response.data), 4)
            self.assertEqual(len(response.data["results"]), 1)
            self.assertEqual(
                response.data["results"][0]["id"], self.comment.id
            )

    def test_create_comment(self):
        """ensure authorized users/owner can comment"""
        authorized_user = self.get_user_with_permission(
            "group_add_postcomment"
        )
        for user in [authorized_user, self.owner]:
            response = self._authorized_post(
                reverse(
                    "groups:comment-list", kwargs={"post_id": self.post.id}
                ),
                {"body": "test"},
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertHasEveryField(PostComment, response.data)

    def test_get_comment_detail(self):
        """ensure authorized users/owner can get comment detail"""
        authorized_user = self.get_user_with_permission(
            "group_view_postcomment"
        )

        for user in [authorized_user, self.owner]:
            response = self._authorized_get(
                reverse(
                    "groups:comment-detail",
                    kwargs={
                        "post_id": self.post.id,
                        "comment_id": self.comment.id,
                    },
                ),
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertHasEveryField(PostComment, response.data)
            self.assertEqual(response.data["id"], self.comment.id)

    def test_update_comment_detail(self):
        """ensure authorized users/owner/author can update comment detail"""
        authorized_user = self.get_user_with_permission(
            "group_change_postcomment"
        )
        comment = PostComment.objects.create(
            body="test_body", post=self.post, author=self.default_user,
        )
        for user in [self.owner, authorized_user, self.default_user]:
            patch_response = self._authorized_patch(
                reverse(
                    "groups:comment-detail",
                    kwargs={
                        "post_id": self.post.id,
                        "comment_id": comment.id,
                    },
                ),
                {"body": "new_test_description"},
                user=user,
            )
            put_response = self._authorized_put(
                reverse(
                    "groups:comment-detail",
                    kwargs={
                        "post_id": self.post.id,
                        "comment_id": comment.id,
                    },
                ),
                {"id": "wont work", "body": "new_test_description", },
                user=user,
            )
            for response in [patch_response, put_response]:
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                self.assertNotEqual(response.data["id"], "wont work")
                self.assertEqual(response.data["body"], "new_test_description")

    def test_delete_comment(self):
        """ensures owner/user with
        permission/post_author can delete post"""
        authorized_user = self.get_user_with_permission(
            "group_delete_postcomment"
        )
        for i in range(3):
            PostComment.objects.create(
                body="test2", author=self.default_user, post=self.post
            ).save()
        for index, user in enumerate(
            [self.owner, self.default_user, authorized_user]
        ):
            response = self._authorized_delete(
                reverse(
                    "groups:comment-detail",
                    kwargs={
                        "post_id": self.post.id,
                        "comment_id": index + self.comment.id,
                    },
                ),
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
            self.assertEqual(PostComment.objects.all().count(), 3 - index)


class CommentUnsuccessfulEndpointsTest(GroupsAPITestCase):
    def test_get_comment_list_unauthorized(self):
        """ensure users without permission cant see comment list"""
        response = self.client.get(
            reverse("groups:comment-list", kwargs={"post_id": self.post.id}),
        )
        # 404 is fine, bcs unauthorized users can have
        # a view comment permission (default role)
        # but here - they have not.
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_comment_list_with_wrong_data(self):
        response = self.client.get(
            reverse("groups:comment-list", kwargs={"post_id": 99}),
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_comment_with_wrong_data(self):
        response = self._authorized_post(
            reverse("groups:comment-list", kwargs={"post_id": 99}),
            {"body": "test"},
            user=self.default_user,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertIn("detail", response.data)

    def test_create_comment_list_with_missing_data(self):
        response = self._authorized_post(
            reverse("groups:comment-list", kwargs={"post_id": self.post.id}),
            {},
            user=self.owner,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("body", response.data)

    def test_get_comment_detail_with_wrong_data(self):
        response = self.client.get(
            reverse(
                "groups:comment-detail",
                kwargs={"post_id": self.post.id, "comment_id": 99},
            ),
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertIn("detail", response.data)

    def test_update_comment_detail_with_wrong_data(self):
        put_response = self._authorized_put(
            reverse(
                "groups:comment-detail",
                kwargs={
                    "post_id": self.post.id,
                    "comment_id": self.comment.id,
                },
            ),
            {},
            user=self.owner,
        )
        self.assertEqual(put_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_comment_while_unauthorized(self):
        """
        ensure default/unauthorized users
        can't delete else's comment
        """
        authorized_user = self.get_user_with_permission(
            "group_view_postcomment"
        )
        for user in [authorized_user, None]:
            response = self._authorized_delete(
                reverse(
                    "groups:comment-detail",
                    kwargs={
                        "post_id": self.post.id,
                        "comment_id": self.comment.id,
                    },
                ),
                user=user,
            )
            if user is None:
                self.assertEqual(
                    response.status_code, status.HTTP_401_UNAUTHORIZED
                )
            else:
                self.assertEqual(
                    response.status_code, status.HTTP_403_FORBIDDEN
                )


class RateableMixinTest(GroupsAPITestCase):
    def test_rate_post_comment(self):
        authorized_user = self.get_user_with_permission(
            "group_rate_post", "group_rate_postcomment", "group_view_post",
        )
        self.post.disliked_by.add(authorized_user)
        self.post.save()

        post_like_response = self._authorized_post(
            reverse("groups:post-like", kwargs={"post_id": self.post.id}),
            None,
            user=authorized_user,
        )

        # assert user is not disliking after like
        self.assertEqual(post_like_response.data["disliked_by"], [])

        post_dislike_response = self._authorized_post(
            reverse("groups:post-dislike", kwargs={"post_id": self.post.id},),
            None,
            user=authorized_user,
        )

        # assert user is not liking after dislike
        self.assertEqual(post_dislike_response.data["liked_by"], [])

        self.post.disliked_by.add(authorized_user)
        self.post.save()
        comment_like_response = self._authorized_post(
            reverse(
                "groups:comment-like",
                kwargs={
                    "post_id": self.post.id,
                    "comment_id": self.comment.id,
                },
            ),
            None,
            user=authorized_user,
        )

        # assert user is not disliking after like
        self.assertEqual(comment_like_response.data["disliked_by"], [])

        comment_dislike_response = self._authorized_post(
            reverse(
                "groups:comment-dislike",
                kwargs={
                    "post_id": self.post.id,
                    "comment_id": self.comment.id,
                },
            ),
            None,
            user=authorized_user,
        )

        # assert user is not liking after dislike
        self.assertEqual(comment_dislike_response.data["liked_by"], [])

        for response in [post_dislike_response, post_like_response]:
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertHasEveryField(
                Post, response.data, exclude=["posting_datetime"]
            )

        for response in [
            comment_like_response,
            comment_dislike_response,
        ]:
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertHasEveryField(PostComment, response.data)

        for response in [comment_like_response, post_like_response]:
            self.assertEqual(len(response.data["liked_by"]), 1)
            self.assertEqual(response.data["liked_by"][0]["id"], authorized_user.id)
            self.assertIn("description", response.data["liked_by"][0])

        for response in [
            comment_dislike_response,
            post_dislike_response,
        ]:
            self.assertEqual(len(response.data["disliked_by"]), 1)
            self.assertEqual(response.data["disliked_by"][0]["id"], authorized_user.id)
            self.assertIn("description", response.data["disliked_by"][0])

    def test_already_rate_post(self):
        authorized_user = self.get_user_with_permission(
            "group_rate_post", "group_rate_postcomment", "group_view_post",
        )
        for instance in [self.post, self.comment]:
            instance.liked_by.add(authorized_user)
            instance.disliked_by.add(authorized_user)
            instance.save()

        post_like_response = self._authorized_post(
            reverse("groups:post-like", kwargs={"post_id": self.post.id}),
            None,
            user=authorized_user,
        )

        post_dislike_response = self._authorized_post(
            reverse("groups:post-dislike", kwargs={"post_id": self.post.id},),
            None,
            user=authorized_user,
        )

        comment_like_response = self._authorized_post(
            reverse(
                "groups:comment-like",
                kwargs={
                    "post_id": self.post.id,
                    "comment_id": self.comment.id,
                },
            ),
            None,
            user=authorized_user,
        )
        comment_dislike_response = self._authorized_post(
            reverse(
                "groups:comment-dislike",
                kwargs={
                    "post_id": self.post.id,
                    "comment_id": self.comment.id,
                },
            ),
            None,
            user=authorized_user,
        )
        for response in [
            post_dislike_response,
            post_like_response,
            comment_like_response,
            comment_dislike_response,
        ]:
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
