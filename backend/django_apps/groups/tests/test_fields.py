from django.test import TestCase
from rest_framework.exceptions import ValidationError

from ..fields import ObjectPermissionTargetField, CustomUserRelatedField
from ..models import Group, Role
from ...users.models import CustomUser
from ...users.serializers import ProfileSerializer


class ObjectPermissionTargetFieldTest(TestCase):
    def setUp(self) -> None:
        self.group = Group.objects.create(title="some_title")
        self.role = Role.objects.create(name="some_role", social_group=self.group)
        self.role.edit_permission("group_view_post")
        self.role.edit_permission(
            "edit_role_users", target=self.group.get_default_role()
        )
        self.nontarget_permission = self.role.groupobjectpermission_set.get(
            permission__codename="group_view_post"
        )
        self.target_permission = self.role.groupobjectpermission_set.get(
            permission__codename="edit_role_users"
        )
        self.field = ObjectPermissionTargetField()

    def test_to_representation(self):
        self.assertEqual(
            self.field.to_representation(
                self.nontarget_permission.content_object
            ),
            None,
        )
        self.assertEqual(
            self.field.to_representation(
                self.target_permission.content_object
            ),
            self.target_permission.content_object.name,
        )
        self.assertEqual(self.field.to_representation(None), None)

    def test_to_internal_value(self):
        self.assertEqual(
            self.field.to_internal_value("1_default_role"),
            Role.objects.get(name="1_default_role"),
        )
        with self.assertRaises(ValidationError):
            self.field.to_internal_value("does_not_exist")

        # noinspection PyTypeChecker
        self.assertEqual(self.field.to_internal_value(None), None)


class CustomUserRelatedFieldTest(TestCase):
    def setUp(self) -> None:
        self.user = CustomUser.objects.create_user(
            username="some_username",
            password=123,
            email="email_adress@mail.ru"
        )
        self.field = CustomUserRelatedField(queryset=CustomUser.objects.all())

    def test_to_internal_value(self):
        self.assertEqual(
            self.field.to_internal_value(self.user.id),
            self.user
        )

    def test_to_representation(self):
        self.assertEqual(
            self.field.to_representation(self.user),
            ProfileSerializer(self.user.profile).data
        )
