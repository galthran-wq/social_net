from django.contrib.auth.models import AnonymousUser
from django.db.models import Q
from django.test import TestCase

from .test_views import GroupsAPITestCase
from ...groups.models import Group, Role
from ...users.tests import AuthorizedTestRequestsMixin


class GroupModelTests(AuthorizedTestRequestsMixin, TestCase):
    def setUp(self) -> None:
        self.owner = self.get_new_user()
        self.owner.set_password("test_test_test")
        self.owner.save()

        self.group = Group.objects.create(
            title="test_title",
            description="test_description",
            owner=self.owner,
        )
        self.group.save()

    def test_manager_creation(self):
        """check if owner is added to followers, default values"""
        new_group = Group.objects.create(
            title="test_title2",
            description="test_description2",
            owner=self.owner,
        )
        new_group.save()
        self.assertEqual(new_group.followers.count(), 1)
        self.assertIn(self.owner, new_group.followers.all())
        self.assertFalse(new_group.follow_queue.exists())

        # test nullable description
        new_group = Group.objects.create(title="test_title2", owner=self.owner)
        self.assertEqual(new_group.description, None)

    def test_follower_editing(self):
        """test adding follower, assigning certain permissions and removing"""
        new_follower = self.get_new_user()
        new_follower.set_password("test_test_test")
        new_follower.save()

        self.group.edit_follower(new_follower)
        self.assertEqual(self.group.followers.count(), 2)
        self.assertIn(self.owner, self.group.followers.all())
        self.assertIn(new_follower, self.group.followers.all())

        new_role = Role.objects.create(name="new_test_role", social_group=self.group)
        new_role.edit_permission("group_view_post")
        new_role.edit_member(new_follower)
        # also add follower role permissions
        self.group.roles.get(
            name=self.group.get_role_name("follower")
        ).edit_permission("group_view_postcomment")

        # check new permission status
        self.assertTrue(new_follower.has_perm("group_view_post", self.group))
        self.assertTrue(
            new_follower.has_perm("group_view_postcomment", self.group)
        )

        self.group.edit_follower(new_follower, to_add=False)
        # assert user has lost permission, and not a follower anymore
        self.assertFalse(new_follower.has_perm("group_view_post", self.group))
        self.assertFalse(
            new_follower.has_perm("group_view_postcomment", self.group)
        )
        self.assertFalse(new_follower.groups.exists())
        self.assertEqual(self.group.followers.count(), 1)
        self.assertIn(self.owner, self.group.followers.all())

        # assert nothing happened to the actual role
        self.assertTrue(self.group.roles.filter(name="new_test_role").exists())
        self.assertTrue(
            self.group.roles.get(
                name="new_test_role"
            ).groupobjectpermission_set.get(
                permission__codename="group_view_post"
            )
        )

    def test_improper_follower_editing(self):
        new_follower = self.get_new_user()
        new_follower.set_password("test_test_test")
        new_follower.save()

        for i in range(5):
            # try adding follower several times (5)
            # assert nothing is changing
            self.group.edit_follower(new_follower)
            self.assertEqual(
                new_follower.groups.get().name,
                self.group.roles.get(
                    name=self.group.get_role_name("follower")
                ).name,
            )
            self.assertEqual(self.group.followers.count(), 2)
            self.assertIn(self.owner, self.group.followers.all())
            self.assertIn(new_follower, self.group.followers.all())

        for i in range(5):
            # try removing several times (5)
            # assert nothing is changing
            self.group.edit_follower(new_follower, to_add=False)
            self.assertEqual(new_follower.groups.count(), 0)
            self.assertEqual(self.group.followers.count(), 1)
            self.assertIn(self.owner, self.group.followers.all())

    def test_follow_request_and_approve(self):
        new_user = self.get_new_user()
        new_user.set_password("test_test_test")
        new_user.save()

        self.group.request_follow(new_user)
        self.assertEqual(self.group.follow_queue.count(), 1)
        self.assertIn(new_user, self.group.follow_queue.all())
        self.assertEqual(self.group.followers.count(), 1)
        self.assertIn(self.owner, self.group.followers.all())
        self.assertFalse(new_user.groups.exists())

        self.group.approve_follower(new_user)
        self.assertEqual(self.group.follow_queue.count(), 0)
        self.assertNotIn(new_user, self.group.follow_queue.all())
        self.assertEqual(self.group.followers.count(), 2)
        self.assertIn(self.owner, self.group.followers.all())
        self.assertIn(new_user, self.group.followers.all())
        self.assertTrue(new_user.groups.exists())
        self.assertTrue(
            new_user.groups.get(),
            self.group.roles.get(name=self.group.get_role_name("follower")),
        )

    def test_improper_follow_request_and_approve(self):
        """try repeating the same action, ensure nothing changes"""
        new_user = self.get_new_user()
        new_user.set_password("test_test_test")
        new_user.save()

        for i in range(5):
            self.group.request_follow(new_user)
            self.assertIn(new_user, self.group.follow_queue.all())
            self.assertIn(self.owner, self.group.followers.all())
            self.assertFalse(new_user.groups.exists())

        for i in range(5):
            self.group.approve_follower(new_user)
            self.assertEqual(self.group.followers.count(), 2)
            self.assertIn(self.owner, self.group.followers.all())
            self.assertIn(new_user, self.group.followers.all())
            self.assertTrue(new_user.groups.exists())
            self.assertTrue(
                new_user.groups.get(),
                self.group.roles.get(
                    name=self.group.get_role_name("follower")
                ),
            )


class PermissionListForUserTest(GroupsAPITestCase):
    def setUp(self):
        super(PermissionListForUserTest, self).setUp()

    def test_for_default_role(self):
        user = self.get_user_with_permission()
        self.group.get_default_role().edit_permission("group_view_post")

        # assert translates permissions from default role
        permissions = self.group.get_user_permissions(user)
        self.assertEqual(
            permissions.get(),
            self.group.get_default_role().groupobjectpermission_set.get()
        )

    def test_for_user_roles(self):
        user = self.get_user_with_permission("group_view_post")
        user_role = user.groups.get().role

        # assert translates permissions from user role
        permissions = self.group.get_user_permissions(user)
        self.assertEqual(
            permissions.get(),
            user_role.groupobjectpermission_set.get()
        )

    def test_for_anonymous_user(self):
        user = AnonymousUser()
        self.group.get_default_role().edit_permission("group_view_post")

        # assert translates permissions from default role
        permissions = self.group.get_user_permissions(user)
        self.assertEqual(
            permissions.get(),
            self.group.get_default_role().groupobjectpermission_set.get()
        )

        self.group.get_default_role().edit_permission("group_view_post", to_add=False)
        self.assertFalse(permissions.exists())


class RoleModelTest(AuthorizedTestRequestsMixin, TestCase):
    def setUp(self) -> None:
        self.owner = self.get_new_user()
        self.owner.set_password("test_test_test")
        self.owner.save()

        self.group = Group.objects.create(
            title="test_title",
            description="test_description",
            owner=self.owner,
        )
        self.group.save()

        self.role = Role.objects.create(name="new_test_role", social_group=self.group)
        self.role.save()

    def test_user_editing(self):
        new_user = self.get_new_user()
        new_user.set_password("test_test")
        new_user.save()

        self.assertFalse(new_user.groups.exists())
        for i in range(5):
            self.role.edit_member(new_user)
            self.assertEqual(new_user.groups.count(), 1)
            self.assertEqual(new_user.groups.get().name, self.role.name)

        for i in range(5):
            self.role.edit_member(new_user, to_add=False)
            self.assertFalse(new_user.groups.exists())

    def test_permission_editing(self):
        new_user = self.get_new_user()
        new_user.set_password("test_test")
        new_user.save()
        self.role.edit_member(new_user)

        non_target_permission = "group_view_post"
        target_permission = "edit_role_users"

        for i in range(5):
            self.role.edit_permission(non_target_permission)
            self.role.edit_permission(
                target_permission, target=self.group.get_default_role(),
            )
            self.assertTrue(
                new_user.has_perm(non_target_permission, self.group)
            )
            self.assertTrue(
                new_user.has_perm(
                    target_permission, self.group.get_default_role()
                )
            )

        for i in range(5):
            self.role.edit_permission(non_target_permission, to_add=False)
            self.role.edit_permission(
                target_permission,
                target=self.group.get_default_role(),
                to_add=False,
            )
            self.assertFalse(
                new_user.has_perm(non_target_permission, self.group)
            )
            self.assertFalse(
                new_user.has_perm(
                    target_permission, self.group.get_default_role()
                )
            )
