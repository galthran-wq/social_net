from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory
from urllib.parse import urlencode

from django_apps.groups.models import Group, Post
from django_apps.groups.queryset_filters import PostsFilter
from django_apps.users.tests import AuthorizedTestRequestsMixin


class PostsFilterTest(APITestCase, AuthorizedTestRequestsMixin):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.user = self.get_new_user()
        self.group = Group.objects.create(
            title="some_title"
        )
        self.post = Post.objects.create(
            group=self.group,
            title="some_title"
        )
        self.group2 = Group.objects.create(
            title="some_title2"
        )
        self.post2 = Post.objects.create(
            group=self.group2,
            title="some_title2"
        )
        self.filter_class = PostsFilter

    def test_group_filter(self):
        query_params = urlencode({"group": self.group.id})
        request = self.factory.get(
            '/?%s' % query_params
        )
        queryset = self.filter_class(
            data=request.GET,
            queryset=Post.objects.all(),
            request=request
        ).qs
        self.assertEqual(queryset.get(), self.post)



