from django.db.models import Manager


class GroupManager(Manager):
    def create(self, *args, **kwargs):
        """
        Additionally adds group owner to
        followers on instance create.
        """
        instance = super().create(*args, **kwargs)
        # make owner follow group
        if instance.owner:
            instance.edit_follower(instance.owner)
        return instance
