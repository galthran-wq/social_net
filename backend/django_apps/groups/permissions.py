from django.http import Http404
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from .utils import extract_group_from_post_request


class GroupPermissions(
    IsAuthenticatedOrReadOnly, permissions.DjangoObjectPermissions
):
    """
    List permissions are handled by :class:`.IsAuthenticatedOrReadOnly`

    Defines required object permissions.

    Overrides object permission flow to include special cases.
    """

    perms_map = {
        "GET": [],  # Authorized can get group list/detail
        "OPTIONS": [],
        "HEAD": [],
        "POST": [],
        "PUT": ["%(app_label)s.change_%(model_name)s"],
        "PATCH": ["%(app_label)s.change_%(model_name)s"],
        "DELETE": ["%(app_label)s.delete_%(model_name)s"],
    }

    @staticmethod
    def is_group_owner(request, group):
        """
        :param request:
        :param group:
        :type group: :class:`.Group`
        :rtype: bool
        """
        return request.user == group.owner

    @staticmethod
    def is_user_blacklisted(group, user):
        """
        :param group:
        :param user:
        :type user: :class:`.CustomUser`
        :rtype: bool
        """
        blacklist_role = group.get_role_by_name("blacklist")
        # user is in blacklist, permission denied
        if user in blacklist_role.user_set.all():
            return True

    def has_object_permission(self, request, view, group):
        """
        There are 3 main steps:
            #. If request user is owner - passed
            #. If request user is blacklisted - 403
            #. Check user personal permissions:
                #. Assign default role to user
                #. Get permission status (check object permission)
                   using super()'s method
                #. Remove default role
                #. Return permission status

        :param request:
        :param view:
        :param group:
        :rtype: bool
        """
        # owner special case
        if self.is_group_owner(request, group):
            return True
        # blacklist special case
        if self.is_user_blacklisted(group, request.user):
            return False
        elif not request.user.is_anonymous:
            default_role = group.get_default_role()
            try:
                request.user.groups.add(default_role)
                permission_status = super(
                    GroupPermissions, self
                ).has_object_permission(request, view, group)
            finally:
                request.user.groups.remove(default_role)
            return permission_status
        return super(GroupPermissions, self).has_object_permission(
            request, view, group
        )


class FollowPermissions(GroupPermissions):
    """
    Overrides *perms_map* to include corresponding 'POST' permission.
    """

    perms_map = dict(
        GroupPermissions.perms_map, POST=["%(app_label)s.group_follow"],
    )


class ApproveFollowPermissions(GroupPermissions):
    """
    Overrides *perms_map* to include corresponding 'POST' permission.
    """

    perms_map = dict(
        GroupPermissions.perms_map,
        POST=["%(app_label)s.group_approve_follow"],
    )


class RolesPermissions(GroupPermissions):
    """
    Handles create and object permissions.
    (View permissions are handled by view queryset filter
    (:class:`.RoleFilter`))

    """

    perms_map = {
        "GET": ["%(app_label)s.view_%(model_name)s"],
        "OPTIONS": ["%(app_label)s.view_%(model_name)s"],
        "HEAD": ["%(app_label)s.view_%(model_name)s"],
        "POST": ["%(app_label)s.group_add_%(model_name)s"],
        "PUT": ["%(app_label)s.change_%(model_name)s"],
        "PATCH": ["%(app_label)s.change_%(model_name)s"],
        "DELETE": ["%(app_label)s.delete_%(model_name)s"],
    }

    def has_permission(self, request, view):
        """
        Nonobject permissions are determined using
        :class:`.GroupPermissions` object permissions method

        :example:

        User can create role, only if he has
        corresponding permission on group object

        :param request:
        :param view:
        :rtype: bool
        """
        if view.action == "create":
            group = view.get_group()
            try:
                return super(RolesPermissions, self).has_object_permission(
                    request, view, group
                )
            except Http404:
                # raise 403
                return False
        return True

    @staticmethod
    def get_exception_map(group):
        """
        Maps request method to list of role objects, that are
        restricted to be passed.

        :param :class:`.Group` group:
        :rtype: dict
        """
        follower_role = group.get_role_by_name("follower")
        blacklist_role = group.get_role_by_name("blacklist")
        default_role = group.get_role_by_name("default")
        return {
            "GET": [],
            "OPTIONS": [],
            "HEAD": [],
            "POST": [],
            "PUT": [],
            "PATCH": [],
            "DELETE": [follower_role, blacklist_role, default_role],
        }

    def has_object_permission(self, request, view, role):
        """
        There are 4 main steps:
            #. If role is in exception map - 403
            #. If request user is owner - passed
            #. If request user is blacklisted - 403
            #. Check user personal permissions:
                #. Assign default role to user
                #. Get permission status (check object permission)
                   using :class:`DjangoObjectPermissions`
                #. Remove default role
                #. Return permission status

        :param request:
        :param view:
        :param role:
        :rtype: bool
        """
        # unchangeable roles special case
        if role in self.get_exception_map(role.social_group)[request.method]:
            return False
        # owner special case
        if self.is_group_owner(request, role.social_group):
            return True
        # blacklisted special case
        if self.is_user_blacklisted(role.social_group, request.user):
            return False
        elif not request.user.is_anonymous:
            try:
                request.user.groups.add(role.social_group.get_default_role())
                permission_status = super(
                    GroupPermissions, self
                ).has_object_permission(request, view, role)
            finally:
                request.user.groups.remove(role.social_group.get_default_role())
            return permission_status
        return super(GroupPermissions, self).has_object_permission(
            request, view, role
        )


class BaseRoleSpecificFieldEditPermissions(RolesPermissions):
    """
    Defines *field_name* - if passed in request data,
    :class:`.RolesPermissions` object permission method is invoked,
    using overriden *perms_map*.

    :example:

    The request with {field_name} in data,
    is only passed, if the user has corresponding permission
    """

    perms_map = {
        "GET": [],
        "OPTIONS": [],
        "HEAD": [],
        "POST": [],
        "PUT": [],
        "PATCH": [],
        "DELETE": [],
    }
    field_name = None

    def has_permission(self, request, view):
        """
        :param request:
        :param view:
        :return: True
        """
        return True

    def has_object_permission(self, request, view, role):
        """
        Check if *field_name* is in data, if so -
        invoke :class:`.RolesPermissions` super method

        :param request:
        :param view:
        :param role:
        :rtype: bool
        """
        if self.field_name in request.data:
            return super(
                BaseRoleSpecificFieldEditPermissions, self
            ).has_object_permission(request, view, role)
        return True


class RoleEditUsersPermissions(BaseRoleSpecificFieldEditPermissions):
    """
    Defines concrete *field_name* and corresponding permissions
    """

    field_name = "users"
    perms_map = dict(
        BaseRoleSpecificFieldEditPermissions.perms_map,
        PATCH=["%(app_label)s.edit_role_users"],
        PUT=["%(app_label)s.edit_role_users"],
    )


class RoleEditPermissionsPermissions(BaseRoleSpecificFieldEditPermissions):
    """
    Defines concrete *field_name* and corresponding permissions

    Requires user to have corresponding permissions in order
    to update role's name
    """

    field_name = "permissions"
    perms_map = dict(
        BaseRoleSpecificFieldEditPermissions.perms_map,
        PATCH=["%(app_label)s.edit_role_permissions"],
        PUT=["%(app_label)s.edit_role_permissions"],
    )

    @staticmethod
    def get_exception_map(group):
        """
        Append *blacklist* system role to 'PATCH'/'PUT'
        role lists, in order to avoid it's permissions updating
        """
        mapping = RoleEditUsersPermissions.get_exception_map(group)
        blacklist_role = group.get_role_by_name("blacklist")
        mapping["PATCH"].append(blacklist_role)
        mapping["PUT"].append(blacklist_role)
        return mapping


class RoleEditNamePermissions(BaseRoleSpecificFieldEditPermissions):
    """
    Defines concrete *field_name* and corresponding permissions

    Requires user to have corresponding permissions in order
    to update role's name
    """

    field_name = "name"
    perms_map = dict(
        BaseRoleSpecificFieldEditPermissions.perms_map,
        PATCH=["%(app_label)s.edit_role_name"],
        PUT=["%(app_label)s.edit_role_name"],
    )

    @staticmethod
    def get_exception_map(group):
        """
        Append all system roles to 'PATCH'/'PUT'
        role lists, in order to avoid their name updating
        """
        mapping = RoleEditUsersPermissions.get_exception_map(group)
        blacklist_role = group.get_role_by_name("blacklist")
        default_role = group.get_role_by_name("default")
        followers_role = group.get_role_by_name("follower")
        mapping["PATCH"].extend([blacklist_role, default_role, followers_role])
        mapping["PUT"].extend([blacklist_role, default_role, followers_role])
        return mapping


class PostPermissions(GroupPermissions):
    """
    Defines create and object permissions.
    (View permissions are handled by view queryset filter
    (:class:`.PostFilter`))
    """

    perms_map = {
        "GET": [],
        "OPTIONS": [],
        "HEAD": [],
        "POST": ["%(app_label)s.group_add_post"],
        "PUT": ["%(app_label)s.group_change_post"],
        "PATCH": ["%(app_label)s.group_change_post"],
        "DELETE": ["%(app_label)s.group_delete_post"],
    }

    def has_permission(self, request, view):
        """
        Several steps are performed:
            #. Ensures user is authenticated, return False otherwise
            #. If view.action is not create - return True
            #. Else, Group is extracted
               from request using view's *get_post* method
            #. Permission status is determined using
               :class:`.GroupPermissions` super method
        """
        authorized_or_read_only_status = super(
            PostPermissions, self
        ).has_permission(request, view)

        if not authorized_or_read_only_status:
            return False

        if view.action == "create":
            try:
                group = extract_group_from_post_request(request)
            except BaseException:
                # will be handed to serializer -> validation error -> 400 error
                # with msg
                return True

            return super(PostPermissions, self).has_object_permission(
                request, view, group
            )
        return True

    @staticmethod
    def is_post_author(user, post):
        """
        Checks if user is post's author

        :param user:
        :param post:
        :return:
        """
        return user == post.author

    def has_object_permission(self, request, view, post):
        """
        Include's extra special case: post author.

        If user is post author - pass

        :param request:
        :param view:
        :param post:
        :return:
        """
        if self.is_post_author(request.user, post):
            return True

        group = post.group
        return super(PostPermissions, self).has_object_permission(
            request, view, group
        )


class RatePostPermissions(PostPermissions):
    """
    Overrides *perms_map* to include corresponding 'POST' permission.
    """

    perms_map = dict(
        PostPermissions.perms_map, POST=["%(app_label)s.group_rate_post"],
    )


class PostPublishingPermissions(PostPermissions):
    """
    Overrides *perms_map* to include corresponding 'POST' permission.
    """

    perms_map = dict(
        PostPermissions.perms_map, POST=["%(app_label)s.group_publish_post"],
    )

    @staticmethod
    def is_post_author(user, post):
        """
        Disallow post author to publish his own post.

        :param user:
        :param post:
        :return: False
        """
        return False


class PostSchedulingPermissions(PostPermissions):
    """
    Overrides *perms_map* to include corresponding 'POST' permission.
    """

    perms_map = dict(
        PostPermissions.perms_map, POST=["%(app_label)s.group_schedule_post"],
    )

    @staticmethod
    def is_post_author(user, post):
        """
        Disallow post author to schedule his own post.

        :param user:
        :param post:
        :return: False
        """
        return False


class CommentPermissions(GroupPermissions):
    """
    Defines permissions all (list, detail) permissions
    in *perms_map*.
    """

    perms_map = {
        "GET": ["%(app_label)s.group_view_postcomment"],
        "OPTIONS": ["%(app_label)s.group_view_postcomment"],
        "HEAD": ["%(app_label)s.group_view_postcomment"],
        "POST": ["%(app_label)s.group_add_postcomment"],
        "PUT": ["%(app_label)s.group_change_postcomment"],
        "PATCH": ["%(app_label)s.group_change_postcomment"],
        "DELETE": ["%(app_label)s.group_delete_postcomment"],
    }

    def has_permission(self, request, view):
        """
        Several steps are performed:
            #. If user unauthorized - return False
            #. Else, extract post from view using *get_post* method
            #. If request.method is detail one, return True
               (pass to object permissions method)
            #. Else, extract group from post
            #. Return permission status, obtained
               using :class:`.GroupPermissions`
               object permission method
        """
        authorized_or_read_only_status = super(
            CommentPermissions, self
        ).has_permission(request, view)

        if not authorized_or_read_only_status:
            return False

        if not view.detail:
            group = view.get_post().group
            return super(CommentPermissions, self).has_object_permission(
                request, view, group
            )
        return True

    @staticmethod
    def is_comment_author(user, comment):
        """
        :param user:
        :param comment:
        :rtype: bool
        """
        return user == comment.author

    def has_object_permission(self, request, view, comment):
        """
        Include's extra special case: comment author.

        If user is comment author - pass

        :param request:
        :param view:
        :param comment:
        :rtype: bool
        """
        if self.is_comment_author(request.user, comment):
            return True

        group = comment.post.group
        return super(CommentPermissions, self).has_object_permission(
            request, view, group
        )


class RateCommentPermissions(CommentPermissions):
    """
    Overrides *perms_map* to include corresponding 'POST' permission.
    """

    perms_map = dict(
        CommentPermissions.perms_map,
        POST=["%(app_label)s.group_rate_comment"],
    )
