from django.apps import AppConfig


class GroupsConfig(AppConfig):
    name = "django_apps.groups"

    # noinspection PyUnresolvedReferences
    def ready(self):
        from . import signals  # noqa
