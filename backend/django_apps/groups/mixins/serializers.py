from rest_framework import serializers

from django_apps.users.models import Profile
from django_apps.users.serializers import ProfileSerializer


class RateableSerializerMixin(serializers.Serializer):
    is_liked = serializers.SerializerMethodField()
    is_disliked = serializers.SerializerMethodField()
    liked_by = serializers.SerializerMethodField()
    disliked_by = serializers.SerializerMethodField()

    def get_is_liked(self, post):
        user = self.context["request"].user
        return post.liked_by.filter(id=user.id).exists()

    def get_is_disliked(self, post):
        user = self.context["request"].user
        return post.disliked_by.filter(id=user.id).exists()

    def get_liked_by(self, post):
        liked_by_profiles = Profile.objects.filter(
            user__in=post.liked_by.all()
        )
        return self.shorten_field(
            liked_by_profiles,
            ProfileSerializer
        )

    def get_disliked_by(self, post):
        disliked_by_profiles = Profile.objects.filter(
            user__in=post.disliked_by.all()
        )
        return self.shorten_field(
            disliked_by_profiles,
            ProfileSerializer
        )
