import datetime

from django.db import models

from ...groups import tasks
from ...users.models import CustomUser


class Postable(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Rateable(models.Model):
    liked_by = models.ManyToManyField(
        to=CustomUser, default=[], related_name="liked_%(class)s"
    )
    disliked_by = models.ManyToManyField(
        to=CustomUser, default=[], related_name="disliked_%(class)s"
    )

    def like(self, user):
        """
        Toggles like on instance
        """
        if self.liked_by.filter(pk=user.id).exists():
            self.liked_by.remove(user)
        else:
            self.liked_by.add(user)

            # if disliked previously
            if self.disliked_by.filter(pk=user.id).exists():
                self.disliked_by.remove(user)

    def dislike(self, user):
        """
        Toggles dislike on instance
        """
        if self.disliked_by.filter(pk=user.id).exists():
            self.disliked_by.remove(user)
        else:
            self.disliked_by.add(user)

            # if liked previously
            if self.liked_by.filter(pk=user.id).exists():
                self.liked_by.remove(user)

    class Meta:
        abstract = True


class Schedulable(models.Model):
    is_published = models.BooleanField(default=False)
    publishing_datetime = models.DateTimeField(null=True)

    def schedule(self, delay_in_minutes):
        self.is_published = False
        self.publishing_datetime = datetime.datetime.today(
        ) + datetime.timedelta(minutes=delay_in_minutes)
        self.save()
        # convert countdown to seconds
        tasks.submit_post.apply_async(
            args=[self.id], countdown=delay_in_minutes * 60
        )

    def publish(self):
        """Places a post"""
        if not self.is_published:
            self.is_published = True
            self.publishing_datetime = None
            self.save()

    class Meta:
        abstract = True
