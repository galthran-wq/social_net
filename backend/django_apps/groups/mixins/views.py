from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.settings import api_settings


# noinspection PyUnresolvedReferences
class SerializerUserContextMixin:
    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update(
            {"user": self.request.user, }
        )
        context.update(**self.kwargs)
        return context


# noinspection PyUnresolvedReferences
class RateableViewSetMixin:
    """
    Assumes that detail model instance has like(), dislike(), __str__ methods.
    """

    rate_permission_classes = [IsAuthenticated]

    @action(
        detail=True,
        methods=["post"],
        permission_classes=rate_permission_classes,
    )
    def like(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.like(request.user)
        return Response(self.get_serializer(instance).data)

    @action(
        detail=True,
        methods=["post"],
        permission_classes=rate_permission_classes,
    )
    def dislike(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.dislike(request.user)
        return Response(self.get_serializer(instance).data)


class ShortenFieldMixin:
    @staticmethod
    def shorten_field(queryset, serializer_cls, context=None):
        page_size = api_settings.PAGE_SIZE
        return {
            "count": queryset.count(),
            "results": serializer_cls(
                queryset[:page_size],
                many=True,
                context=context
            ).data
        }
