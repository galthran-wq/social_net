""""
This module contains filter classes for
django_filters filtering via query params
"""
from django_filters import rest_framework as filters

from django_apps.groups.models import Post, Group, PostComment


class PostsFilter(filters.FilterSet):

    group = filters.NumberFilter(
        field_name="group__id",
        lookup_expr="exact",
    )

    author = filters.NumberFilter(
        field_name="author__id",
        lookup_expr="exact"
    )

    class Meta:
        model = Post
        fields = [
            "group",
            "title",
            "author"
        ]


class GroupsFilter(filters.FilterSet):

    owner = filters.NumberFilter(
        field_name="owner__id",
        lookup_expr="exact",
    )

    class Meta:
        model = Group
        fields = [
            "title",
            "owner"
        ]


class PostCommentFilter(filters.FilterSet):

    post = filters.NumberFilter(
        field_name="post__id",
        lookup_expr="exact"
    )

    class Meta:
        model = PostComment
        fields = [
            "post"
        ]
