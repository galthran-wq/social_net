from django.contrib.auth.models import Group as PermissionGroup
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import gettext_lazy as _
from guardian.models import GroupObjectPermission
from guardian.shortcuts import assign_perm, remove_perm

from .managers import GroupManager
from .mixins.models import (
    Postable,
    Rateable,
    Schedulable,
)
from ..users.models import CustomUser


class Group(Postable):
    title = models.CharField(max_length=127)
    description = models.CharField(max_length=512, blank=True, null=True)
    follow_queue = models.ManyToManyField(
        CustomUser, default=[], related_name="follow_requests"
    )
    owner = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)
    objects = GroupManager()

    class Meta:
        # . Lists all group-related permissions
        permissions = [
            # post
            ("group_add_post", _("Can add posts")),
            ("group_change_post", _("Can change posts")),
            ("group_view_post", _("Can see posts")),
            ("group_view_unpublished_post", _("Can see unpublished posts"),),
            ("group_delete_post", _("Can delete posts")),
            # comment
            ("group_add_postcomment", _("Can add comment to the posts"),),
            (
                "group_change_postcomment",
                _("Can change comment to the posts"),
            ),
            ("group_view_postcomment", _("Can view comment to the posts"),),
            (
                "group_delete_postcomment",
                _("Can postcomment comment to the posts"),
            ),
            # role
            ("group_add_role", _("Can add roles to the group")),
            # actions
            ("group_follow", _("Can follow group")),
            ("group_approve_follow", _("Can approve follow")),
            ("group_rate_post", _("Can rate posts")),
            ("group_publish_post", _("Can publish posts")),
            ("group_schedule_post", _("Can schedule posts")),
            ("group_rate_comment", _("Can rate comments")),
        ]

    @property
    def followers(self):
        """
        :return : Followers role members
        :rtype: :class:`.Queryset
        """
        return self.get_role_by_name("follower").user_set

    @staticmethod
    def get_permission_mapping():
        """
        :return: Permission list, both group-related, and role-specific.
        :rtype: dict
        """
        permissions_mapping = {
            "non_target": {
                permission_tuple[0]: permission_tuple[1]
                for permission_tuple in Group._meta.permissions
            },
            "target": {
                permission_tuple[0]: permission_tuple[1]
                for permission_tuple in Role._meta.permissions
            }
        }

        permissions_mapping["non_target"].update(
            {
                f"{default_action}_group": "description"
                for default_action in ["delete", "view", "change"]
            }
        )

        permissions_mapping["target"].update(
            {
                f"{default_action}_role": "description"
                for default_action in ["view", "change", "delete"]
            }
        )
        return permissions_mapping

    def get_user_permissions(self, user: CustomUser):
        default_role_permissions = GroupObjectPermission.objects.filter(
            group=self.get_default_role(),
        )

        if not user.is_anonymous:
            user_roles_permissions = GroupObjectPermission.objects.filter(
                group__in=Role.objects.filter(
                    user__in=[user],
                    social_group=self
                )
            )
            default_role_permissions |= user_roles_permissions

        return default_role_permissions

    def get_role_by_name(self, role_name):
        """
        :param str role_name: Name of the role
        :return: Role
        :rtype: :class:`.Role`
        """
        return self.roles.get(name=self.get_role_name(role_name))

    def get_role_name(self, role_name):
        return f"{self.pk}_{role_name}_role"

    def get_default_role(self):
        """
        :return: Group's default role
        :rtype: :class:`.Role`
        """
        return self.roles.get(name=self.get_role_name("default"))

    def edit_follower(self, *users, to_add=True):
        """
        Edits group followers. Action is chosen according to the *to_add* flag.

        :param users: Users, to be edited
        :param to_add: Adds followers, if True, else: Removes them instead
        """
        follower_role = self.roles.get(name=self.get_role_name("follower"))

        for user in users:
            if not to_add:
                # remove all permissions
                for role in Role.objects.filter(user__id=user.id, social_group=self):
                    role.edit_member(user, to_add=False)
            else:
                follower_role.edit_member(*users, to_add=to_add)

    def request_follow(self, user):
        """
        Adds user to the follow queue

        :param user: User to be added
        """
        self.follow_queue.add(user)

    def cancel_follow_request(self, user):
        self.follow_queue.remove(user)

    def approve_follower(self, *users):
        """
        Approves users from follow queue.

        :param users: Users from follow queue to be approved
        """
        for user in users:
            self.follow_queue.remove(user)
        self.edit_follower(*users)


class Role(PermissionGroup):
    social_group = models.ForeignKey(
        Group, related_name="roles", on_delete=models.CASCADE
    )

    class Meta:
        # . Role specific permissions
        permissions = [
            ("edit_role_users", _("Can edit role's user set")),
            ("edit_role_permissions", _("Can edit role's permissions"),),
            ("edit_role_name", _("Can edit role's name")),
        ]

    def edit_member(self, *users, to_add=True):
        """
        Edits role members. Action is chosen according to the *to_add* flag.

        :param users: Users, to be edited.
        :param to_add: Adds members, if True, else: Removes them instead
        """
        for user in users:
            if to_add:
                user.groups.add(self)
            else:
                user.groups.remove(self)
            user.save()

    def edit_permission(self, *permission_names, target=None, to_add=True):
        """
        Edits role permissions.
        Action is chosen according to the *to_add* flag.

        :param permission_names: Names of permissions to be added
        :param target: Target role, to apply permissions on. (Members
        of some role can have permissions associated with other roles)
        :type target: :class:`.Role`
        :param to_add: Adds members, if True, else: Removes them instead
        """
        if target is None:
            target = self.social_group

        action_method = assign_perm if to_add else remove_perm
        permission_mapping = Group.get_permission_mapping()

        for permission_name in permission_names:
            if (
                permission_name in permission_mapping["non_target"] or
                permission_name in permission_mapping["target"]
            ):
                action_method(permission_name, self, target)


class Post(Schedulable, Postable, Rateable):
    group = models.ForeignKey(
        Group, on_delete=models.CASCADE, related_name="posts"
    )
    author = models.ForeignKey(
        CustomUser, on_delete=models.SET_NULL, null=True
    )
    title = models.CharField(max_length=64)
    body = models.CharField(max_length=512)


class PostComment(Postable, Rateable):
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name="comments"
    )
    author = models.ForeignKey(
        CustomUser, null=True, on_delete=models.SET_NULL
    )
    body = models.CharField(max_length=255)
