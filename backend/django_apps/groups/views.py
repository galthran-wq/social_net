from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework as filters
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .filters import PostFilter, RoleFilter
from .mixins.views import (
    SerializerUserContextMixin,
    RateableViewSetMixin,
)
from .models import Group, Post
from .permissions import (
    FollowPermissions,
    PostPermissions,
    CommentPermissions,
    RatePostPermissions,
    RateCommentPermissions,
    RolesPermissions,
    RoleEditPermissionsPermissions,
    RoleEditUsersPermissions,
    GroupPermissions,
    PostPublishingPermissions,
    PostSchedulingPermissions,
    ApproveFollowPermissions,
    RoleEditNamePermissions,
)
from .serializers import (
    GroupSerializer,
    CreatePostSerializer,
    UpdatePostSerializer,
    PostCommentSerializer,
    RoleSerializer,
    ApproveFollowGroupSerializer,
    RequestFollowGroupSerializer,
    FollowGroupSerializer,
    LeaveGroupSerializer,
    PostScheduleSerializer, ObjectPermissionSerializer, CancelFollowGroupRequestSerializer,
)
from .queryset_filters import PostsFilter, PostCommentFilter, GroupsFilter


class GroupViewSet(SerializerUserContextMixin, ModelViewSet):
    permission_classes = [GroupPermissions]
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    lookup_url_kwarg = "group_id"
    # . Routes requests with only numerical ids
    lookup_value_regex = "[0-9]{1,}"
    # . Required by *django_filters*
    filter_backends = [filters.DjangoFilterBackend]
    # Filter class, that defines query param filters
    # used by *django_filters*
    filterset_class = GroupsFilter

    @action(
        detail=True,
        methods=["post"],
        permission_classes=[FollowPermissions],
        serializer_class=FollowGroupSerializer,
    )
    def follow(self, request, *args, **kwargs):
        request.data.update({"user": request.user.id})
        return self.update(request)

    @action(
        detail=True,
        methods=["post"],
        permission_classes=[IsAuthenticated],
        serializer_class=RequestFollowGroupSerializer,
    )
    def request_follow(self, request, *args, **kwargs):
        request.data.update({"user": request.user.id})
        return self.update(request)

    @action(
        detail=True,
        methods=["post"],
        permission_classes=[IsAuthenticated],
        serializer_class=CancelFollowGroupRequestSerializer,
    )
    def cancel_follow_request(self, request, *args, **kwargs):
        request.data.update({"user": request.user.id})
        return self.update(request)

    @action(
        detail=True,
        methods=["post"],
        permission_classes=[ApproveFollowPermissions],
        serializer_class=ApproveFollowGroupSerializer,
    )
    def approve_follow(self, request, *args, **kwargs):
        return self.update(request)

    @action(
        detail=True,
        methods=["post"],
        permission_classes=[IsAuthenticated],
        serializer_class=LeaveGroupSerializer,
    )
    def leave(self, request, *args, **kwargs):
        request.data.update({"user": request.user.id})
        return self.update(request)

    @action(
        detail=False,
        methods=["get"],
        permission_classes=[],
    )
    def permissions(self, request, *args, **kwargs):
        return Response(Group.get_permission_mapping())

    @action(
        detail=True,
        methods=["get"],
        permission_classes=[],
    )
    def my_permissions(self, request, *args, **kwargs):
        group = self.get_object()
        user_permissions = group.get_user_permissions(request.user)
        serialized_permissions = ObjectPermissionSerializer(user_permissions, many=True).data
        unique_serializer_permissions = [
            dict(t) for t in {tuple(d.items()) for d in serialized_permissions}
        ]
        return Response(unique_serializer_permissions)


class RoleViewSet(SerializerUserContextMixin, ModelViewSet):
    lookup_url_kwarg = "role_name"
    lookup_field = "name"
    # . Used to extract ancestor object
    #  (group in this case)
    group_lookup_url_kwarg = "group_id"
    serializer_class = RoleSerializer
    permission_classes = [
        RolesPermissions,
        RoleEditPermissionsPermissions,
        RoleEditUsersPermissions,
        RoleEditNamePermissions,
    ]
    filter_backends = [
        # . Used to filter roles
        #  in accordance with obtained view permissions
        RoleFilter,
        # . Required by *django_filters*
        filters.DjangoFilterBackend
    ]

    def get_group(self):
        """
        Extracts ancestor object (:class:`.Group` instance)
        using provided in url id

        :raises Group.DoesNotExist: the group is not found
        :return: :class:`.Group` ancestor object
        """
        try:
            group_id = self.kwargs[self.group_lookup_url_kwarg]
            group = Group.objects.get(pk=group_id)
        except Group.DoesNotExist:
            raise NotFound(_("No group found with given data"))

        return group

    def get_queryset(self):
        """
        Returns all ancestor object
        (:class:`.Group` instance) roles

        :return: :class:`.Queryset`<:class:`.Role`>
        """
        return self.get_group().roles.all()

    def get_serializer_context(self):
        """
        Adds ancestor group to serializer context

        :return: context
        :rtype: dict
        """
        context = super(RoleViewSet, self).get_serializer_context()
        context.update({"group": self.get_group()})
        return context


class PostViewSet(
    SerializerUserContextMixin, RateableViewSetMixin, ModelViewSet
):
    permission_classes = [PostPermissions]
    queryset = Group.objects.all()  # Gets filtered to actual posts
    lookup_url_kwarg = "post_id"
    # . Routes requests with only numerical ids
    lookup_value_regex = "[0-9]{1,}"
    # .
    rate_permission_classes = [RatePostPermissions]
    filter_backends = [
        # . Used to filter Group queryset to posts,
        # that request user can see
        # in accordance with his permissions
        PostFilter,
        # . Required by *django_filters*
        filters.DjangoFilterBackend
    ]
    # Filter class, that defines query param filters
    # used by *django_filters*
    filterset_class = PostsFilter

    def get_serializer_class(self):
        """
        Defines, which serializier class to user if no
        is defined.

        Makes distinction between create and update flow,
        chooses the right serializer for each one.

        :return: serializer class
        """
        if self.serializer_class:
            return self.serializer_class

        if self.request.method == "POST":
            return CreatePostSerializer
        else:
            return UpdatePostSerializer

    @action(
        detail=True,
        methods=["post"],
        permission_classes=[PostPublishingPermissions],
    )
    def publish(self, request, *args, **kwargs):
        post = self.get_object()
        post.publish()
        return Response(
            self.get_serializer(post).data, status=status.HTTP_200_OK
        )

    @action(
        detail=True,
        methods=["post"],
        permission_classes=[PostSchedulingPermissions],
        serializer_class=PostScheduleSerializer,
    )
    def schedule(self, request, *args, **kwargs):
        return self.update(request)


class PostCommentViewSet(
    SerializerUserContextMixin, RateableViewSetMixin, ModelViewSet
):
    permission_classes = [CommentPermissions]
    serializer_class = PostCommentSerializer
    lookup_url_kwarg = "comment_id"
    # . Routes requests with only numerical ids
    lookup_value_regex = "[0-9]{1,}"
    # . Used to extract ancestor object
    #  (post in this case)
    post_lookup_url_kwarg = "post_id"
    # . Required by *django_filters*
    filter_backends = [filters.DjangoFilterBackend]
    # Filter class, that defines query param filters
    # used by *django_filters*
    filterset_class = PostCommentFilter
    rate_permission_classes = [RateCommentPermissions]

    def get_post(self) -> Post:
        """
        Extracts ancestor object (:class:`.Post` instance)
        using provided in url id

        :return: :class:`.Post` ancestor object
        """
        try:
            post_id = self.kwargs[self.post_lookup_url_kwarg]
            post = Post.objects.get(pk=post_id)
        except BaseException:
            raise NotFound(_("No post found with given data"))

        return post

    def get_queryset(self):
        """
        Returns all ancestor object
        (:class:`.Post` instance) comments

        :return: :class:`.Queryset`<:class:`.PostComment`>
        """
        return self.get_post().comments.all()

    def get_serializer_context(self):
        """
        Add ancestor's object (:class:`.Group` instance)
        to serializer context

        :return: context
        :rtype: dict
        """
        context = super().get_serializer_context()
        context.update({"post": self.get_post()})
        return context
