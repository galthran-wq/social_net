from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Group, Role


# noinspection PyUnusedLocal,PyUnusedLocal
@receiver(post_save, sender=Group)
def initialize_roles(sender, instance, **kwargs):
    """
    Initializes three default roles used by system:
        - default: no role permission group
        - follower: followers
        - blacklist: blacklisted users (have no permissions)
    """
    group = instance
    default_role_name = group.get_role_name("default")
    follower_role_name = group.get_role_name("follower")
    blacklist_name = group.get_role_name("blacklist")
    if group.roles.filter(
        Q(name=default_role_name)
        | Q(name=follower_role_name)
        | Q(name=blacklist_name)
    ).exists():
        return

    group.roles.add(
        Role.objects.create(name=default_role_name, social_group=group),
        Role.objects.create(name=follower_role_name, social_group=group),
        Role.objects.create(name=blacklist_name, social_group=group),
    )
    group.save()
