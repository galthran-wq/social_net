from celery import shared_task


@shared_task
def submit_post(post_id):
    """
    Gets :class:`.Post` instance, using provided *post_id*,
    and publishes it

    :param post_id:
    :return:
    """
    from .models import Post

    post = Post.objects.get(pk=post_id)
    post.publish()
