from rest_framework.routers import DefaultRouter

from .views import (
    GroupViewSet,
    PostViewSet,
    PostCommentViewSet,
    RoleViewSet,
)

app_name = "groups"

router = DefaultRouter()
router.register(
    "posts/(?P<post_id>[0-9]{1,})/comments",
    PostCommentViewSet,
    basename="comment",
)
router.register("posts", PostViewSet, basename="post")
router.register(
    "groups/(?P<group_id>[0-9]{1,})/roles", RoleViewSet, basename="role",
)
router.register("groups", GroupViewSet, basename="group")

urlpatterns = router.urls

# todo
# create serparate endoint for group followers
# todo
# tests
