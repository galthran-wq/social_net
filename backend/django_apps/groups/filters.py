# noinspection PyPep8Naming
from django.db.models import Value as V, IntegerField
from django.db.models.functions import Concat, Cast
from rest_framework.filters import BaseFilterBackend
from rest_framework_guardian.filters import ObjectPermissionsFilter

from .models import Post, Role


class GroupBasePermissionsFilter(BaseFilterBackend):
    """
    Base custom filter, that provides a way
    to filter Group queryset to any other arbitrary one,
    according to user's permissions.

    :example:

        User needs to get queryset,
        filtered according to several permissions.
        As in :class:`.PostFilter`,
        user receives both unpublished and published posts,
        if both permissions are obtained
    """

    #: List of tuples of form ({Permission name}, {Finalize method signature})
    permissions = []
    #: Is used as a template for finalize method
    finalize_method_format = "finalize_%(signature)s"

    def filter_queryset(self, request, queryset, view):
        """
        Combines finalized querysets for every
        obtained permission in permissions

        :param request:
        :param queryset:
        :param view:
        :return: Filtered queryset
        """
        from guardian.shortcuts import get_objects_for_user

        user = request.user
        formated_permissions = [
            (
                permission_pair[0]
                % {
                    "app_label": queryset.model._meta.app_label,
                    "signature": permission_pair[1],
                },
                permission_pair[1],
            )
            for permission_pair in self.permissions
        ]

        filtered_queryset = Post.objects.none()
        for permission, permission_signature in formated_permissions:
            # user's personal permissions
            filtered_group_queryset = get_objects_for_user(
                user, permission, queryset,
            )

            # group's default role permissions
            default_queryset = queryset.filter(
                roles__name=Concat("id", V("_default_role")),
                roles__groupobjectpermission__permission__codename__exact=permission.split(  # noqa
                    "."
                )[
                    1
                ],
            )

            finalize_method = getattr(
                self,
                self.finalize_method_format
                % {"signature": permission_signature},
            )

            filtered_queryset |= finalize_method(
                default_queryset
            ) | finalize_method(filtered_group_queryset)

        # owner case
        if not request.user.is_anonymous:
            filtered_queryset |= self._finalize_owner(request.user, queryset)
        return filtered_queryset

    def _finalize_owner(self, user, group_queryset):
        owned_groups = group_queryset.filter(owner=user)
        posts = Post.objects.none()
        for group in owned_groups:
            posts |= Post.objects.filter(group=group)
        return posts


class PostFilter(GroupBasePermissionsFilter):
    """
    Defines group permissions and finalize method
    for filtering initial queryset to Posts.
    """

    #: Include view_[unpublished_]post permissions
    permissions = [
        ("%(app_label)s.group_%(signature)s", "view_post"),
        ("%(app_label)s.group_%(signature)s", "view_unpublished_post",),
    ]

    def finalize_view_post(self, group_queryset):
        """
        Returns all published posts from the given group queryset.
        :param group_queryset:
        :return: Queryset<group.models.Post>
        """
        posts = Post.objects.none()
        for group in group_queryset:
            posts |= Post.objects.filter(is_published=True, group=group)
        return posts

    def finalize_view_unpublished_post(self, group_queryset):
        """
        Returns all unpublished posts from the given group queryset.

        :param group_queryset:
        :return: Queryset<group.models.Post>
        """
        posts = Post.objects.none()
        for group in group_queryset:
            posts |= Post.objects.filter(is_published=False, group=group)
        return posts


class RoleFilter(ObjectPermissionsFilter):
    """
    Extended in a way, to include all the accessible objects not
    only by personal permissions, but also by default ones.

    :example:

        User doesn't have a personal permission
        (granted by some role) to view posts,
        but it is provided by the default role
    """

    def filter_queryset(self, request, queryset, view):
        """
        Additionally returns queryset, granted by default role.
        """
        group = view.get_group()

        # owner special case
        if request.user == group.owner:
            return queryset

        filtered_queryset = (
            super(RoleFilter, self)
            .filter_queryset(request, queryset, view)
            .all()
        )

        permission = self.perm_format % {
            "app_label": queryset.model._meta.app_label,
            "model_name": queryset.model._meta.model_name,
        }
        default_role_permissions = (
            group.get_default_role()
            .groupobjectpermission_set.annotate(
                object_pk_as_int=Cast("object_pk", output_field=IntegerField())
            )
            .filter(permission__codename__exact=permission.split(".")[1])
            .values_list("object_pk_as_int", flat=True)
        )

        default_queryset = Role.objects.filter(id__in=default_role_permissions)

        return filtered_queryset | default_queryset
