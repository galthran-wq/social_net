from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.relations import PrimaryKeyRelatedField

from .models import Role
from ..users.models import CustomUser
from ..users.serializers import ProfileSerializer


class ObjectPermissionTargetField(serializers.Field):
    """
    Custom serializer field, that is used to
    work with object permission's target.
    """

    def to_representation(self, target):
        """
        :param target: Object permission's target
        :type target: groups.models.Role or groups.models.Group
        :returns: None, if target is a Group, else: name of the role
        :rtype: None or str
        """
        role_content_type = ContentType.objects.get_for_model(Role)

        if target is not None:
            target_content_type = ContentType.objects.get_for_model(target)
            if target_content_type == role_content_type:
                return target.name
        return None

    def to_internal_value(self, role_target: str):
        """
        :param role_target: Either name of the role or None, in case of group
        :type role_target: str or None
        :raises ValidationError: if :attribute:`role_target` isn't None
        and doesn't match any Role.
        """
        target = None
        if role_target is not None:
            try:
                target = Role.objects.get(name=role_target)
            except Role.DoesNotExist:
                raise ValidationError(
                    _(f'Role with name "{role_target}" does not exist')
                )

        return target


class CustomUserRelatedField(PrimaryKeyRelatedField):
    def to_representation(self, user):
        """
        Return serialized user's profile data
        as representation

        :param user:
        :return: serialized :class:`.Profile`
        :rtype: serialized :class:`.Profile`
        """
        return ProfileSerializer(
            user.profile
        ).data

    def use_pk_only_optimization(self):
        return False
