from django.db.models import Count
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from guardian.models import GroupObjectPermission
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .mixins.serializers import RateableSerializerMixin
from .mixins.views import ShortenFieldMixin
from ..users.models import Profile, CustomUser
from .fields import ObjectPermissionTargetField, CustomUserRelatedField
from .models import Group, Post, PostComment, Role
from .validators import NewPermissionValidator
from ..users.serializers import ProfileSerializer


class GroupSerializer(serializers.ModelSerializer,
                      ShortenFieldMixin):
    """
    Besides default ModelSerializer flow:
        #. adds **followers** field and corresponding getter
        #. overrides create to include user from context as the new owner
    """
    followers = serializers.SerializerMethodField()
    follow_queue = serializers.SerializerMethodField()
    owner = CustomUserRelatedField(read_only=True)
    is_following = serializers.SerializerMethodField()
    is_follow_requested = serializers.SerializerMethodField()

    def get_followers(self, group):
        """
        Gets associated group's follower role,
        fetches the first page of members, serializes them

        :returns: List of serialized followers
        """
        follower_role = group.get_role_by_name("follower")
        followers = Profile.objects.filter(
            user__in=follower_role.user_set.all()
        )
        return self.shorten_field(followers, ProfileSerializer)

    def get_is_following(self, group):
        user = self.context["request"].user
        follower_role = group.get_role_by_name("follower")
        return follower_role.user_set.filter(id=user.id).exists()

    def get_is_follow_requested(self, group):
        return group.follow_queue.filter(
            id=self.context["request"].user.id
        ).exists()

    def get_follow_queue(self, group):
        follow_queue = group.follow_queue.all()
        follow_queue_profiles = Profile.objects.filter(
            user__in=follow_queue
        )
        return self.shorten_field(
            follow_queue_profiles,
            ProfileSerializer
        )

    def create(self, validated_data):
        """
        Adds user from context as *owner* to validated_data

        :param validated_data:
        :return: new :class:`.Group` instance
        """
        validated_data.update({"owner": self.context["request"].user})
        return super(GroupSerializer, self).create(validated_data)

    class Meta:
        model = Group
        fields = [
            "id",
            "title",
            "description",
            "followers",
            "follow_queue",
            "owner",
            "created_at",
            "updated_at",
            "is_following",
            "is_follow_requested"
        ]
        read_only_fields = [
            "id",
            "followers",
            "owner",
            "follow_queue",
            "created_at",
            "updated_at",
            "is_follow_requested"
        ]


class FollowGroupSerializer(GroupSerializer):
    """
    Supports the same serialization flow as :class:`GroupSerializer`

    New field *user* - user, that is trying to follow
    """

    user = CustomUserRelatedField(
        write_only=True,
        queryset=CustomUser.objects.all()
    )

    def validate_user(self, user):
        """
        :param user:
        :type user: :class:`.CustomUser`
        :raises ValidationError: If user is already following the group.
        :return: user
        """
        group = self.instance
        if group.followers.filter(id=user.id).exists():
            raise ValidationError(_("User is already following the group"))
        return user

    def create(self, validated_data):
        """
        :raises  NotImplementedError: create is restricted.
        """
        raise NotImplementedError

    def update(self, group, validated_data):
        """
        Update's group followers to include provided user.

        :param group:
        :type group: :class:`.Group`
        :param validated_data:
        :return: group
        """
        group.edit_follower(validated_data["user"])
        return group

    class Meta:
        model = Group
        new_fields = ["user"]
        fields = GroupSerializer.Meta.fields + new_fields
        read_only_fields = GroupSerializer.Meta.fields


class LeaveGroupSerializer(GroupSerializer):
    """
    Supports the same serialization flow as :class:`GroupSerializer`

    New field *user* - user, that is trying to leave
    """

    user = CustomUserRelatedField(
        write_only=True,
        queryset=CustomUser.objects.all()
    )

    def validate_user(self, user):
        """
        :param user:
        :type user: :class:`.CustomUser`
        :raises ValidationError: If user is group owner
        :returns: user
        """
        group = self.instance
        if user == group.owner:
            raise ValidationError(
                _("Owner cannot leave group. Use delete instead")
            )
        elif not group.followers.filter(id=user.id):
            raise ValidationError(_("User is not following the group"))
        return user

    def create(self, validated_data):
        """
        :raises  NotImplementedError: create is restricted.
        """
        raise NotImplementedError

    def update(self, group, validated_data):
        """
        Update's group followers to exclude provided user.

        :param group:
        :type group: :class:`.Group`
        :param validated_data:
        :return: group
        """
        group.edit_follower(validated_data["user"], to_add=False)
        return group

    class Meta:
        model = Group
        new_fields = ["user"]
        fields = GroupSerializer.Meta.fields + new_fields
        read_only_fields = GroupSerializer.Meta.fields


class RequestFollowGroupSerializer(GroupSerializer):
    """
    Supports the same serialization flow as :class:`GroupSerializer`

    New field *user* - user, that is trying to request follow
    """

    user = CustomUserRelatedField(
        write_only=True,
        queryset=CustomUser.objects.all()
    )

    def validate_user(self, user):
        """
        :raises ValidationError: If user is already following the group
        :rtype: :class:`.CustomUser`
        """
        group = self.instance
        if group.followers.filter(id=user.id).exists():
            raise ValidationError(_("User is already following group"))
        return user

    def create(self, validated_data):
        """
        :raises  NotImplementedError: Create is restricted
        """
        raise NotImplementedError

    def update(self, group, validated_data):
        """
        Adds user to follow queue.

        :param group:
        :type group: :class:`.Group`
        :param validated_data:
        :rtype: group
        """
        group.request_follow(validated_data["user"])
        return group

    class Meta:
        model = Group
        new_fields = ["user"]
        fields = GroupSerializer.Meta.fields + new_fields
        read_only_fields = GroupSerializer.Meta.fields


class CancelFollowGroupRequestSerializer(GroupSerializer):
    """
    Supports the same serialization flow as :class:`GroupSerializer`

    New field *user* - user, that is trying to cancel follow request
    """

    user = CustomUserRelatedField(
        write_only=True,
        queryset=CustomUser.objects.all()
    )

    def validate_user(self, user):
        """
        :raises ValidationError: If user is not in follow queue
        :rtype: :class:`.CustomUser`
        """
        group = self.instance
        if not group.follow_queue.filter(id=user.id).exists():
            raise ValidationError(_("User is not in follow queue"))
        return user

    def create(self, validated_data):
        """
        :raises  NotImplementedError: Create is restricted
        """
        raise NotImplementedError

    def update(self, group, validated_data):
        """
        Removes user from follow queue.

        :param group:
        :type group: :class:`.Group`
        :param validated_data:
        :rtype: group
        """
        group.cancel_follow_request(validated_data["user"])
        return group

    class Meta:
        model = Group
        new_fields = ["user"]
        fields = GroupSerializer.Meta.fields + new_fields
        read_only_fields = GroupSerializer.Meta.fields


class ApproveFollowGroupSerializer(GroupSerializer):
    """
    Supports the same serialization flow as :class:`GroupSerializer`

    New field *users* - list of users, that are  to be approved
    """

    users = CustomUserRelatedField(
        many=True,
        write_only=True,
        queryset=CustomUser.objects.all()
    )

    def validate_users(self, users):
        """
        :param users:
        :type users: :class:`.CustomUser`
        :raises ValidationError: If any of the users is not in follow queue
        :return: users
        """
        group = self.instance
        for user in users:
            if not group.follow_queue.filter(id=user.id).exists():
                raise ValidationError(
                    _(
                        f"User {user.id} has not requested "
                        f"to follow the group"
                    )
                )
        return users

    def create(self, validated_data):
        """
        :raises  NotImplementedError: Create is restricted
        """
        raise NotImplementedError

    def update(self, group, validated_data):
        """
        Removes users from follower queue
        and adds them to followers (assigns corresponding role)

        :param  group:
        :type group: :class:`.Group`
        :param validated_data:
        :rtype: group
        """
        new_followers = self.validated_data["users"]
        group.approve_follower(*new_followers)
        return group

    class Meta:
        model = Group
        new_fields = ["users"]
        fields = GroupSerializer.Meta.fields + new_fields
        read_only_fields = GroupSerializer.Meta.fields


class BasePostSerializer(serializers.ModelSerializer,
                         ShortenFieldMixin,
                         RateableSerializerMixin):
    author = CustomUserRelatedField(read_only=True)
    best_comment = serializers.SerializerMethodField()
    comments_amount = serializers.SerializerMethodField()

    def get_best_comment(self, post):
        try:
            best_comment = post.comments.annotate(
                Count('liked_by')
            ).order_by('-liked_by__count')[:1].get()
            return PostCommentSerializer(
                best_comment,
                context={"request": self.context["request"]}
            ).data
        except:
            pass

    def get_comments_amount(self, post):
        return post.comments.count()

    class Meta:
        model = Post
        fields = [
            "id",
            "group",
            "author",
            "title",
            "body",
            "is_published",
            "liked_by",
            "disliked_by",
            "publishing_datetime",
            "created_at",
            "updated_at",
            "is_liked",
            "is_disliked",
            "best_comment",
            "comments_amount"
        ]


class CreatePostSerializer(BasePostSerializer):
    """
    Defines fields, that are to be accepted on instance create.
    """

    class Meta(BasePostSerializer.Meta):
        read_only_fields = [
            "id",
            "author",
            "is_published",
            "liked_by",
            "disliked_by",
            "publishing_datetime",
            "created_at",
            "updated_at",
        ]

    def create(self, validated_data):
        """
        Adds user from context as *author* to validated_data

        :param validated_data:
        :rtype: :class:`.Post`
        """
        validated_data.update(
            {"author": self.context["request"].user, }
        )
        return super(CreatePostSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        """
        :raises  NotImplementedError: Update is restricted
        """
        raise NotImplementedError


class UpdatePostSerializer(BasePostSerializer):
    """
    Defines fields, that are accepted on instance update

    Same as in :class:`.CreatePostSerializer`,
    but without *author* and *group* (they are unchangeable)
    """

    class Meta(BasePostSerializer.Meta):
        read_only_fields = [
            "id",
            "author",
            "is_published",
            "group",
            "liked_by",
            "disliked_by",
            "publishing_datetime",
            "created_at",
            "updated_at",
        ]

    def create(self, validated_data):
        """
        :raises  NotImplementedError: Create is restricted
        """
        raise NotImplementedError


class PostScheduleSerializer(BasePostSerializer):
    """
    Accepts only new *delay_in_minutes* field.
    Schedules post publishing on instance update
    """

    delay_in_minutes = serializers.IntegerField(min_value=0, write_only=True)

    def create(self, validated_data):
        """
        :raises NotImplementedError: Create is restricted
        """
        raise NotImplementedError

    def update(self, post, validated_data):
        """
        Schedules post according to provided delay

        :param post:
        :type post: :class:`.Post`
        :param validated_data:
        :return: post
        """
        post.schedule(validated_data["delay_in_minutes"])
        return post

    class Meta(BasePostSerializer.Meta):
        model = Post
        new_fields = ["delay_in_minutes"]
        read_only_fields = BasePostSerializer.Meta.fields
        fields = read_only_fields + new_fields


class ObjectPermissionSerializer(serializers.ModelSerializer):
    """
    Serializes :class:`.GroupObjectPermission` instance.
    Defines two fields:
        #. *name* - name of the permission
        #. *target* - target of the permission
    """
    # . Uses custom :class:`.ObjectPermissionTargetField`
    target = ObjectPermissionTargetField(
        allow_null=True, source="content_object", required=False
    )
    name = serializers.SlugRelatedField(
        slug_field="codename",
        queryset=Permission.objects.all(),
        source="permission",
    )

    class Meta:
        model = GroupObjectPermission
        fields = ["name", "target"]


class RoleSerializer(serializers.ModelSerializer):
    """
    Adds 2 new fields, besides ModelSerializer's:
        #. *permissions* - list of serialized role's permissions
        #. *users* - id list of role's users
    """
    group = serializers.PrimaryKeyRelatedField(
        source="social_group",
        read_only=True
    )
    permissions = ObjectPermissionSerializer(
        many=True,
        source="groupobjectpermission_set",
        validators=[NewPermissionValidator()],
        required=False,
    )
    users = CustomUserRelatedField(
        many=True,
        queryset=CustomUser.objects.all(),
        source="user_set",
        required=False,
    )

    def create(self, validated_data):
        """
        Changes role's name to appropriate one.
        (to avoid clashing between different group roles)

        :param validated_data:
        :return:
        """
        group = self.context["group"]
        validated_data.update(
            {
                "social_group": group,
                "name": group.get_role_name(validated_data.pop("name")),
            }
        )
        return super(RoleSerializer, self).create(validated_data)

    def update(self, role, validated_data):
        """
        If blacklist role's users are edited,
        remove all their roles.

        Defines a custom flow of role permissions updating
        according to the serialized permission representation

        :param :class:`.Role` role:
        :param validated_data:
        :returns: role
        """
        # if blacklist users are edited
        try:
            users = validated_data["user_set"]
            group = self.context["group"]
            blacklist_role = group.get_role_by_name("blacklist")
            if role == blacklist_role:
                for user in users:
                    for obtained_role in group.roles.filter(
                        user__in=[user]
                    ).all():
                        obtained_role.edit_member(user, to_add=False)
        except KeyError:
            pass
        # if role's permissions are edited
        try:
            permissions = validated_data.pop("groupobjectpermission_set")
            role.groupobjectpermission_set.all().delete()
            for permission in permissions:
                target = permission.get("content_object", None)
                role.edit_permission(
                    permission["permission"].codename, target=target
                )
        finally:
            return super(RoleSerializer, self).update(role, validated_data)

    def validate_users(self, users):
        """
        :param :class:`.CustomUser` users:
        :raises ValidationError: If any of the users is not a group follower
        :return: users
        """
        group = self.context["group"]

        for user in users:
            if not group.followers.filter(id=user.id):
                raise ValidationError(
                    _(f"User {user.id} is not following group.")
                )

        return users

    class Meta:
        model = Role
        fields = ["name", "users", "permissions", "group"]
        read_only_fields = ["group"]


class PostCommentSerializer(serializers.ModelSerializer,
                            ShortenFieldMixin,
                            RateableSerializerMixin):
    author = CustomUserRelatedField(read_only=True)

    class Meta:
        model = PostComment
        fields = [
            "id",
            "post",
            "author",
            "body",
            "liked_by",
            "disliked_by",
            "is_liked",
            "is_disliked",
            "created_at",
            "updated_at",
        ]
        read_only_fields = [
            "id",
            "post",
            "author",
            "liked_by",
            "disliked_by",
            "is_liked",
            "is_disliked",
            "created_at",
            "updated_at",
        ]

    def create(self, validated_data):
        """
        Appends *user* and *post* from context as
        new comment *author* and *post* respectively.

        :param validated_data:
        :rtype: :class:`.PostComment`
        """
        validated_data.update(
            {
                "author": self.context["request"].user,
                "post": self.context["post"],
            }
        )
        return super(PostCommentSerializer, self).create(validated_data)
