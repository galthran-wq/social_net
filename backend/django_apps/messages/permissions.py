from rest_framework.permissions import BasePermission


SAFE_METHODS = [
    "GET",
    "HEAD",
    "OPTIONS",
]


class MessageDetailPermissions(BasePermission):
    def has_object_permission(self, request, view, message):
        """
        Ensures message detail can be edited
        by message.author only

        :return: Permission status
        :rtype: bool
        """
        return request.user == message.author or request.method in SAFE_METHODS


class DialogueDetailPermissions(BasePermission):
    def has_object_permission(self, request, view, dialogue):
        """
        Ensures dialogue can be edited (but not deleted)
        only by dialogue's owner

        :return: Permission status
        :rtype: bool
        """
        # no one can delete
        if request.method == "DELETE":
            return False
        return request.user == dialogue.creator


class LeaveDialoguePermissions(BasePermission):
    def has_object_permission(self, request, view, dialogue):
        """
        Ensures owner can't leave dialogue

        :return: Permission status
        :rtype: bool
        """
        return dialogue.creator != request.user
