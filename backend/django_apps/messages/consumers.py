from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from django.utils.translation import gettext_lazy as _
from twisted.protocols.memcache import ClientError

from django_apps.messages.models import Dialogue


class Consumer(JsonWebsocketConsumer):
    # . Name of the broadcast group
    BROADCAST_GROUP_NAME = "broadcast"
    # . Pattern of the dialogue groups
    DIALOGUE_GROUP_NAME_PATTERN = "dialogue_{}"
    # . Initialize groups with the Broadcast
    groups = [BROADCAST_GROUP_NAME]

    @staticmethod
    def get_dialogue_group_name(dialogue_id):
        """
        Return group name for specific dialogue

        :param dialogue_id: Id of the dialogue
        :rtype: str
        """
        return Consumer.DIALOGUE_GROUP_NAME_PATTERN.format(dialogue_id)

    def manage_groups(self, *, to_add=True):
        """
        Connects (or disconnects) user to (from) all the corresponding groups.

        :example:

            We want user to be connected to all the dialogue groups when he
            enters the site.

        :param to_add: Flag, used to determine whether to
        connect or disconnect the user.
        """
        if to_add:
            action_method = async_to_sync(self.channel_layer.group_add)
        else:
            action_method = async_to_sync(self.channel_layer.group_discard)

        # Add all dialogues, corresponding to the user
        user_dialogues = Dialogue.objects.filter(
            participants__id=self.scope["user"].id
        )
        for dialogue_id in user_dialogues.values_list("id", flat=True):
            action_method(
                self.get_dialogue_group_name(dialogue_id), self.channel_name,
            )

    def connect(self):
        """
        Ensures user is not anonymous, then connects the user
        to all the corresponding groups via :method:`.manage_groups`
        """
        if self.scope["user"].is_anonymous:
            self.close()
        else:
            self.accept()

        self.manage_groups()

    def disconnect(self, close_code):
        """
        Disconnects user from all the corresponding groups
        via :method:`.manage_groups`
        """
        self.manage_groups(to_add=False)

    def receive_json(self, content, **kwargs):
        """
        Reads an event-type from *content*.
        Fetches corresponding event handler.

        :raises ClientError: Wrong event type. Handler error.
        """
        event = content.get("event", None)

        try:
            handler = self.get_event_handler_method(event)
            if handler is None:
                raise ClientError(
                    _('Event "{}" is not supported').format(event)
                )
            else:
                handler(content)
        except ClientError as e:
            self.send_json({"errors": e.args})

    def get_event_handler_method(self, event):
        """
        Translates event of type text.text to text_text,
        and returns method with such name if one exists'
        :returns: Corresponding event handler or None
        """
        try:
            return getattr(self, event.replace(".", "_"), None)
        except AttributeError:
            return None

    # Client-invoked Events
    # ...

    # System-invoked events
    def dialogue_message_created(self, content):
        """
        Sends content to the underlying user

        :param content: Serialized message
        """
        self.send_json(content)

    def dialogue_message_edited(self, content):
        """
        Sends content to the underlying user

        :param content: Serialized message
        """
        self.send_json(content)

    def dialogue_participants_added(self, content):
        """
        Gets send to all the dialogue members (including new ones)
        with corresponding event types.

        New users will receive message with *dialogue.self_added* event type,
        others - *dialogue.added*

        :param content: Serialized dialogue
        """
        user = self.scope["user"]
        dialogue_id = content["dialogue"]
        new_participants = content["participants"]

        # if the user is in the dialogue members
        if dialogue_id in user.dialogue_set.values_list("id", flat=True):
            # if the user is newly added
            if user.id in new_participants:
                # update corresponding event type
                content.update({"type": "dialogue.self_added"})

                # add user to the dialogue group
                async_to_sync(self.channel_layer.group_add)(
                    self.DIALOGUE_GROUP_NAME_PATTERN.format(dialogue_id),
                    self.channel_name,
                )
            else:
                # update event type
                content.update({"type": "dialogue.added"})
            # send event to the underlying user
            self.send_json(content)

    def dialogue_participants_removed(self, content):
        """
        Gets send to all the dialogue members (including new ones)
        with corresponding event types.

        Removed users will receive message with *dialogue.self_removed* event type,
        others - *dialogue.removed*

        :param content: Serialized dialogue
        """
        user = self.scope["user"]
        dialogue_id = content["dialogue"]
        removed_participants = content["participants"]

        # if the user is in the dialogue members
        if dialogue_id in user.dialogue_set.values_list("id", flat=True):
            # if user was removed from that dialogue
            if user.id in removed_participants:
                content.update({"type": "dialogue.self_removed"})

                # remove user from the dialogue group
                async_to_sync(self.channel_layer.group_discard)(
                    self.DIALOGUE_GROUP_NAME_PATTERN.format(dialogue_id),
                    self.channel_name,
                )
            else:
                content.update({"type": "dialogue.removed"})
            # send event message
            self.send_json(content)
