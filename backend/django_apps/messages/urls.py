from rest_framework.routers import DefaultRouter

from django_apps.messages.views import DialogueViewSet, MessageViewSet

app_name = "messages"

router = DefaultRouter()
router.register("dialogues", DialogueViewSet, basename="dialogue")
router.register("messages", MessageViewSet, basename="message")

urlpatterns = router.urls

# todo
# consumer test conversation time-to-time errors
