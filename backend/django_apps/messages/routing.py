from django.conf.urls import url

from .consumers import Consumer

websocket_urlpatterns = [
    url("^ws/$", Consumer),
]
