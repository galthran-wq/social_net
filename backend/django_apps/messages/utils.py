from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from django_apps.messages.consumers import Consumer


def dispatch_dialogue_participants_added_event(
    dialogue_id, new_participants_id: list
):
    """
    Helper for dispatching *participants_added* system event

    :param dialogue_id: id of the dialogue
    :param new_participants_id: list of new participants ids
    """
    async_to_sync(get_channel_layer().group_send)(
        Consumer.BROADCAST_GROUP_NAME,
        {
            "type": "dialogue.participants_added",
            "dialogue": dialogue_id,
            "participants": new_participants_id,
        },
    )


def dispatch_dialogue_participants_removed_event(
    dialogue_id, removed_participants_id: list
):
    """
    Helper for dispatching *participants_removed* system event

    :param dialogue_id: id of the dialogue
    :param removed_participants_id: list of new participants ids
    """
    async_to_sync(get_channel_layer().group_send)(
        Consumer.get_dialogue_group_name(dialogue_id),
        {
            "type": "dialogue.participants_removed",
            "dialogue": dialogue_id,
            "participants": removed_participants_id,
        },
    )


def dispatch_message_created_event(new_message, **new_message_data):
    """
    Helper for dispatching *message_created* system event

    :param new_message: new message object
    :type new_message: :class:`.Message`
    :param new_message_data: serialized new message data
    """
    async_to_sync(get_channel_layer().group_send)(
        Consumer.get_dialogue_group_name(new_message.dialogue.id),
        dict({"type": "dialogue.message_created", }, **new_message_data),
    )


def dispatch_message_edited_event(updated_message, **message_data):
    """
    Helper for dispatching *message_edited* system event

    :param updated_message: edited message object
    :type updated_message: :class:`.Message`
    :param message_data: serialized new message data
    """
    async_to_sync(get_channel_layer().group_send)(
        Consumer.get_dialogue_group_name(updated_message.dialogue.id),
        dict({"type": "dialogue.message_edited", }, **message_data),
    )
