from django.apps import AppConfig


class MessagesConfig(AppConfig):
    name = "django_apps.messages"
