from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from django_apps.messages.models import Dialogue, Message
from django_apps.messages.permissions import (
    MessageDetailPermissions,
    DialogueDetailPermissions,
    LeaveDialoguePermissions,
)
from django_apps.messages.serializers import (
    CreateDialogueSerializer,
    CreateMessageSerializer,
    RemoveDialogueParticipantsSerializer,
    AddDialogueParticipantsSerializer,
    UpdateMessageSerializer,
)


class DialogueViewSet(ModelViewSet):
    lookup_url_kwarg = "dialogue_id"
    serializer_class = CreateDialogueSerializer
    permission_classes = (IsAuthenticated & DialogueDetailPermissions,)

    def get_queryset(self):
        """
        Gets dialogue queryset for request.user

        :rtype: :class:`.Queryset`<:class:`.Dialogue`>
        """
        return Dialogue.objects.filter(participants=self.request.user)

    @action(
        detail=True,
        methods=["post"],
        serializer_class=AddDialogueParticipantsSerializer,
        permission_classes=(IsAuthenticated,),
    )
    def add_participants(self, *args, **kwargs):
        """
        Invokes update with redefined serializer_class:
        :class:`.AddDialogueParticipantsSerializer`

        Redefines *permission_classes* to:
        :class:`.IsAuthenticated`

        Accepts only POST requests.
        """
        return self.update(self.request, *args, **kwargs)

    @action(
        detail=True,
        methods=["post"],
        serializer_class=RemoveDialogueParticipantsSerializer,
    )
    def remove_participants(self, *args, **kwargs):
        """
        Invokes update with redefined serializer_class:
        :class:`.RemoveDialogueParticipantsSerializer`

        Accepts only POST requests.
        """
        return self.update(self.request, *args, **kwargs)

    @action(
        detail=True,
        methods=["post"],
        permission_classes=(IsAuthenticated & LeaveDialoguePermissions,),
    )
    def leave(self, *args, **kwargs):
        """
        Fetches detail object via :method:`.get_object` method
        Invokes dialogue :method:`.edit_participants` method

        Redefines *permission_classes* to:
        :class:`.IsAuthenticated` and :class:`.LeaveDialoguePermissions`

        Accepts only POST requests.
        """
        dialogue = self.get_object()
        dialogue.edit_participants(self.request.user, to_add=False)
        return Response(data=self.get_serializer(dialogue).data)


class MessageViewSet(ModelViewSet):
    lookup_url_kwarg = "message_id"
    serializer_class = CreateMessageSerializer
    permission_classes = (IsAuthenticated & MessageDetailPermissions,)

    def get_queryset(self):
        """
        Return all user messages
        (Excluding ignored ones)

        :rtype: :class:`.Queryset`<:class:`.Message`>
        """
        return Message.objects.filter(
            dialogue__participants=self.request.user,
        ).exclude(ignored_by=self.request.user)

    def get_serializer_class(self):
        """
        Returns appropriate serializer class, depending on request method.
        :class:`.CreateMessageSerializer` for POST requests, else:
        :class:`.UpdateMessageSerializer`

        :return: serializer class
        """
        return (
            CreateMessageSerializer
            if self.request.method == "POST"
            else UpdateMessageSerializer
        )

    @action(
        detail=True, methods=["post"], permission_classes=(IsAuthenticated,),
    )  # anyone can ignore messages
    def ignore(self, request, *args, **kwargs):
        """
        Fetches detail object via :method:`.get_object` method
        Invokes message :method:`.ignore` method

        Redefines *permission_classes* to:
        :class:`.IsAuthenticated`

        Accepts only POST requests.
        """
        message = self.get_object()
        message.ignore(request.user)
        return Response()
