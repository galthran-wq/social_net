from django.db import models

from django_apps.users.models import CustomUser


class Dialogue(models.Model):
    creator = models.ForeignKey(
        CustomUser,
        null=True,
        on_delete=models.SET_NULL,
        related_name="creator",
    )
    participants = models.ManyToManyField(CustomUser)

    def edit_participants(self, *users, to_add=True):
        """
        Edits dialogue participants.
        Action is chosen according to the *to_add* flag.

        :param users: users to be edited
        :param to_add: Adds participants, if True, else: Removes them instead
        """
        if to_add:
            action_method = self.participants.add
        else:
            action_method = self.participants.remove

        action_method(*users)
        self.save()


class Message(models.Model):
    dialogue = models.ForeignKey(
        Dialogue, on_delete=models.CASCADE, related_name="messages"
    )
    author = models.ForeignKey(
        CustomUser,
        null=True,
        on_delete=models.SET_NULL,
        related_name="messages",
    )
    body = models.CharField(max_length=255)
    # . Users can ignore (hide (delete 1 side)) messages
    # Intended to be populated by message.dialogue.participants
    ignored_by = models.ManyToManyField(CustomUser, default=[])

    def ignore(self, user):
        """
        Makes *user* ignore this message.

        :param user: ignore
        """
        self.ignored_by.add(user)
        self.save()
