from django.utils.translation import gettext_lazy as _
from rest_framework import serializers, pagination
from rest_framework.exceptions import ValidationError

from django_apps.messages import utils
from django_apps.messages.models import Message, Dialogue


class CreateMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ["id", "body", "author", "dialogue"]
        read_only_fields = ["author", "id"]

    def validate_dialogue(self, dialogue):
        """
        Ensures user from context is dialogue participant

        :param dialogue: dialogue object
        :type dialogue: :class:`.Dialogue`
        :raises ValidationError: if user is not a dialogue participant
        :return: dialogue
        """
        if self.context["request"].user not in dialogue.participants.all():
            raise ValidationError(
                _("You are not a participant of this dialogue!")
            )
        return dialogue

    def create(self, validated_data):
        """
        Add *author* from context to validated_data.
        Additionally invokes :func:`.dispatch_message_created_event` helper
        """
        validated_data.update({"author": self.context["request"].user})
        new_message = super(CreateMessageSerializer, self).create(
            validated_data
        )

        self.instance = new_message
        utils.dispatch_message_created_event(new_message, **self.data)

        return new_message

    def update(self, instance, validated_data):
        """
        Update is restricted.
        Use :class:`.UpdateMessageSerializer` instead.

        :raises NotImplementedError:
        """
        raise NotImplementedError


class UpdateMessageSerializer(serializers.ModelSerializer):
    class Meta(CreateMessageSerializer.Meta):
        """
        Makes *author* field read only, thus preventing it
        from updating
        """
        read_only_fields = ["id", "author", "dialogue"]

    def create(self, validated_data):
        """
        Create is restricted.
        Use :class:`.CreateMessageSerializer` instead.

        :raises NotImplementedError:
        """
        raise NotImplementedError

    def update(self, instance, validated_data):
        """
        Additionally invokes :func:`.dispatch_message_updated_event` helper
        """
        updated_message = super(UpdateMessageSerializer, self).update(
            instance, validated_data
        )

        utils.dispatch_message_edited_event(instance, **self.data)

        return updated_message


class CreateDialogueSerializer(
    serializers.ModelSerializer
):
    messages = serializers.SerializerMethodField()

    class Meta:
        model = Dialogue
        fields = ["id", "participants", "messages", "creator"]
        read_only_fields = ["id", "messages", "creator"]

    def validate_participants(self, participants):
        """
        Ensures user can create dialogue with his friends only.

        :param participants: :class:`.QuerySet`<:class:`.CustomUser`>
        :raises ValidationError: if any of the participants is not in
        request.user friend list
        """
        request_user = self.context["request"].user
        request_user_friends = request_user.profile.friends

        for user in participants:
            if (
                    user.profile not in request_user_friends.all()
                    and request_user != user
            ):
                raise ValidationError(
                    _("User {} is not in your friend list").format(user.id)
                )
        return participants

    def get_messages(self, dialogue):
        """
        Returns the first page of latest messages

        :param dialogue: dialogue object
        :type dialogue: :class:`Dialogue`
        :return: first page of serialized messages
        """
        messages = Message.objects.filter(dialogue=dialogue).order_by("-id")
        paginator = pagination.PageNumberPagination()
        page = paginator.paginate_queryset(messages, self.context["request"])
        serializer = CreateMessageSerializer(
            page, many=True, context={"request": self.context["request"]},
        )
        return serializer.data

    def create(self, validated_data):
        """
        Add *author* from context to validated_data.
        Additionally invokes :func:`.dispatch_message_created_event` helper
        """
        new_participants = validated_data["participants"]
        validated_data.update({"creator": self.context["request"].user})
        new_dialogue = super(CreateDialogueSerializer, self).create(
            validated_data
        )

        # send an event to websocket consumers
        self.instance = new_dialogue
        utils.dispatch_dialogue_participants_added_event(
            new_dialogue.id, [user.id for user in new_participants]
        )

        return new_dialogue

    def update(self, instance, validated_data):
        """
        Update is restricted.
        Use :class:`.UpdateDialogueSerializer` instead.

        :raises NotImplementedError:
        """
        raise NotImplementedError


class UpdateDialogueSerializer(serializers.ModelSerializer):
    """
    Currently, there is nothing to update in the dialogue object.
    """
    class Meta:
        model = Dialogue
        fields = ["id", "participants", "messages", "creator"]
        read_only_fields = [
            "id",
            "participants",
            "messages",
            "creator",
        ]

    def create(self, validated_data):
        """
        Create is restricted.
        Use :class:`.CreateDialogueSerializer` instead.

        :raises NotImplementedError:
        """
        raise NotImplementedError

    def update(self, instance, validated_data):
        """
        Restricted till there is something
        to update.

        :raises NotImplementedError:
        """
        raise NotImplementedError


class AddDialogueParticipantsSerializer(CreateDialogueSerializer):
    """
    Changes edit flow to append
    """
    class Meta(CreateDialogueSerializer.Meta):
        pass

    def create(self, validated_data):
        """
        Create is restricted.
        Use :class:`.CreateDialogueSerializer` instead.

        :raises NotImplementedError:
        """
        raise NotImplementedError

    def update(self, dialogue, validated_data):
        """
        Adds new participants to the dialogue.
        Invokes :func:`.dispatch_dialogue_participants_added_event` helper

        :param validated_data: validated data
        :param dialogue: Dialogue object
        :type dialogue: :class:`.Dialogue`
        """
        new_participants = validated_data.pop("participants")
        new_participants_ids = [
            participant.id for participant in new_participants
        ]
        dialogue.edit_participants(*new_participants_ids)

        # send an event to websocket consumers
        utils.dispatch_dialogue_participants_added_event(
            dialogue.id, new_participants_ids
        )

        return dialogue


class RemoveDialogueParticipantsSerializer(CreateDialogueSerializer):
    """
    Changes edit flow to discard
    """
    class Meta(CreateDialogueSerializer.Meta):
        pass

    def validate_participants(self, participants):
        """
        Overrides super's validation to check whether
        everyone from the *participants* is a dialogue participant

        :raises ValidationError: If any one from the list is not
        a dialogue participant
        :param participants: :class:`.QuerySet`<:class:`.CustomUser`>
        :return: participants
        """
        dialogue = self.instance
        dialogue_participants = dialogue.participants.all()

        for user in participants:
            if user not in dialogue_participants:
                raise ValidationError(
                    _(f"User {user} is not dialogue participants")
                )
        return participants

    def create(self, validated_data):
        """
        Create is restricted.
        Use :class:`.CreateDialogueSerializer` instead.

        :raises NotImplementedError:
        """
        raise NotImplementedError

    def update(self, dialogue, validated_data):
        """
        Removes users from dialogue.
        Invokes :func:`.dispatch_dialogue_participants_removed_event` helper
        """
        removed_participants = validated_data.pop("participants")
        removed_participants_ids = [
            participant.id for participant in removed_participants
        ]
        dialogue.edit_participants(*removed_participants_ids, to_add=False)

        # send an event to websocket consumers
        utils.dispatch_dialogue_participants_removed_event(
            dialogue.id, removed_participants_ids
        )

        return dialogue
