from unittest.mock import patch

from django.test import TestCase, override_settings

from django_apps.messages.consumers import Consumer
from django_apps.messages.models import Message, Dialogue
from django_apps.messages.serializers import CreateMessageSerializer
from django_apps.messages.utils import (
    dispatch_message_edited_event,
    dispatch_message_created_event,
    dispatch_dialogue_participants_added_event,
    dispatch_dialogue_participants_removed_event,
)
from django_apps.users.tests import AuthorizedTestRequestsMixin


@override_settings(
    CHANNEL_LAYERS={
        "default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}
    }
)
@patch("channels.layers.InMemoryChannelLayer.group_send")
class UtilsTest(AuthorizedTestRequestsMixin, TestCase):
    def setUp(self) -> None:
        self.user = self.get_new_user()

        self.dialogue = Dialogue.objects.create(creator=self.user)
        self.dialogue.participants.add(self.user)

        self.message = Message.objects.create(
            dialogue=self.dialogue, author=self.user, body="test_body"
        )

    def test_dispatch_message_created_event(self, group_send):
        serialized_data = CreateMessageSerializer(self.message).data

        dispatch_message_created_event(self.message, **serialized_data)
        group_send.assert_called_with(
            Consumer.get_dialogue_group_name(self.message.dialogue.id),
            dict({"type": "dialogue.message_created", }, **serialized_data),
        )

    def test_dispatch_message_edited_event(self, group_send):
        serialized_data = CreateMessageSerializer(self.message).data
        dispatch_message_edited_event(self.message, **serialized_data)
        group_send.assert_called_with(
            Consumer.get_dialogue_group_name(self.message.dialogue.id),
            dict({"type": "dialogue.message_edited", }, **serialized_data),
        )

    def test_dispatch_participants_added_event(self, group_send):
        participants_ids = [
            user.id for user in self.dialogue.participants.all()
        ]

        dispatch_dialogue_participants_added_event(
            self.dialogue.id, participants_ids
        )
        group_send.assert_called_with(
            Consumer.BROADCAST_GROUP_NAME,
            {
                "type": "dialogue.participants_added",
                "dialogue": self.dialogue.id,
                "participants": participants_ids,
            },
        )

    def test_dispatch_participants_removed_event(self, group_send):
        participants_ids = [
            user.id for user in self.dialogue.participants.all()
        ]

        dispatch_dialogue_participants_removed_event(
            self.dialogue.id, participants_ids
        )
        group_send.assert_called_with(
            Consumer.get_dialogue_group_name(self.dialogue.id),
            {
                "type": "dialogue.participants_removed",
                "dialogue": self.dialogue.id,
                "participants": participants_ids,
            },
        )
