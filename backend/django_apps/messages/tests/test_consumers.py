import asyncio

import pytest
from channels.db import database_sync_to_async
from channels.layers import get_channel_layer
from channels.testing import WebsocketCommunicator
from django.db import transaction

from django_apps.messages.consumers import Consumer
from django_apps.messages.models import Dialogue, Message
from django_apps.messages.serializers import CreateMessageSerializer
from django_apps.users.models import CustomUser, EmailAddress


@pytest.fixture(scope="function")
def first_user():
    with transaction.atomic():
        email = EmailAddress.objects.create(
            email="some_email{}@gmail.com".format(CustomUser.objects.count())
        )
        email.save()

        first_user = CustomUser.objects.create(
            username="some_username{}".format(CustomUser.objects.count()),
            email=email,
        )
        first_user.set_password("test-password")
        first_user.save()
    return first_user


second_user = first_user


# noinspection PyShadowingNames
@pytest.fixture(scope="function")
def dialogue(first_user, second_user):
    with transaction.atomic():
        dialogue = Dialogue.objects.create()
        dialogue.participants.add(first_user.id, second_user.id)
        dialogue.save()
    return dialogue


async def create_message_helper(dialogue, author):
    """
    Messages are created via corresponding HTTP endpoints,
        which i can't use here, bcs of async context.
    These are the core steps that should be performed by a handler:
    """
    message = await database_sync_to_async(Message.objects.create)(
        dialogue=dialogue, author=author, body="test_body2"
    )
    message_data = CreateMessageSerializer(message).data
    await get_channel_layer().group_send(
        Consumer.get_dialogue_group_name(message.dialogue.id),
        dict({"type": "dialogue.message_created", }, **message_data),
    )
    return message, message_data


async def message_edit_helper(message):
    message.body = "new_body222"
    await database_sync_to_async(message.save)()

    message_data = CreateMessageSerializer(message).data
    await get_channel_layer().group_send(
        Consumer.get_dialogue_group_name(message.dialogue.id),
        dict({"type": "dialogue.message_edited", }, **message_data),
    )
    return message, message_data


async def dialogue_add_participants_helper(dialogue, *participants):
    await database_sync_to_async(dialogue.participants.add)(*participants)

    dialogue_data = {
        "dialogue": dialogue.id,
        "participants": [participant.id for participant in participants],
    }

    # same as in utils.py, but async
    await get_channel_layer().group_send(
        Consumer.BROADCAST_GROUP_NAME,
        dict({"type": "dialogue.participants_added", }, **dialogue_data),
    )
    return dialogue_data


async def dialogue_remove_participants_helper(dialogue, *participants):
    await database_sync_to_async(dialogue.participants.remove)(*participants)

    dialogue_data = {
        "dialogue": dialogue.id,
        "participants": [participant.id for participant in participants],
    }

    # same as in utils.py, but async
    await get_channel_layer().group_send(
        Consumer.get_dialogue_group_name(dialogue.id),
        dict({"type": "dialogue.participants_removed", }, **dialogue_data),
    )
    return dialogue_data


async def dialogue_create_helper(*participants, creator):
    dialogue = await database_sync_to_async(Dialogue.objects.create)(
        creator=creator
    )

    dialogue_data = await dialogue_add_participants_helper(
        dialogue, creator, *(list(participants) + [creator])
    )

    return dialogue, dialogue_data


@pytest.mark.django_db(transaction=True)
class TestConsumer:
    # noinspection PyShadowingNames
    @pytest.mark.asyncio
    async def test_message_created_and_edited(
        self, first_user, second_user, dialogue
    ):
        first_user_connection = WebsocketCommunicator(Consumer, "testws/",)
        second_user_connection = WebsocketCommunicator(Consumer, "testws/",)
        # set users, because we cannot do this via middleware, bcs here is
        # async context
        first_user_connection.scope["user"] = first_user
        second_user_connection.scope["user"] = second_user

        (connected1, subprotocol1,) = await first_user_connection.connect()
        (connected2, subprotocol2,) = await second_user_connection.connect()
        assert connected1 and connected2

        # Assert no messages
        message_count_query = database_sync_to_async(Message.objects.count)
        assert not await message_count_query()

        # create a message (separate action (hitting HTTP endpoint))
        message, message_data = await create_message_helper(
            dialogue, first_user
        )

        # accept newly created message on both users connections
        message1 = await first_user_connection.receive_json_from(timeout=5)
        message2 = await second_user_connection.receive_json_from(timeout=5)

        # Check if the message was actually created
        assert await message_count_query() == 1

        assert (
            message1
            == message2
            == dict({"type": "dialogue.message_created", **message_data})
        )

        message, message_data = await message_edit_helper(message)
        message1 = await first_user_connection.receive_json_from(timeout=5)
        message2 = await second_user_connection.receive_json_from(timeout=5)

        assert (
            message1
            == message2
            == dict({"type": "dialogue.message_edited", **message_data})
        )

        # Close
        await first_user_connection.disconnect()
        await second_user_connection.disconnect()

    # noinspection PyShadowingNames
    @pytest.mark.asyncio
    async def test_dialogue_created(self, first_user, second_user):
        first_user_connection = WebsocketCommunicator(Consumer, "testws/",)
        second_user_connection = WebsocketCommunicator(Consumer, "testws/",)
        # set users, because we cannot do this via middleware, bcs here is
        # async context
        first_user_connection.scope["user"] = first_user
        second_user_connection.scope["user"] = second_user

        (connected1, subprotocol1,) = await first_user_connection.connect()
        (connected2, subprotocol2,) = await second_user_connection.connect()
        assert connected1 and connected2

        dialogue, dialogue_data = await dialogue_create_helper(
            second_user, creator=first_user
        )
        message11 = await first_user_connection.receive_json_from(timeout=5)
        message12 = await first_user_connection.receive_json_from(timeout=5)
        message21 = await second_user_connection.receive_json_from(timeout=5)
        message22 = await second_user_connection.receive_json_from(timeout=5)
        self_added_response = dict(
            {"type": "dialogue.self_added"}, **dialogue_data
        )
        added_response = dict({"type": "dialogue.added"}, **dialogue_data)
        assert (message11, message12) == (
            self_added_response,
            added_response,
        ) or (message12, message11) == (self_added_response, added_response,)
        assert (message21, message22) == (
            self_added_response,
            added_response,
        ) or (message22, message21) == (self_added_response, added_response,)

        # Close
        await first_user_connection.disconnect()
        await second_user_connection.disconnect()

    # noinspection PyShadowingNames
    @pytest.mark.asyncio  # todo works from time to time
    async def _test_participants_editing(
        self, first_user, second_user, dialogue
    ):
        first_user_connection = WebsocketCommunicator(Consumer, "testws/",)
        second_user_connection = WebsocketCommunicator(Consumer, "testws/",)
        # set users, because we cannot do this via middleware, bcs here is
        # async context
        first_user_connection.scope["user"] = first_user
        second_user_connection.scope["user"] = second_user

        (connected1, subprotocol1,) = await first_user_connection.connect()
        (connected2, subprotocol2,) = await second_user_connection.connect()
        assert connected1 and connected2

        dialogue_data = await dialogue_remove_participants_helper(
            dialogue, second_user
        )
        await asyncio.sleep(10)
        message1 = await first_user_connection.receive_json_from(timeout=10)
        message2 = await second_user_connection.receive_json_from(timeout=10)

        assert message1 == dict({"type": "dialogue.removed"}, **dialogue_data)

        assert message2 == dict(
            {"type": "dialogue.self_removed"}, **dialogue_data
        )

        # add
        dialogue_data = await dialogue_add_participants_helper(
            dialogue, second_user
        )
        await asyncio.sleep(10)
        message1 = await first_user_connection.receive_json_from(timeout=10)
        message2 = await second_user_connection.receive_json_from(timeout=10)
        assert message1 == dict({"type": "dialogue.added"}, **dialogue_data)

        assert message2 == dict(
            {"type": "dialogue.self_added"}, **dialogue_data
        )
        # Close
        await first_user_connection.disconnect()
        await second_user_connection.disconnect()
