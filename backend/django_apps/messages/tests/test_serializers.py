from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory
from rest_framework.views import APIView

from django_apps.messages.models import Message, Dialogue
from django_apps.messages.serializers import (
    CreateMessageSerializer,
    CreateDialogueSerializer,
    AddDialogueParticipantsSerializer,
    RemoveDialogueParticipantsSerializer,
    UpdateMessageSerializer,
)
from django_apps.users.tests import AuthorizedTestRequestsMixin


# noinspection PyPep8Naming
class InitSetupMixin(AuthorizedTestRequestsMixin):
    def setUp(self) -> None:
        self.first_user = self.get_new_user()
        self.second_user = self.get_new_user()

        self.default_user1 = self.get_new_user()
        self.default_user2 = self.get_new_user()

        self.first_user.profile.friends.add(
            self.second_user.profile, self.default_user1.profile
        )
        self.second_user.profile.friends.add(
            self.first_user.profile, self.default_user2.profile
        )

        self.default_user1.profile.friends.add(self.first_user.profile)
        self.default_user2.profile.friends.add(self.second_user.profile)

        self.default_user1.save()
        self.default_user2.save()

        self.first_user.save()
        self.second_user.save()

        self.dialogue = Dialogue.objects.create(creator=self.first_user)
        self.dialogue.participants.add(self.first_user, self.second_user)

        self.message = Message.objects.create(
            dialogue=self.dialogue, author=self.first_user, body="test_body",
        )


class DialogueSerializerTest(InitSetupMixin, TestCase):
    def test_serialization(self):
        factory = APIRequestFactory()
        request = factory.post(reverse("messages:dialogue-list"), None)
        # Convert Django's request to DRF
        request = APIView().initialize_request(request)

        # create two pages of messages
        page_size = settings.REST_FRAMEWORK.get("PAGE_SIZE", 10)
        for _ in range(page_size * 2):
            Message.objects.create(
                author=self.first_user,
                dialogue=self.dialogue,
                body="some_body",
            )

        data = CreateDialogueSerializer(
            self.dialogue, context={"request": request}
        ).data

        self.assertEqual(len(data), 4)
        self.assertEqual(data["id"], self.dialogue.id)
        self.assertEqual(
            set(data["participants"]),
            set(self.dialogue.participants.values_list("id", flat=True)),
        )
        # Assert only one page of messages was sent
        self.assertEqual(len(data["messages"]), page_size)
        # Assert descending order
        for i in range(page_size - 1):
            self.assertTrue(
                int(data["messages"][i]["id"])
                > int(data["messages"][i + 1]["id"])
            )
        self.assertEqual(data["creator"], self.first_user.id)

    @patch(
        "django_apps.messages.utils.dispatch_dialogue_participants_added_event"
    )
    def test_deserialization(self, dispatch_consumer_event):
        """
        Ensure deserialization works,
        """
        data = {"participants": [self.second_user.id, self.first_user.id]}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:dialogue-list"), data)
        request = APIView().initialize_request(request)
        request.user = self.second_user

        serializer = CreateDialogueSerializer(
            data=data, context={"request": request}
        )
        self.assertTrue(serializer.is_valid())
        dialogue = serializer.save()

        # assert the event is dispatched
        dispatch_consumer_event.assert_called_with(
            dialogue.id, data["participants"]
        )

        new_data = serializer.data
        self.assertEqual(len(new_data), 4)
        self.assertEqual(new_data["id"], self.dialogue.id + 1)
        self.assertEqual(
            set(new_data["participants"]),
            {self.second_user.id, self.first_user.id},
        )
        self.assertEqual(new_data["messages"], [])
        self.assertEqual(new_data["creator"], self.second_user.id)

    @patch(
        "django_apps.messages.utils.dispatch_dialogue_participants_added_event"
    )
    def test_deserialization_with_missing_data(self, dispatch_consumer_event):
        data = {}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:dialogue-list"), data)
        request.user = self.second_user

        serializer = CreateDialogueSerializer(
            data=data, context={"request": request}
        )
        self.assertFalse(serializer.is_valid())

        # assert the event is not dispatched
        self.assertFalse(dispatch_consumer_event.called)

        self.assertEqual(len(serializer.errors), 1)
        self.assertIn("participants", serializer.errors)

    @patch(
        "django_apps.messages.utils.dispatch_dialogue_participants_added_event"
    )
    def test_deserialization_wrong_data(self, dispatch_consumer_event):
        data = {"participants": [self.default_user1]}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:dialogue-list"), data)
        request.user = self.second_user

        serializer = CreateDialogueSerializer(
            data=data, context={"request": request}
        )
        self.assertFalse(serializer.is_valid())

        # assert the event is not dispatched
        self.assertFalse(dispatch_consumer_event.called)

        self.assertEqual(len(serializer.errors), 1)
        self.assertIn("participants", serializer.errors)


class EditDialogueParticipantsSerializersTest(InitSetupMixin, TestCase):
    @patch(
        "django_apps.messages.utils.dispatch_dialogue_participants_added_event"
    )
    def test_add_deserialization(self, dispatch_consumer_event):
        data = {"participants": [self.default_user1.id]}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:dialogue-list"), data)
        request.user = self.first_user

        serializer = AddDialogueParticipantsSerializer(
            self.dialogue, data=data, context={"request": request}
        )
        self.assertTrue(serializer.is_valid())
        updated_dialogue = serializer.save()

        dispatch_consumer_event.assert_called_with(
            self.dialogue.id, data["participants"]
        )

        self.assertEqual(updated_dialogue.participants.count(), 3)
        self.assertIn(self.second_user, updated_dialogue.participants.all())
        self.assertIn(self.first_user, updated_dialogue.participants.all())
        self.assertIn(self.default_user1, updated_dialogue.participants.all())

        #
        data = {"participants": [self.default_user2.id]}
        request.user = self.second_user
        serializer = AddDialogueParticipantsSerializer(
            self.dialogue, data=data, context={"request": request}
        )
        self.assertTrue(serializer.is_valid())
        updated_dialogue = serializer.save()

        self.assertEqual(updated_dialogue.participants.count(), 4)
        self.assertIn(self.second_user, updated_dialogue.participants.all())
        self.assertIn(self.first_user, updated_dialogue.participants.all())
        self.assertIn(self.default_user1, updated_dialogue.participants.all())
        self.assertIn(self.default_user2, updated_dialogue.participants.all())

    def test_add_deserialization_not_friends(self):
        data = {"participants": [self.default_user2.id]}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:dialogue-list"), data)
        request.user = self.first_user

        serializer = AddDialogueParticipantsSerializer(
            self.dialogue, data=data, context={"request": request}
        )
        self.assertFalse(serializer.is_valid())
        self.assertIn("participants", serializer.errors)

    @patch(
        "django_apps.messages.utils.dispatch_dialogue_participants_removed_event"  # noqa
    )
    def test_remove_deserialization(self, dispatch_consumer_event):
        data = {"participants": [self.second_user.id]}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:dialogue-list"), data)
        request.user = self.first_user

        serializer = RemoveDialogueParticipantsSerializer(
            self.dialogue, data=data, context={"request": request}
        )
        self.assertTrue(serializer.is_valid())
        updated_dialogue = serializer.save()

        dispatch_consumer_event.assert_called_with(
            self.dialogue.id, data["participants"]
        )

        self.assertEqual(updated_dialogue.participants.count(), 1)
        self.assertNotIn(self.second_user, updated_dialogue.participants.all())


class MessageSerializerTest(InitSetupMixin, TestCase):
    def test_serialization(self):
        factory = APIRequestFactory()
        request = factory.post(reverse("messages:message-list"), None)
        data = CreateMessageSerializer(
            self.message, context={"request": request}
        ).data
        data2 = UpdateMessageSerializer(
            self.message, context={"request": request}
        ).data
        self.assertEqual(data, data2)

        self.assertEqual(len(data), 4)
        self.assertEqual(data["id"], self.message.id)
        self.assertEqual(data["dialogue"], self.message.dialogue.id)
        self.assertEqual(data["author"], self.message.author.id)
        self.assertEqual(data["body"], self.message.body)

    @patch("django_apps.messages.utils.dispatch_message_created_event")
    def test_deserialization(self, dispatch_consumer_event):
        data = {"dialogue": self.dialogue.id, "body": "test_body2"}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:message-list"), None)
        request.user = self.first_user
        serializer = CreateMessageSerializer(
            data=data, context={"request": request}
        )
        self.assertTrue(serializer.is_valid())
        new_message = serializer.save()

        # assert the event is dispatched with new_message
        dispatch_consumer_event.assert_called_with(
            new_message, **serializer.data
        )

        new_data = serializer.data
        self.assertEqual(len(new_data), 4)
        self.assertEqual(new_data["id"], self.message.id + 1)
        self.assertEqual(new_data["dialogue"], data["dialogue"])
        self.assertEqual(new_data["author"], self.first_user.id)
        self.assertEqual(new_data["body"], data["body"])

    @patch("django_apps.messages.utils.dispatch_message_edited_event")
    def test_updating(self, dispatch_consumer_event):
        factory = APIRequestFactory()
        request = factory.post(reverse("messages:message-list"), None)
        request.user = self.first_user
        serializer = UpdateMessageSerializer(
            self.message,
            data={"body": "new_body"},
            context={"request": request},
            partial=True,
        )
        self.assertTrue(serializer.is_valid())
        updated_message = serializer.save()

        # assert the event is dispatched with new_message
        dispatch_consumer_event.assert_called_with(
            updated_message, **serializer.data
        )

        new_data = serializer.data
        self.assertEqual(len(new_data), 4)
        self.assertEqual(new_data["id"], self.message.id)
        self.assertEqual(new_data["body"], "new_body")

    def test_deserialization_with_missing_data(self):
        data = {}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:message-list"), None)
        request.user = self.first_user
        serializer = CreateMessageSerializer(
            data=data, context={"request": request}
        )
        self.assertFalse(serializer.is_valid())

        self.assertEqual(len(serializer.errors), 2)
        self.assertIn("dialogue", serializer.errors)
        self.assertIn("body", serializer.errors)

    def test_deserialization_wrong_data(self):
        data = {"dialogue": 999, "body": "test_body2"}

        factory = APIRequestFactory()
        request = factory.post(reverse("messages:message-list"), None)
        request.user = self.first_user
        serializer = CreateMessageSerializer(
            data=data, context={"request": request}
        )
        self.assertFalse(serializer.is_valid())

        self.assertEqual(len(serializer.errors), 1)
        self.assertIn("dialogue", serializer.errors)
