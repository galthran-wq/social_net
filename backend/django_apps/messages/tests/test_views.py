from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from django_apps.messages.models import Dialogue, Message
from django_apps.users.tests import AuthorizedTestRequestsMixin


class InitSetupMixin(AuthorizedTestRequestsMixin):
    # noinspection PyPep8Naming
    def setUp(self) -> None:
        self.first_user = self.get_new_user()
        self.second_user = self.get_new_user()

        self.default_user1 = self.get_new_user()
        self.default_user2 = self.get_new_user()

        self.first_user.profile.friends.add(
            self.second_user.profile, self.default_user1.profile
        )
        self.second_user.profile.friends.add(
            self.first_user.profile, self.default_user2.profile
        )

        self.default_user1.profile.friends.add(self.first_user.profile)
        self.default_user2.profile.friends.add(self.second_user.profile)

        self.default_user1.save()
        self.default_user2.save()

        self.first_user.save()
        self.second_user.save()

        self.dialogue = Dialogue.objects.create(creator=self.first_user)
        self.dialogue.participants.add(self.first_user, self.second_user)
        self.dialogue.save()

        self.message = Message.objects.create(
            dialogue=self.dialogue, author=self.first_user, body="test_body",
        )


class DialogueViewSetSuccessfulTest(InitSetupMixin, APITestCase):
    def test_list(self):
        response = self._authorized_get(
            reverse("messages:dialogue-list"), user=self.first_user
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # user without dialogue request
        response = self._authorized_get(
            reverse("messages:dialogue-list"), user=self.default_user1
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)

    def test_detail(self):
        response = self._authorized_get(
            reverse(
                "messages:dialogue-detail",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], self.dialogue.id)

    def test_create(self):
        response = self._authorized_post(
            reverse("messages:dialogue-list"),
            {"participants": [self.first_user.id, self.second_user.id, ]},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["id"], self.dialogue.id + 1)
        # assert actually created
        self.assertTrue(Dialogue.objects.get(pk=self.dialogue.id + 1))

    def test_edit_users(self):
        # test add
        response = self._authorized_post(
            reverse(
                "messages:dialogue-add-participants",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.default_user1.id]},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["participants"]), 3)
        self.assertIn(self.default_user1.id, response.data["participants"])
        self.assertIn(self.first_user.id, response.data["participants"])
        self.assertIn(self.second_user.id, response.data["participants"])

        response = self._authorized_post(
            reverse(
                "messages:dialogue-add-participants",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.default_user2.id]},
            user=self.second_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["participants"]), 4)
        self.assertIn(self.default_user2.id, response.data["participants"])
        self.assertIn(self.default_user1.id, response.data["participants"])
        self.assertIn(self.first_user.id, response.data["participants"])
        self.assertIn(self.second_user.id, response.data["participants"])

        # test remove
        response = self._authorized_post(
            reverse(
                "messages:dialogue-remove-participants",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.default_user2.id]},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["participants"]), 3)
        self.assertIn(self.default_user1.id, response.data["participants"])
        self.assertIn(self.first_user.id, response.data["participants"])
        self.assertIn(self.second_user.id, response.data["participants"])

        response = self._authorized_post(
            reverse(
                "messages:dialogue-remove-participants",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.default_user1.id]},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["participants"]), 2)
        self.assertIn(self.first_user.id, response.data["participants"])
        self.assertIn(self.second_user.id, response.data["participants"])

        response = self._authorized_post(
            reverse(
                "messages:dialogue-remove-participants",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.second_user.id]},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["participants"]), 1)
        self.assertIn(self.first_user.id, response.data["participants"])

    def test_leave(self):
        response = self._authorized_post(
            reverse(
                "messages:dialogue-leave",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            None,
            user=self.second_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotIn(self.second_user.id, response.data["participants"])

    def test_delete(self):
        """Noone can delete dialogue"""
        for user in [self.first_user, self.second_user]:
            response = self._authorized_delete(
                reverse(
                    "messages:dialogue-detail",
                    kwargs={"dialogue_id": self.dialogue.id},
                ),
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class DialogueViewSetUnsuccessfulTest(InitSetupMixin, APITestCase):
    def test_detail_not_found(self):
        response = self._authorized_get(
            reverse("messages:dialogue-detail", kwargs={"dialogue_id": 999},),
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_add_users_not_friends(self):
        response = self._authorized_post(
            reverse(
                "messages:dialogue-add-participants",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.default_user2.id]},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("participants", response.data)

        response = self._authorized_post(
            reverse(
                "messages:dialogue-add-participants",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.default_user1.id]},
            user=self.second_user,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("participants", response.data)

    def test_remove_users_not_creator(self):
        response = self._authorized_post(
            reverse(
                "messages:dialogue-remove-participants",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.default_user2.id]},
            user=self.second_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_leave_with_creator(self):
        response = self._authorized_post(
            reverse(
                "messages:dialogue-leave",
                kwargs={"dialogue_id": self.dialogue.id},
            ),
            {"participants": [self.default_user2.id]},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class MessagesViewSetSuccessfulTest(InitSetupMixin, APITestCase):
    def test_list(self):
        # assert users can fetch messages from the dialogue
        for user in [self.first_user, self.second_user]:
            response = self._authorized_get(
                reverse("messages:message-list"), user=user
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(len(response.data["results"]), 1)
            self.assertEqual(response.data["results"][0]["body"], "test_body")

        # assert noone else sees them
        response = self._authorized_get(
            reverse("messages:message-list"), user=self.default_user1
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)

    def test_detail(self):
        # assert users from dialogue can see message detail
        for user in [self.first_user, self.second_user]:
            response = self._authorized_get(
                reverse(
                    "messages:message-detail",
                    kwargs={"message_id": self.message.id},
                ),
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data["id"], self.message.id)

    def test_create(self):
        for user in [self.first_user, self.second_user]:
            response = self._authorized_post(
                reverse("messages:message-list"),
                {"dialogue": self.dialogue.id, "body": "test_body2"},
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(response.data["body"], "test_body2")
        self.assertEqual(Message.objects.count(), 3)

    def test_update(self):
        """Only author can update messages"""
        response = self._authorized_patch(
            reverse(
                "messages:message-detail",
                kwargs={"message_id": self.message.id},
            ),
            {"body": "test_body22"},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["body"], "test_body22")

    def test_delete(self):
        response = self._authorized_delete(
            reverse(
                "messages:message-detail",
                kwargs={"message_id": self.message.id},
            ),
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_ignore(self):
        """
        Tests if user can ignore message,
        if it affects anyone else's message list,
        and if user can ignore self messages.
        """
        for user in [self.first_user, self.second_user]:
            list_response = self._authorized_get(
                reverse("messages:message-list"), user=user
            )
            self.assertEqual(list_response.status_code, status.HTTP_200_OK)
            self.assertEqual(list_response.data["count"], 1)
            self.assertEqual(
                list_response.data["results"][0]["id"], self.message.id,
            )

            response = self._authorized_post(
                reverse(
                    "messages:message-ignore",
                    kwargs={"message_id": self.message.id},
                ),
                None,
                user=user,
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            # assert doesn't see anymore
            list_response = self._authorized_get(
                reverse("messages:message-list"), user=user
            )
            self.assertEqual(list_response.status_code, status.HTTP_200_OK)
            self.assertEqual(list_response.data["count"], 0)


class MessagesViewSetUnsuccessfulTest(InitSetupMixin, APITestCase):
    def test_detail_not_found(self):
        response = self._authorized_get(
            reverse("messages:message-detail", kwargs={"message_id": 999}),
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_with_wrong_data(self):
        response1 = self._authorized_post(
            reverse("messages:message-list"),
            {"dialogue": 999, "body": "test_body2"},
            user=self.second_user,
        )
        response2 = self._authorized_post(
            reverse("messages:message-list"),
            {"dialogue": self.dialogue.id, "body": "test_body2"},
            user=self.default_user1,
        )
        self.assertEqual(response1.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response2.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_with_wrong_data(self):
        response = self._authorized_patch(
            reverse(
                "messages:message-detail",
                kwargs={"message_id": self.message.id},
            ),
            {"dialogue": 999, "author": 999, "body": "test_body22"},
            user=self.first_user,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["body"], "test_body22")
        self.assertNotEqual(response.data["dialogue"], 999)
        self.assertNotEqual(response.data["author"], 999)

    def test_delete_not_author(self):
        response = self._authorized_delete(
            reverse(
                "messages:message-detail",
                kwargs={"message_id": self.message.id},
            ),
            user=self.second_user,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
