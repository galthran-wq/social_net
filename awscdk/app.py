#!/usr/bin/env python3
import os
from awscdk.root import ApplicationStack

from aws_cdk import core


env_variables = {
    "DEBUG": os.environ.get(
        "DEBUG"
    ),
    "DJANGO_ALLOWED_HOSTS": os.environ.get(
        "DJANGO_ALLOWED_HOSTS"
    ),
    "SQL_ENGINE": os.environ.get(
        "SQL_ENGINE"
    ),
    "SQL_DATABASE": os.environ.get(
        "SQL_DATABASE"
    ),
    "SQL_USER": os.environ.get(
        "SQL_USER"
    ),
    "SQL_PASSWORD": os.environ.get(
        "SQL_PASSWORD",
    ),
    "SECRET_KEY": os.environ.get(
        "SECRET_KEY",
    ),
    "AWS_REGION": os.environ.get(
        "AWS_REGION", "us-west-1"
    ),
    "ENVIRONMENT_NAME": os.environ.get(
        "ENVIRONMENT", "dev"
    ),
    "BASE_DOMAIN_NAME": os.environ.get(
        "DOMAIN_NAME"
    ),
    "BASE_APP_NAME": os.environ.get(
        "APP_NAME"
    )
}

env_variables["FULL_DOMAIN_NAME"] = env_variables["BASE_DOMAIN_NAME"]

if env_variables["ENVIRONMENT_NAME"] != "prod":
    # prepend environment name to full domain name
    env_variables["FULL_DOMAIN_NAME"] = (
        env_variables["ENVIRONMENT_NAME"] +
        "." +
        env_variables["FULL_DOMAIN_NAME"]
    )

full_app_name = (
    env_variables["ENVIRONMENT_NAME"] +
    "-" +
    env_variables["BASE_APP_NAME"]
)

app = core.App()

ApplicationStack(
    app,
    f"{full_app_name}-stack",
    env_variables=env_variables,
    full_app_name=full_app_name,
)

app.synth()
