from aws_cdk import core
from aws_cdk import aws_route53 as route53


class HostedZone(core.Construct):
    def __init__(
            self,
            scope,
            id,
    ):
        super(HostedZone, self).__init__(scope, id)

        self.hosted_zone = route53.HostedZone(
            self,
            "hosted_zone",
            zone_name=scope.env_variables.get(
                "BASE_DOMAIN_NAME"
            ),
        )
