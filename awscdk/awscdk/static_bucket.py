from aws_cdk import core, aws_s3, aws_iam


class StaticBucket(core.Construct):
    def __init__(
            self,
            scope,
            id,
    ):
        super(StaticBucket, self).__init__(scope, id)
        self.static_bucket = aws_s3.Bucket(
            self,
            "static_bucket",
            access_control=aws_s3.BucketAccessControl.PUBLIC_READ,
            bucket_name=f"{scope.full_app_name}-static",
            public_read_access=True
        )

        # grant access to static files for everyone
        self.policy_statement = aws_iam.PolicyStatement(
            actions=["s3:GetObject"],
            resources=[f"{self.static_bucket.bucket_arn}/static/*"],
        )

        self.policy_statement.add_any_principal()

        self.static_site_policy_document = aws_iam.PolicyDocument(
            statements=[self.policy_statement]
        )

        self.static_bucket.add_to_resource_policy(self.policy_statement)

