from aws_cdk import core, aws_elasticache, aws_cloudformation, aws_ec2


class ElasticacheStack(aws_cloudformation.NestedStack):
    def __init__(
            self,
            scope,
            id
    ):
        super(ElasticacheStack, self).__init__(scope, id)

        self.elasticache_security_group = aws_ec2.CfnSecurityGroup(
            self,
            "elasticache-security-group",
            vpc_id=scope.vpc.vpc_id,
            group_description="ElastiCacheSecurityGroup",
            security_group_ingress=[
                aws_ec2.CfnSecurityGroup.IngressProperty(
                    ip_protocol="tcp",
                    to_port=6379,
                    from_port=6379,
                    source_security_group_id=scope.vpc.vpc_default_security_group,
                )
            ],
        )

        self.elasticache_subnet_group = aws_elasticache.CfnSubnetGroup(
            self,
            "elasticache-subnet-group",
            subnet_ids=scope.vpc.select_subnets(
                subnet_type=aws_ec2.SubnetType.ISOLATED
            ).subnet_ids,
            description="The subnet group for ElastiCache",
        )

        self.cluster = aws_elasticache.CfnCacheCluster(
            self,
            "cache-cluster",
            cache_node_type="cache.t3.micro",
            engine="redis",
            num_cache_nodes=1,
            vpc_security_group_ids=[
                self.elasticache_security_group.get_att("GroupId").to_string()
            ],
            cache_subnet_group_name=self.elasticache_subnet_group.ref
        )

        scope.env_variables.update({
            "REDIS_HOST": self.cluster.attr_redis_endpoint_address,
            "REDIS_PORT": self.cluster.attr_redis_endpoint_port
        })
