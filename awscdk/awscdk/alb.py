from aws_cdk import aws_elasticloadbalancingv2 as aws_alb, aws_ec2 as ec2
from aws_cdk import core


HTTP_PORT = 80
HTTPS_PORT = 443


class ApplicationLoadBalancerStack(core.NestedStack):
    def __init__(
            self,
            scope,
            id
    ):
        super(ApplicationLoadBalancerStack, self).__init__(scope, id)
        self.alb = aws_alb.ApplicationLoadBalancer(
            scope=scope,
            id="alb",
            vpc=scope.vpc,
            internet_facing=True,
        )

        # allow HTTP/HTTPS (80/443) traffic
        self.alb.connections.allow_from_any_ipv4(
            port_range=ec2.Port.tcp(HTTP_PORT),
            description="HTTP outbound traffic rule"
        )

        self.alb.connections.allow_from_any_ipv4(
            port_range=ec2.Port.tcp(HTTPS_PORT),
            description="HTTPS inbound traffic rule"
        )

        # Inner communication relies on HTTP
        self.alb.connections.allow_to_any_ipv4(
            port_range=ec2.Port.tcp(HTTP_PORT),
            description="HTTP outbound traffic rule"
        )

        # add two listeners (HTTP/HTTPS)
        self.http_listener = self.alb.add_listener(
            id="http-listener",
            port=HTTP_PORT,
            protocol=aws_alb.ApplicationProtocol.HTTP,
            open=True,
            default_target_groups=None
        )

        self.https_listener = self.alb.add_listener(
            id="https_listener",
            certificates=[scope.certificate],
            port=HTTPS_PORT,
            protocol=aws_alb.ApplicationProtocol.HTTPS,
            open=True,
            default_target_groups=None
        )

        # create default target group with IP target type
        self.default_target_group = aws_alb.ApplicationTargetGroup(
            self,
            "default-target-group",
            target_type=aws_alb.TargetType.IP,
            vpc=scope.vpc,
            port=HTTP_PORT,
            protocol=aws_alb.ApplicationProtocol.HTTP
        )

        # register target group on both listeners
        self.http_listener.add_target_groups(
            "default-target-group",
            target_groups=[self.default_target_group]
        )

        self.https_listener.add_target_groups(
            "https_default_target_group",
            target_groups=[self.default_target_group]
        )

