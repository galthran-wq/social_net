from aws_cdk import core, aws_ecs as ecs, aws_cloudformation as cloudformation


class BackendTasksStack(cloudformation.NestedStack):
    def __init__(self, scope: core.Construct, id: str, **kwargs,) -> None:
        super().__init__(
            scope, id, **kwargs,
        )

        # migrate
        self.migrate_task = ecs.FargateTaskDefinition(
            self, "migrate-task", family=f"{scope.full_app_name}-migrate"
        )

        # todo grant access to secret variables
        # ...

        self.migrate_task.add_container(
            "migrate-command",
            image=scope.image,
            environment=scope.env_variables,
            # secrets=scope.secrets,
            command=["python3", "manage.py", "migrate", "--no-input"],
            logging=ecs.LogDrivers.aws_logs(stream_prefix="MigrateCommand"),
        )
