from alb import ApplicationLoadBalancerStack
from aws_cdk import core, aws_ecr_assets, aws_ecs
from backend_services import BackendServicesStack
from backend_tasks import BackendTasksStack
from certificate import Certificate
from cloudfront import CloudfrontStack
from ecs import EcsStack
from elasticache import ElasticacheStack
from hosted_zone import HostedZone
from rds import RdsStack
from static_bucket import StaticBucket
from vpc import VPCStack


class ApplicationStack(core.Stack):
    def __init__(
            self,
            scope: core.Construct,
            id: str,
            env_variables,
            full_app_name: str,
            **kwargs
    ) -> None:
        super(ApplicationStack, self).__init__(scope, id, **kwargs)

        self.env_variables = env_variables
        self.full_app_name = full_app_name

        # here goes all nested stacks definitions
        # Hosted zone
        self.hosted_zone = HostedZone(
            self,
            "hosted-zone"
        ).hosted_zone

        # TLS certificate for self.full_domain_name
        self.certificate = Certificate(
            self,
            "certificate"
        )

        # s3 bucket for static/media stack
        self.static_bucket = StaticBucket(
            self,
            "static-bucket"
        ).static_bucket

        # vpc
        self.vpc_stack = VPCStack(
            self,
            "vpc-stack"
        )
        self.vpc = self.vpc_stack.vpc

        # Application load balancer with 2 listeners
        # without any target groups (so far)
        self.alb_stack = ApplicationLoadBalancerStack(
            self,
            "alb-stack"
        )
        self.alb = self.alb_stack.alb

        # CloudFront
        # Note: account must be verified
        #       in order to use cloudfront
        # self.cloudfront = CloudfrontStack(
        #     self,
        #     f"{full_app_name}-cloudfront"
        # )

        # Aurora PostgreSQL RDS cluster
        self.rds_stack = RdsStack(
            self,
            "rds-stack"
        )

        # Redis ElastiCache
        self.elasticache_stack = ElasticacheStack(
            self,
            "elasticache-stack"
        )

        # ecs cluster
        self.ecs = EcsStack(
            self,
            "ecs-stack"
        )
        self.ecs_cluster = self.ecs.cluster

        # Backend image that is used for all backend services
        # (api (w/ gunicorn), websocket (w/ daphne),
        # celery, and celery-beat
        self.image = aws_ecs.AssetImage(
            "../backend",
            file="scripts/prod/Dockerfile"
        )

        # Secrets todo
        # self.secrets = None

        # backend ecs service
        self.backend_services_stack = BackendServicesStack(
            self,
            "backend-services"
        )
        self.api_service = self.backend_services_stack.api_service

        # backend tasks
        self.backend_tasks = BackendTasksStack(
            self,
            "backend-tasks"
        )

        # celery ecs services todo
        # self.celery_services = None

