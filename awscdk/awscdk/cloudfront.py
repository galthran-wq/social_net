from aws_cdk import core, aws_route53, aws_route53_targets
from aws_cdk import aws_cloudfront


MATCH_VIEWER = aws_cloudfront.OriginProtocolPolicy.MATCH_VIEWER
FORWARD_ALL = aws_cloudfront.CfnDistribution.ForwardedValuesProperty(
    cookies={
        "forward": "all"
    },
    headers=["*"],
    query_string=True
)
FORWARD_QUERY_STRING = aws_cloudfront.CfnDistribution.ForwardedValuesProperty(
    query_string=True
)


class CloudfrontStack(core.NestedStack):
    def __init__(
        self,
        scope,
        id,
        **kwargs
    ):
        super(CloudfrontStack, self).__init__(scope, id, **kwargs)

        alb = scope.alb
        static_bucket = scope.static_bucket

        self.distribution = aws_cloudfront.CloudFrontWebDistribution(
            self,
            "cloudfront-distribution",
            origin_configs=[
                aws_cloudfront.SourceConfiguration(
                    custom_origin_source=aws_cloudfront.CustomOriginConfig(
                        domain_name=alb.load_balancer_dns_name,
                        origin_protocol_policy=MATCH_VIEWER,
                    ),
                    behaviors=[
                        # redirect corresponding responses to
                        # api and websocket services
                        aws_cloudfront.Behavior(
                            allowed_methods=aws_cloudfront.CloudFrontAllowedMethods.ALL,
                            forwarded_values=FORWARD_ALL,
                            path_pattern=path
                        ) for path in ["/api/*", "/ws/*"]
                    ]
                ),
                aws_cloudfront.SourceConfiguration(
                    s3_origin_source=aws_cloudfront.S3OriginConfig(
                        s3_bucket_source=static_bucket,
                    ),
                    behaviors=[
                        aws_cloudfront.Behavior(
                            forwarded_values=FORWARD_QUERY_STRING,
                            max_ttl=core.Duration.seconds(0),
                            min_ttl=core.Duration.seconds(0),
                            path_pattern=path
                        ) for path in ["/static/*", "/media/*"]
                    ] + [
                        # todo default behavior for now
                        aws_cloudfront.Behavior(
                            forwarded_values=FORWARD_QUERY_STRING,
                            is_default_behavior=True
                        )
                    ],
                )
            ],
            # todo parameter is deprecated
            alias_configuration=aws_cloudfront.AliasConfiguration(
                acm_cert_ref=scope.certificate.certificate_arn,
                names=[scope.env_variables["FULL_DOMAIN_NAME"]],
            ),
        )

        aws_route53.ARecord(
            self,
            "alias-record",
            target=aws_route53.AddressRecordTarget.from_alias(
                aws_route53_targets.CloudFrontTarget(self.distribution)  # noqa
            ),
            zone=scope.hosted_zone,
            record_name=f"{scope.env_variables['FULL_DOMAIN_NAME']}.",
        )

