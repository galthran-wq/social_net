from aws_cdk import (
    core,
    aws_ec2 as ec2,
    aws_rds as rds
)


class RdsStack(core.Construct):
    def __init__(self, scope: core.Construct, id: str) -> None:
        super().__init__(scope, id)

        # secrets manager for DB password
        # self.db_secret = secrets.Secret(
        #     self,
        #     "DBSecret",
        #     secret_name=f"{scope.full_app_name}-db-secret",
        #     generate_secret_string=secrets.SecretStringGenerator(
        #         secret_string_template=json.dumps({"username": "postgres"}),
        #         exclude_punctuation=True,
        #         include_space=False,
        #         generate_string_key="password",
        #     ),
        # )

        # self.db_secret_arn = ssm.StringParameter(
        #     self,
        #     "DBSecretArn",
        #     parameter_name=f"{scope.full_app_name}-secret-arn",
        #     string_value=self.db_secret.secret_arn,
        # )

        self.db_security_group = ec2.CfnSecurityGroup(
            self,
            "db-security-group",
            vpc_id=scope.vpc.vpc_id,
            group_description="DBSecurityGroup",
            security_group_ingress=[
                ec2.CfnSecurityGroup.IngressProperty(
                    ip_protocol="tcp",
                    to_port=5432,
                    from_port=5432,
                    source_security_group_id=scope.vpc.vpc_default_security_group,  # noqa
                )
            ],
        )

        self.db_subnet_group = rds.CfnDBSubnetGroup(
            self,
            "db-subnet-group",
            subnet_ids=scope.vpc.select_subnets(
                subnet_type=ec2.SubnetType.ISOLATED
            ).subnet_ids,
            db_subnet_group_description=f"{scope.full_app_name}-db-subnet-group",  # noqa
        )

        # AURORA SERVERLESS POSTGRES CLUSTER
        self.db_config = {
            "engine": "aurora-postgresql",
            "engine_mode": "serverless",
            "engine_version": "10.7",
            "enable_http_endpoint": True,
            "vpc_security_group_ids": [
                self.db_security_group.get_att("GroupId").to_string()
            ],
            "db_subnet_group_name": self.db_subnet_group.ref,
            "master_username": scope.env_variables.get(
                "SQL_USER"
            ),
            "master_user_password": scope.env_variables.get(
                "SQL_PASSWORD"
            ),
            "database_name": scope.env_variables.get(
                "SQL_DATABASE"
            ),
        }

        self.cluster = rds.CfnDBCluster(
            self,
            "db-cluster",
            **self.db_config
        )

        scope.env_variables.update({
            "SQL_HOST": self.cluster.get_att(
                "Endpoint.Address"
            ).to_string(),
            "SQL_PORT": self.cluster.get_att(
                "Endpoint.Port"
            ).to_string(),
        })

        # AURORA PROVISIONED POSTGRESQL CLUSTER
        # self.db_cluster_parameter_group = rds.ParameterGroup.from_parameter_group_name(
        #     self,
        #     'db-cluster-parameter-group',
        #     'default.aurora-postgresql10'
        # ),
        #
        # self.db_config = {
        #     "engine": rds.DatabaseClusterEngine.aurora_postgres(
        #         version=rds.AuroraPostgresEngineVersion.VER_11_7
        #     ),
        #     "instance_props": rds.InstanceProps(
        #         vpc=scope.vpc,
        #         vpc_subnets=ec2.SubnetSelection(
        #             subnet_group_name=self.db_subnet_group.db_subnet_group_name
        #         ),
        #         security_groups=[self.db_security_group]
        #     ),
        #     "master_user": rds.Login(
        #         username=scope.env_variables.get(
        #             "SQL_USER"
        #         ),
        #         password=core.SecretValue(
        #             scope.env_variables.get(
        #                 "SQL_PASSWORD"
        #             )
        #         ),
        #     ),
        # }
        #
        # self.cluster = rds.DatabaseCluster(
        #     self,
        #     "DBCluster",
        #     **self.db_config
        # )
