from aws_cdk import aws_certificatemanager as aws_acm


class Certificate(aws_acm.Certificate):
    def __init__(self, scope, id, **kwargs):
        super().__init__(
            scope,
            id,
            domain_name=scope.env_variables['FULL_DOMAIN_NAME'],
            validation=aws_acm.CertificateValidation.from_email(),
            validation_method=aws_acm.ValidationMethod.EMAIL,
            **kwargs
        )
