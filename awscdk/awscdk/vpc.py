from aws_cdk import core
from aws_cdk import aws_ec2 as ec2


class VPCStack(core.NestedStack):
    def __init__(self, scope, id, **kwargs):
        super().__init__(scope, id, **kwargs)

        self.vpc = ec2.Vpc(
            self,
            id,
            cidr="10.0.0.0/16",
            nat_gateways=0,
            max_azs=2,
            subnet_configuration=[
                ec2.SubnetConfiguration(
                    cidr_mask=24,
                    name="public-subnet",
                    subnet_type=ec2.SubnetType.PUBLIC
                ),
                ec2.SubnetConfiguration(
                    cidr_mask=24,
                    name="isolated-subnet",
                    subnet_type=ec2.SubnetType.ISOLATED
                )
            ]
        )

