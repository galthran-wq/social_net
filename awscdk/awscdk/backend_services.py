from aws_cdk import aws_cloudformation, aws_ec2, aws_ecs, aws_logs


class BackendServicesStack(aws_cloudformation.NestedStack):
    def __init__(
            self,
            scope,
            id
    ):
        super(BackendServicesStack, self).__init__(scope, id)

        self.api_task = aws_ecs.FargateTaskDefinition(
            self,
            "api-task",
        )

        self.api_task.add_container(
            id="api-container",
            image=scope.image,
            command=["/start_prod.sh"],
            logging=aws_ecs.AwsLogDriver(
                stream_prefix="api-container",
                log_retention=aws_logs.RetentionDays.ONE_WEEK,
            ),
            environment=scope.env_variables,
            # secrets=scope.variables.secret_variables
        )

        scope.static_bucket.grant_read_write(
            self.api_task.task_role
        )

        # todo grant secret access
        # ...

        port_mapping = aws_ecs.PortMapping(
            container_port=8000, protocol=aws_ecs.Protocol.TCP
        )
        self.api_task.default_container.add_port_mappings(port_mapping)

        self.api_service = aws_ecs.FargateService(
            self,
            "api-service",
            cluster=scope.ecs_cluster,
            task_definition=self.api_task,
            security_group=aws_ec2.SecurityGroup.from_security_group_id(
                self,
                "api-service-security-group",
                security_group_id=scope.vpc.vpc_default_security_group,
            ),
            assign_public_ip=True,
        )

        # todo substitute for https listener only,
        #  bcs http will redirect to https
        scope.alb_stack.http_listener.add_targets(
            "api-target",
            port=80,
            targets=[self.api_service],
            priority=2,
            path_patterns=["*"],
            # health_check=aws.HealthCheck(
            #     healthy_http_codes="200-299", path="/api/health-check/",
            # ),
        )

        # todo websocket service
