import os

from aws_cdk import core, aws_secretsmanager, aws_ecs, aws_cloudformation


class Secrets(core.NestedStack):
    def __init__(self, scope, id):
        super().__init__(scope, id)

        # self.django_secret_key = aws_secretsmanager.Secret(
        #     self,
        #     "DjangoSecretKey",
        #     generate_secret_string=aws_secretsmanager.SecretStringGenerator(
        #         exclude_punctuation=True, include_space=False,
        #     ),
        # )

        # self.secret_variables = {
        #     "DJANGO_SECRET_KEY": aws_ecs.Secret.from_secrets_manager(
        #         self.django_secret_key
        #     ),
        # }

