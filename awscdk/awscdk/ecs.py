from aws_cdk import aws_cloudformation, aws_ecs


class EcsStack(aws_cloudformation.NestedStack):
    def __init__(
            self,
            scope,
            id
    ):
        super(EcsStack, self).__init__(scope, id)
        self.cluster = aws_ecs.Cluster(
            self,
            "ecs-cluster",
            vpc=scope.vpc
        )
