import {SET_MY_PERMISSIONS, SET_PERMISSIONS} from "@/store/mutation-types";
import {getMyPermissions, getPermissionList} from "@/api/roles";

export default {
  state() {
    return {
      // todo currentRoles: [],
      permissions: {},
      myPermissions: {
        nonTarget: {},
        target: {},
      },
      targetPermissions: {},
      nonTargetPermissions: {},
    }
  },
  mutations: {
    [SET_PERMISSIONS](state, permissions) {
      state.permissions = permissions
      state.targetPermissions = permissions.target
      state.nonTargetPermissions = permissions.non_target
    },
    [SET_MY_PERMISSIONS](state, myPermissions) {
      myPermissions.forEach((permission) => {
        if (permission.target) {
          if (!state.myPermissions.target[permission.name])
            state.myPermissions.target[permission.name] = []
          state.myPermissions.target[permission.name].push(permission.target)
        } else
          state.myPermissions.nonTarget[permission.name] = true
      })
    },
  },
  getters: {
    getRoleRepresentationName() {
      return roleName => {
        return roleName.slice(
          roleName.indexOf("_") + 1,
          roleName.length - 5
        )
      }
    },
    getRoleInnerName(state, getters) {
      return roleInnerName => `${getters.currentGroupId}_${roleInnerName}_role`
    },
    permissions(state) {
      return state.permissions
    },
    myPermissions(state) {
      return state.myPermissions
    },
    hasPermission(state, _, __, rootState) {
      return (name, target=null) => {
        let isOwner = false
        if (rootState.currentUser)
          isOwner = rootState.currentUserId === rootState.currentGroupOwner.id

        let found
        if (target) {
          if (state.myPermissions.target[name])
            found = state.myPermissions.target[name].includes(target)
        } else {
          found = state.myPermissions.nonTarget[name]
        }

        return found || isOwner
      }
    },
    targetPermissions(state) {
      return state.targetPermissions
    },
    nonTargetPermissions(state) {
      return state.nonTargetPermissions
    },
  },
  actions: {
    fetchPermissions(context) {
      return getPermissionList()
        .then(response => {
          context.commit(SET_PERMISSIONS, response.data)
        })
    },
    fetchMyPermissions(context) {
      return getMyPermissions(context.getters.currentGroupId)
        .then(response => {
          context.commit(SET_MY_PERMISSIONS, response.data)
        })
    }
  }
}