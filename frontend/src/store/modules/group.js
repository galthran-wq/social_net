import {
  SET_CURRENT_GROUP,
} from "@/store/mutation-types";
import {
  cancelFollowGroupRequest as cancelFollowGroupRequestAPI,
  fetchGroup,
  followGroup as followGroupAPI,
  leaveGroup as leaveGroupAPI,
  requestFollowGroup as requestFollowGroupAPI
} from "@/api/groups";
import roles from "@/store/modules/roles";

export default {
  modules: {
    roles
  },
  state() {
    return {
      currentGroup: {},
    }
  },
  mutations: {
    [SET_CURRENT_GROUP](state, group) {
      // todo, a way to set group correctly
      state.currentGroup = {
        id: group.id,
        owner: group.owner,
        followQueue: group.follow_queue,
        followers: {
          count: group.followers.count,
          entries: group.followers.results
        },
        isFollowing: group.is_following,
        isFollowRequested: group.is_follow_requested
      }
      // state.currentGroup = group
    },
  },
  actions: {
    fetchCurrentGroup(context, groupId) {
      return fetchGroup(groupId)
        .then(response => {
          context.commit(SET_CURRENT_GROUP, response.data)
          return response
        })
    },
    leaveGroup(context) {
      return leaveGroupAPI(context.getters.currentGroupId)
        .then(response => {
          context.commit(SET_CURRENT_GROUP, response.data)
          return response
        })
    },
    followGroup(context) {
      return followGroupAPI(context.getters.currentGroupId)
        .then(response => {
          context.commit(SET_CURRENT_GROUP, response.data)
          return response
        })
    },
    requestFollowGroup(context) {
      return requestFollowGroupAPI(context.getters.currentGroupId)
        .then(response => {
          context.commit(SET_CURRENT_GROUP, response.data)
          return response
        })
    },
    cancelFollowGroupRequest(context) {
      return cancelFollowGroupRequestAPI(context.getters.currentGroupId)
        .then(response => {
          context.commit(SET_CURRENT_GROUP, response.data)
          return response
        })
    }
  },
  getters: {
    followersCount(state) {
      if (state.currentGroup.followers)
        return state.currentGroup.followers.count
    },
    isFollowing(state) {
      return state.currentGroup.isFollowing
    },
    isFollowRequested(state) {
      return state.currentGroup.isFollowRequested
    },
    followersPage(state) {
      if (state.currentGroup.followers)
        return state.currentGroup.followers.entries
    },
    currentGroupId(state) {
      return state.currentGroup.id
    },
    currentGroupOwner(state) {
      return state.currentGroup.owner
    }
  }
}
