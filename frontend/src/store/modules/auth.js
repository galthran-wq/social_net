import {RESET_ACCESS, RESET_REFRESH, SET_ACCESS, SET_REFRESH} from "@/store/mutation-types";
import jwtDecode from "jwt-decode";
import {login as loginAPI, logout as logoutAPI, refreshAccess as refreshAccessAPI, signup as signupAPI} from "@/api/auth";

export default {
  state() {
    return {
      refresh: localStorage.getItem("refresh"),
      access: localStorage.getItem("access"),
    }
  },
  mutations: {
    [SET_ACCESS](state, access) {
      localStorage.setItem("access", access)
      state.access = access
    },
    [RESET_ACCESS](state) {
      localStorage.removeItem("access")
      state.access = null
    },
    [SET_REFRESH](state, refresh) {
      localStorage.setItem("refresh", refresh)
      state.refresh = refresh
    },
    [RESET_REFRESH](state) {
      localStorage.removeItem("refresh")
      state.refresh = null
    },
  },
  actions: {
    login(context, credentials) {
      return loginAPI(credentials)
        .then(response => {
          context.commit(SET_ACCESS, response.data.access)
          context.commit(SET_REFRESH, response.data.refresh)
          return response
        })
    },
    signup(context, credentials) {
      return signupAPI(credentials)
        .then(response => {
          context.commit(SET_ACCESS, response.data.access)
          context.commit(SET_REFRESH, response.data.refresh)
          return response.data
        })
    },
    logout(context) {
      return logoutAPI(context.getters.refresh)
        .then(response => {
          context.commit(RESET_ACCESS)
          context.commit(RESET_REFRESH)
          return response
        })
    },
    refreshAccess(context) {
      context.commit(RESET_ACCESS)
      return refreshAccessAPI(context.getters.refresh)
        .then(response => {
          context.commit(SET_ACCESS, response.data.access)
          return response
        })
    },
  },
  getters: {
    currentUser(state) {
      return state.access && jwtDecode(state.access)
    },
    currentUserId(state, getters) {
      return getters.currentUser.user_id
    },
    access(state) {
      return state.access
    },
    refresh(state) {
      return state.refresh
    }
  }
}