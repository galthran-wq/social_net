import {SET_PROFILE} from "@/store/mutation-types";

export default {
  state() {
    return {
      profile: {},
    }
  },
  mutations: {
    [SET_PROFILE](state, profile) {
      state.profile = profile
    },
  },
  actions: {
    fetchProfile(context) {
      return this._vm.$http.get(
        'auth/profile/'
      ).then(response => {
        context.commit(SET_PROFILE, response.data)
      })
    },
  },
  getters: {
    profile(state) {
      return state.profile
    },
  }
}