import axios from "axios"

export function fetchGroups(queryParams) {
  return axios.get(
    '/groups/',
    {params: queryParams}
  )
}

export function fetchGroup(id, queryParams) {
  return axios.get(
    `/groups/${id}/`,
    {params: queryParams}
  )
}

export function followGroup(groupId, queryParams) {
  return axios.post(
    `/groups/${groupId}/follow/`,
    {},
    {params: queryParams}
  )
}

export function requestFollowGroup(groupId, queryParams) {
  return axios.post(
    `/groups/${groupId}/request_follow/`,
    {},
    {params: queryParams}
  )
}


export function cancelFollowGroupRequest(groupId, queryParams) {
  return axios.post(
    `/groups/${groupId}/cancel_follow_request/`,
    {},
    {params: queryParams}
  )
}


export function leaveGroup(groupId, queryParams) {
  return axios.post(
    `/groups/${groupId}/leave/`,
    {},
    {params: queryParams}
  )
}

export function createGroup(data, queryParams) {
  return axios.post(
    '/groups/',
    data,
    {params: queryParams}
  )
}

export function deleteGroup(groupId, queryParams) {
  return axios.delete(
    `/groups/${groupId}/`,
    {params: queryParams}
  )
}
