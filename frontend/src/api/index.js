import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from '@/store'
import createAuthRefreshInterceptor from 'axios-auth-refresh';

axios.defaults.baseURL = "http://localhost:80/api/"

axios.interceptors.request.use((config) => {
  if (store.getters.access) {
    // Set JWT before request is sent
    config.headers['Authorization'] = 'Bearer ' + store.getters.access
  }
  return config
})

// refresh if access is expired
createAuthRefreshInterceptor(
  axios,
  (failedRequest) => {
    return store.dispatch('refreshAccess')
      .then(response => {
        failedRequest.config.headers['Authorization'] = 'Bearer ' + response.data.access;
        return Promise.resolve()
      })
    }
)

Vue.use(VueAxios, axios)