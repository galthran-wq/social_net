import axios from "axios";

export function fetchRoles(groupId, queryParams) {
  return axios.get(
    `/groups/${groupId}/roles/`,
    { params: queryParams}
  )
}

export function createRole(groupId, data, queryParams) {
  return axios.post(
    `/groups/${groupId}/roles/`,
    data,
    { params: queryParams}
  )
}

export function updateRole(groupId, roleName, data, queryParams) {
  return axios.patch(
    `/groups/${groupId}/roles/${roleName}/`,
    data,
    { params: queryParams}
  )
}

export function getMyPermissions(groupId) {
  return axios.get(
    `/groups/${groupId}/my_permissions/`
  )
}

export function getPermissionList() {
  return axios.get(
    '/groups/permissions/'
  )
}