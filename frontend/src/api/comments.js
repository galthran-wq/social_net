import axios from "axios";

export function fetchComments(postId, queryParams) {
  return axios.get(
    `/posts/${postId}/comments/`,
    { params: queryParams}
  )
}


export function createComment(postId, body, queryParams) {
  return axios.post(
    `/posts/${postId}/comments/`,
    {body},
    { params: queryParams}
  )

}

export function toggleLike(postId, commentId, queryParams) {
  return axios.post(
    `/posts/${postId}/comments/${commentId}/like/`,
    {},
    {params: queryParams}
  )
}

export function toggleDislike(postId, commentId, queryParams) {
  return axios.post(
    `/posts/${postId}/comments/${commentId}/dislike/`,
    {},
    {params: queryParams}
  )
}
