import axios from "axios"

export function logout(refresh, queryParams) {
  return axios.post(
    'auth/logout/',
    {refresh},
    {params: queryParams}
  )
}

export function refreshAccess(refresh, queryParams) {
  return axios.post(
    'auth/refresh/',
    {refresh},
    {params: queryParams}
  )
}

export function signup(credentials, queryParams) {
  return axios.post(
    'auth/signup/',
    credentials,
    {params: queryParams}
  )
}


export function login(credentials, queryParams) {
  return axios.post(
    'auth/login/',
    credentials,
    {params: queryParams, skipAuthRefresh: true}
  )
}