import axios from "axios"

export function fetchPosts(queryParams) {
  return axios.get(
    '/posts/',
    { params: queryParams}
  )
}

export function toggleLike(postId, queryParams) {
  return axios.post(
    `/posts/${postId}/like/`,
    {},
    {params: queryParams}
  )
}

export function toggleDislike(postId, queryParams) {
  return axios.post(
    `/posts/${postId}/dislike/`,
    {},
    {params: queryParams}
  )
}

export function createPost(data, queryParams) {
  return axios.post(
    '/posts/',
    data,
    {params: queryParams}
  )
}

export function deletePost(postId, queryParams) {
  return axios.delete(
    `/posts/${postId}/`,
    {params: queryParams}
  )
}
