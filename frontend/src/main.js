import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './api'
import './plugins/bootstrap-vue'
import './plugins/infinite-scroll'
import router from './router'
import store from './store'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
