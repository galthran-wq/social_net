import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/views/Login";
import Signup from "@/views/Signup";
import Profile from "@/views/Profile";
import store from "@/store"
import GroupList from "@/views/GroupList";
import GroupDetail from "@/views/GroupDetail";

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      requiresNonAuth: true
    },
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup,
    meta: {
      requiresNonAuth: true
    },
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/groups',
    name: 'GroupList',
    component: GroupList,
  },
  {
    path: '/groups/:groupId',
    name: 'GroupDetail',
    component: GroupDetail,
    props: true
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  let isLoggedIn = store.getters.currentUser
  let nextRoute;

  if (
    to.matched.some(record => record.meta.requiresAuth) &&
    !isLoggedIn
  )
    // this route requires auth,
    // redirect to login page.
    nextRoute = {name: 'Login'}
  else if (
    to.matched.some(record => record.meta.requiresNonAuth) &&
    isLoggedIn
  )
      nextRoute = {name: 'Profile'}

  if (nextRoute)
    next(nextRoute)
  else
    next()
})

export default router
